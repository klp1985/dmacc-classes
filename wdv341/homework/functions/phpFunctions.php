<?php

		

		function dateMonthNumToText($month) {  //If the month is entered as a number it is converted to the month name as a string
				

				if (is_numeric($month)) { 
						$dateObj   = DateTime::createFromFormat('!m', $month);
						
						$convertedMonth = $dateObj->format('F');
						
						return $convertedMonth;
				
				}
				
				else {
						
						return $month;
				
				}
		
		}

		function monthDayYearDateFormat ($day, $month, $year) {    // Takes input month, day, and year and places it in mm/dd/yyyy format
			
						$inputDate = $day . " " . dateMonthNumToText($month) . " " . $year;
			
						echo date("m/d/Y", strtotime($inputDate));
		}
		
		
		function dayMonthYearDateFormat ($day, $month, $year) {    // Takes input month, day, and year and places it in dd/mm/yyyy format
			
						$inputDate = $day . " " . dateMonthNumToText($month) . " " . $year;
						
						echo date("d/m/Y", strtotime($inputDate));
		}
		
		
		function stringInfo ($inputString) {  //  Takes an input string and displays information on the string.  Info includes length, amout of whitespace before and after, and searches for the string 'DMACC' within the string.  It also removes the leading and trailing whitespace as well as converts any uppercase characters to lowercase.
			
						$trimmedString = trim($inputString); // Removes any leading or trailing whitespace
						
						$stringLowercaseConvert = strtolower($inputString);  // Converts the string to lowercase
						
						$matchPattern = "DMACC";  // String that will be used by the preg_match function below for searching through the input string
						
						
						//String information display
			
						echo "<p>The string you entered:</p><h4>". $inputString . "</h4>";  
		
						echo "<p>The string you entered has <strong>" . strlen($inputString) ."</strong> characters</p>";

						echo "<p>Eliminating leading and trailing whitespace from the string results in <strong>" . strlen($trimmedString) . "</strong> characters.</p>";
						
						echo "<p>The string converted to all lowercase:</p><h4>". $stringLowercaseConvert ."</h4>";
						
						
						 if (preg_match("/$matchPattern/i", $inputString)) {  // Searches through the input string for the pattern defined by '$matchPattern'.  Displays whether it is found or not.
							   
							   echo "<p class = 'underBorder'><strong>" . $matchPattern . "</strong> was found within the input string.</p>";
							   
						 } else {
								
								echo "<p class = 'underBorder'><strong>" . $matchPattern . "</strong> was not found within the input string.</p>";
								
						 }
		}	

		
		function formatNumber($inputNumber) {  // Formats the input number
		
						echo number_format($inputNumber);
		
		}
		
		
		function formatCurrency($inputCurrency) {  // Converts the input number to US currency
						
						echo   "$" . number_format($inputCurrency, 2);
		
		}
?>

<!DOCTYPE html>

	<html>
	
			<head>
					
						<title>PHP Functions Assignment</title>
						
						<!--
							Kraig Popelka
						    1/31/2017
						 -->
						 
						 <style>
						 
								body {
										background-color: #a0d8b2;
										text-align: center;
								}
								
								h1 {
										margin: 0 auto;
										width: 50%;
										border-bottom: 8px ridge white;
								}
								
								.container {
										width: 50%
										margin: 0 auto;
								}
								
								
								
								.underBorder {
										border-bottom: 2px ridge white;
										width: 50%;
										margin: 45px auto;
								}
						 
						 </style>
						 
						
					
			</head>
			
			
			
			<body>
			
						<h1>Intro to PHP Assignment: PHP Functions</h1>
				
				
				<div class = "container">
			
						<p>Your entered date in mm/dd/yyyy format:</p>
				
						<h3 class = "underBorder">
				
								<?php 
										monthDayYearDateFormat(  
												$_POST['day'],
												$_POST['month'],
												$_POST['year']
										);	
								?>
					
						</h3>

				
				
						<p>Your entered date in dd/mm/yyyy format:</p>
						
						<h3 class = "underBorder">
						
								<?php
											dayMonthYearDateFormat(
													$_POST['day'],
													$_POST['month'],
													$_POST['year']
											);
								?>
							
						</h3>
						
								<?php
										stringInfo($_POST['inputString']);
								?>
						
						
						<p>Your number as a formatted number:</p>
						
						<h3 class = "underBorder">
						
								<?php
										formatNumber($_POST['inputNumber']);
								?>
							
						</h3>
						
						<p>Input number converted to US currency:</p>
						
						<h3 class = "underBorder">
								<?php
										formatCurrency($_POST['inputCurrency']);
								?>
						</h3>
						
				</div>	
				
			</body>
	
	</html>