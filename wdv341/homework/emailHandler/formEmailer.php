<?php
//Model-Controller Area.  The PHP processing code goes in this area. 

	//Method 1.  This uses a loop to read each set of name-value pairs stored in the $_POST array
	$tableBody = "";		//use a variable to store the body of the table being built by the script
	
	foreach($_POST as $key => $value)		//This will loop through each name-value in the $_POST array
	{
		if($key != 'compOS') { 			//Checks whether the key corresponds to the checkbox group.  If not places the name value pairs in the table.
				$tableBody .= "<tr>";				//formats beginning of the row
				$tableBody .= "<td>$key</td>";		//dsiplay the name of the name-value pair from the form
				$tableBody .= "<td>$value</td>";	//dispaly the value of the name-value pair from the form
				$tableBody .= "</tr>";				//End this row
		}
		
		else {						// When the key is compOS
				foreach($_POST['compOS'] as $selected){  //Goes through the compOS array and places the selected values of the checkboxes in the table.
				$tableBody .= "<tr>";				//formats beginning of the row
				$tableBody .= "<td>$key</td>";		//dsiplay the name of the name-value pair from the form
				$tableBody .= "<td>$selected</td>";	//dispaly the value of the name-value pair from the form
				$tableBody .= "</tr>";				//End this row
				}		
		}
	} 
	
	//This code pulls the field name and value attributes from the Post file
//The Post file was created by the form page when it gathered all the name value pairs from the form.
//It is building a string of data that will become the body of the email

//          CHANGE THE FOLLOWING INFORMATION TO SEND EMAIL FOR YOU //  

	$toEmail = "kpopelka@gmail.com";		//CHANGE within the quotes. Place email address where you wish to send the form data. 
										//Use your DMACC email address for testing. 
										//Example: $toEmail = "jhgullion@dmacc.edu";		
	
	$subject = "PHP emailer assignment";	//CHANGE within the quotes. Place your own message.  For the assignment use "WDV101 Email Example" 

	$fromEmail = "web@kraigpopelka.info";		//CHANGE within the quotes.  Use your DMACC email address for testing OR
										//use your domain email address if you have Heartland-Webhosting as your provider.
										//Example:  $fromEmail = "contact@jhgullion.org";  

//   DO NOT CHANGE THE FOLLOWING LINES  //

	//Creates the body of the email using HTML.  Form data is inputed into the table
	
	$emailBody = '<html><head><style>';
	
	$emailBody .='tr {text-align: center;}';
	
	$emailBody .='h1 {color: blue;}';

	$emailBody .= '</style></head><body>';
	
	$emailBody .='<h1>Thank You For Your Information, '. $_POST['firstName'] .'</h1>';
	
	$emailBody .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
	 
	$emailBody .= "<tr><th>Form</th> <th>Data</th></tr> ";
	
	$emailBody .= $tableBody;
	
	$emailBody .= '</table>';
	
	$emailBody .= '</body></html>';
	
	$headers = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";  //Allows the email to be created with HTML
	
	$headers .= "From: $fromEmail" . "\r\n";				//Creates the From header with the appropriate address

 	if (mail($toEmail,$subject,$emailBody,$headers)) 	//puts pieces together and sends the email to your hosting account's smtp (email) server
	{
   		echo("<h2 style = 'color: #42f442; text-align: center;'>Confirmation: Your information was successfully sent!</h2>");
  	} 
	else 
	{
   		echo("<h2 style = 'color: red; text-align: center;'>ERROR: There was a problem sending your information.  Please try again.</p>");
  	}
?>



<!doctype html>

<html>

		<head>

				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

				<title>WDV101 Form Emailer Example</title>

				<style type="text/css">
						body {
										background-color: #b6b7ba;
										color: #184968;
										text-align: center;
								}
								
								header {
										text-align: center;
										margin: 0 auto;
										width: 75%;
										border-bottom: 8px ridge #184968;
								}
								
								.container {
										width: 70%;
										margin: 0 auto;
								}
								
								table {
										margin: 0 auto;
								}

				</style>

		</head>

		
		<body>
		
				<header>

				<h1>WDV 341: Intro to PHP</h1>

				<h2>Email Handler Confirmation page</h2>

				<hr>
				
				</header>
				
				<div class = "container">

				<p>You should receive an email confirmation shortly</p>

				<hr>
				
				<h4>Information Sent</h4>

				

				<p>&nbsp;</p>

				<p>
				
							<table border='a'>
									<tr>
										<th>Field Name</th>
										<th>Value of Field</th>
									</tr>
									
									<?php echo $tableBody;  ?>
									
							</table>
							
				</p>
				

				</div>
				
</body>

</html>
