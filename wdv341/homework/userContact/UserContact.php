<?php

				class UserContact {
				
					//Properties
				
						public $name;
						
						public $email;				//email provided by customer
						
						public $reason;
						
						public $comments;
						
						public $mailingList;   //checkbox to be added to mailing list
						
						public $moreInfo;		//checkbox to send more data
						
						public $contactDate;
						
						public $msgText;
						
					//Constructor

					/*	function _construct() {
							
							
							
						}*/
						
						
						
					//Setters
						
						function set_name($inName) {
							
							//this -->  refers to the current object
							// -> go out to the current object and get the property.  No $ in front of property.
							
								$this -> name = $inName;  //Put input value into property
							
						}
						
						
						function set_email($inEmail) {
							
								$this -> email = $inEmail;  //Put input value into property
							
						}
						
						
						function set_reason($inReason) {
							
								$this -> reason = $inReason;
							
						}
						
						
						function set_comments($inComments) {
							
								$this -> comments = $inComments;
							
						}
						
						function set_mailingList($inMailingList) {
							
								$this -> mailingList = $inMailingList;
							
						}
						
						
						function set_moreInfo($inMoreInfo) {
							
								$this -> moreInfo = $inMoreInfo;
							
						}
						
						
						function set_contactDate() {
							      
								  
								date_default_timezone_set('America/Chicago');
							
								$this -> contactDate = date('h:i A \o\n m/d/y');
							
						}
						
						
						
					//Getters

						function get_name() {
							
								return $this -> name;
							
						}
						
						
						function get_email() {
							
								return $this -> email;
							
						}
						
						
						function get_reason() {
							
								return $this -> reason;
								
						}
						
						
						function get_comments() {
							
								return $this -> comments;
								
						}
						
						
						function get_mailingList() {
							
								return $this -> mailingList;
								
						}
						
						
						function get_moreInfo() {
							
								return $this -> moreInfo;
								
						}
						
						
						function get_contactDate() {
							
								
							
								return $this -> contactDate;
								
						}
						
					//Processing Methods
					
						function formatHTMLMessage() {
								
								$msg = "<!DOCTYPE><html><head></head><body><h1>Thank You!</h1>";
								
								$msg .= "<h2>Your message was received at $this->contactDate.</h2>";
							
							    $msg .= "<p>Dear $this->name,</p>";
								
								$msg .= "<p>Thank you very much for your recent contact regarding $this->reason.  Your comment:  '<em>$this->comments</em>' has been received and you should receive a prompt response.</p>";
								
								if ($this->mailingList == "on") {
									$msg .= "<p>Thank you for signing up for the mailing list.  We send out weekly newsletters and special offers.  You will receive these at $this->email.  ";
								} 	
									
								elseif ($this->moreInfo == "on") {
									$msg .= "<p>We greatly appreaciate you signing up to receive information about our products.  Your business is important to us.</p>";
								}
								
								
								if ($this->mailingList == "on" && $this->moreInfo == "on") {
									$msg .= "We also greatly appreaciate you signing up to receive information about our products.  Your business is important to us.</p>";
								}
								
								
								
								

								
								
								$msg .= "<p>Sincerely,</p>";
								
								$msg .= "<p>The ABC Company Online Team</p></body></html>";
								
								
								
								return $msg;
							
						}
						
						
								
						function formatEmailMessage() {
							
								
								
								
							
							    $msgText = "Dear $this->name," . "\r\n";
								
								$msgText .= "\r\n";
								
								$msgText .= "Thank you very much for your recent contact regarding $this->reason.  ";  
								
								$msgText .= "Your comment:  '$this->comments' has been received and you should receive a prompt response." . "\r\n";
								
								if ($this->mailingList == "on") {
									$msgText .= "Thank you for signing up for the mailing list.  We send out weekly newsletters and special offers.  You will receive these at $this->email.  " . "\r\n";
								} 
								
								elseif ($this->moreInfo == "on") {
									$msgText .= "We greatly appreaciate you signing up to receive information about our products.  Your business is important to us." . "\r\n";
								}
								
								if ($this->mailingList == "on" && $this->moreInfo == "on") {
									$msgText .= "We also greatly appreaciate you signing up to receive information about our products.  Your business is important to us.  " ."\r\n";
								}

								$msgText .= "\r\n";
								
								$msgText .= "Sincerely," . "\r\n";
								
								$msgText .= "\r\n";
								
								$msgText .= "The ABC Company Onine Team" . "\r\n";
								
								
								
								
								
								
								return $msgText;
							
						}
					

						function sendEmail() {
							
								$toEmail = $this->email;				
	
								$subject = "ABC Support: $this->reason Ticket Created";	

								$fromEmail = "web@kraigpopelka.info";	
								
								$msg = $this->formatEmailMessage();
								
								$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";
								
								$headers .= "From: $fromEmail" . "\r\n";
								
				
								
								if (mail($toEmail,$subject,$msg,$headers)) 	//puts pieces together and sends the email to your hosting account's smtp (email) server
								{
									echo("<h2 style = 'color: #42f442; text-align: center;'>Confirmation: Your information was successfully sent!</h2>");
								} 
								else 
								{
									echo("<h2 style = 'color: red; text-align: center;'>ERROR: There was a problem sending your information.  Please try again.</p>");
								}
							
						}
						

						
						
						
				} // End of UserContact class

?>