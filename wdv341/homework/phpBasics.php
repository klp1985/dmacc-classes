<?php
		$yourName = "Kraig Popelka";  // Creates a vairable called yourName and assigns a string to it
		
		$number1 = 5;  //Variables number1 and number2 are created and assigned integer values.
		
		$number2 = 2;
		
		$total = $number1 + $number2;  // total variable is created and adds together number1 and number2
		
		$jsArray = "var buildArray = new Array('PHP', 'HTML', 'Javascript');";  // Creates a variable called jsArray and assigns a new javascript array as a string

?>

<!DOCTYPE html>

	<html>
	
			<head>
					
						<title>PHP Basics Assignment</title>
						
						<!--
							Kraig Popelka
						    1/24/2017
						 -->
						 
						 <style>
						 
								body {
										background-color: #a0d8b2;
										text-align: center;
								}
						 
						 </style>
						 
						 
						 
						 
						 <script>
						 
								<?php echo $jsArray ?>  // Places the jsArray variable within the script tags.  This creates a javascript array named buildArray.
						 
						 </script>
					
			</head>
			
			
			
			<body>
			
					<?php echo "<h1>Intro to PHP Assignment: PHP Basics</h1>"; ?>
			
					<h2>My name is <?php echo $yourName ?></h2>  <!-- Places the yourName variable within the h2 element. -->
					
					<p>Value 1: <?php echo $number1 ?></p> <!-- Displays variable number1 on the page -->
					
					<p>Value 2: <?php echo $number2 ?></p> <!-- Displays variable number2 on the page -->
					
					<p>Total: <?php echo $total ?></p> <!-- Displays the calculated total of variables number1 and number2 on the page. -->
					
					<p>Array created with PHP and displayed with Javascript</p>
					
					<script>
								
								document.write(buildArray);  // Writes buildArray to the page.
						 
					 </script>
					
					
			</body>
	
	</html>