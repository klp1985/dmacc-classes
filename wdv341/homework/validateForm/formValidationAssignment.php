<?php

				$validForm = false;
				
				$inName = "";
				$inSSN = "";
				
				
				$nameErrMsg = "";
				$ssnErrMsg = "";
				$radioErrMsg = "";
				
				
				
				function validateName() {
					
						global $inName, $validForm, $nameErrMsg;
						
						$nameErrMsg = "";
						
						if ( !$inName == "") {
							
								$inName = ltrim($inName);
						
						}
						
						else {
								
								$validForm = false;
								
								$nameErrMsg = "Your name is required.";
								
						}
				
				}
				
			
				
				function validateSSN() {
				
						global $inSSN, $validForm, $ssnErrMsg;
						
						$ssnErrMsg = "";
						
						if ( !$inSSN == "") {
								
								if ( !preg_match("/^\d{9}$/",$inSSN)) {
						
								$validForm = false;
								
								$ssnErrMsg = "Invalid SSN format.  Must include 9 numbers and exclude spaces and special characters."; 
						
								}
								
								
						
						}
						
						else {
							
								$validForm = false;
								
								$ssnErrMsg = "Social Security Number is required.";
						}
				
				}
				
				
				
				function validateRadio() {
					
						global $validForm, $radioErrMsg;
						
						$radioErrMsg = "";
						
						if ( !isset($_POST['RadioGroup1'])) {
							
								$validForm = false;
								
								$radioErrMsg = "A response option must be selected.";
						
						}
				
				}
			

		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
				
				$inName = $_POST['inName'];
				$inSSN = $_POST['inSSN'];
				
				$validForm = true;
				
				
				
				
				validateName();
				validateSSN();
				validateRadio();
				
		}

?>




<!DOCTYPE html>

		<html >

				<head>

				
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>WDV341 Intro PHP - Form Validation Example</title>

						
						<style>

								#orderArea	{
									width:600px;
									background-color:#CF9;
								}

								.error	{
									color:red;
									font-style:italic;	
								}

						</style>

						
				</head>



				<body>

						<h1>WDV341 Intro PHP</h1>

						<h2>Form Validation Assignment</h2>
						
						
<?php

					if ($validForm) {			//If the form has been entered and validated a confirmation page is displayed in the VIEW area.
					
?>
						<h3>Thank You!</h3>
						
						<p>Your registration has been processed.</p>

<?php

					}
						
					else {

?>						

						<div id="orderArea">

						  <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
						  
						  <h3>Customer Registration Form</h3>
						  
						  
						  <table width="587" border="0">
						  
								<tr>
									  <td width="117">Name:</td>
									  <td width="246"><input type="text" name="inName" id="inName" size="40" value="<?php echo $inName;?>"/></td>
									  <td width="210" class="error"><?php echo "$nameErrMsg"; //place error message on form  ?></td>
								</tr>
								
								<tr>
									  <td>Social Security</td>
									  <td><input type="text" name="inSSN" id="inSSN" size="40" value="<?php echo $inSSN;?>" /></td>
									  <td class="error"><?php echo "$ssnErrMsg"; //place error message on form  ?></td>
								</tr>
								
								<tr>
								
									  <td>Choose a Response</td>
									  
											<td>
											
												<p>
											
													<label>
													  <input type="radio" name="RadioGroup1" id="RadioGroup1_0" value = "phone"<?php if (isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='phone' ){echo ' checked="checked"';}?>>
													  Phone</label>
													  
													<br>
													
													<label>
													  <input type="radio" name="RadioGroup1" id="RadioGroup1_1" value = "email"<?php if (isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='email' ){echo ' checked="checked"';}?>>
													  Email</label>
													  
													<br>
													
													<label>
													  <input type="radio" name="RadioGroup1" id="RadioGroup1_2" value = "usMail"<?php if (isset($_POST['RadioGroup1']) && $_POST['RadioGroup1']=='usMail' ){echo ' checked="checked"';}?>>
													  US Mail</label>
													  
													<br>
												
												</p>
												
											</td>
											
									  <td class="error"><?php echo "$radioErrMsg"; //place error message on form  ?></td>
									  
								</tr>
							
						  </table>
						  
						  
						  <p>
								<input type="submit" name="submitBtn" id="submitBtn" value="Register" />
								<input type="reset" name="resetBtn" id="resetBtn" value="Clear Form" />
						  </p>
						  
						</form>

						</div>
						
<?php

					} // End else loop
		
?>

				</body>

		</html>