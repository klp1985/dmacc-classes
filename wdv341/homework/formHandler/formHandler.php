<?php
//Model-Controller Area.  The PHP processing code goes in this area. 

	//Method 1.  This uses a loop to read each set of name-value pairs stored in the $_POST array
	$tableBody = "";		//use a variable to store the body of the table being built by the script
	
	foreach($_POST as $key => $value)		//This will loop through each name-value in the $_POST array
	{
		if($key != 'compOS') { 			//Checks whether the key corresponds to the checkbox group.  If not places the name value pairs in the table.
				$tableBody .= "<tr>";				//formats beginning of the row
				$tableBody .= "<td>$key</td>";		//dsiplay the name of the name-value pair from the form
				$tableBody .= "<td>$value</td>";	//dispaly the value of the name-value pair from the form
				$tableBody .= "</tr>";				//End this row
		}
		
		else {						// When the key is compOS
				foreach($_POST['compOS'] as $selected){  //Goes through the compOS array and places the selected values of the checkboxes in the table.
				$tableBody .= "<tr>";				//formats beginning of the row
				$tableBody .= "<td>$key</td>";		//dsiplay the name of the name-value pair from the form
				$tableBody .= "<td>$selected</td>";	//dispaly the value of the name-value pair from the form
				$tableBody .= "</tr>";				//End this row
				}		
		}
	} 
	
	
	//Method 2.  This method pulls the individual name-value pairs from the $_POST using the name
	//as the key in an associative array.  
	
	$inFirstName = $_POST["firstName"];		//Get the value entered in the first name field
	$inLastName = $_POST["lastName"];		//Get the value entered in the last name field
	$inSchool = $_POST["school"];			//Get the value entered in the school field
	$inSemesterGraduate = $_POST["semesterGraduate"]; //Get the value selected from the radio button group
	$inOperatingSystem = $_POST["compOS"]; //Get the value(s) selected from the checkbox group
	$inAreaStudy = $_POST["areaStudy"]; //Get the value selected from the dropdown list
	

?>




<!DOCTYPE html>

		<head>
		
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				
				<title>WDV 341 Intro PHP - Code Example</title>
				
				<style>
				
						body {
									background-color: #b6b7ba;
									color: #184968;
						}
						
						header {
									text-align: center;
									margin: 0 auto;
									width: 75%;
									border-bottom: 8px ridge #184968;
						}
						
						.container {
									width: 50%;
									margin: 0 auto;
						}
						
						table {
									background-color: #d2d6d8;
									color: #184968;
									margin: 0 auto;						
						}
				
						.method-two {
									background-color: #d2d6d8;
									margin: 0 25%;
									padding: 0 10%;
									border: 4px solid #184968;
						}
				</style>
				
		</head>

		<body>
		
			<header>
		
				<h1>WDV341 Intro PHP</h1>
				
				<h2>Form Handler Result Page</h2>
				
			</header>
			
			<div class = "container">
				
				<p>This page displays the results of the Server side processing. </p>
				
				<p>The PHP page has been formatted to use the Model-View-Controller (MVC) concepts. </p>
				
				<hr />
				
				<h3>Display the values from the form using Method 1. Uses a loop to process through the $_POST array</h3>
				
				<p>
					<table border='a'>
					<tr>
						<th>Field Name</th>
						<th>Value of Field</th>
					</tr>
					<?php echo $tableBody;  ?>
					</table>
				</p>
				
				<hr />
				
				<h3>Display the values from the form using Method 2. Displays the individual values.</h3>
				
						<div class = "method-two">
				
								<p>School: <?php echo $inSchool; ?></p>
								
								<p>First Name: <?php echo $inFirstName; ?></p>
								
								<p>Last Name: <?php echo $inLastName; ?></p>
								
								<p>Date of Graduation: <?php echo $inSemesterGraduate; ?></p>
								
								<p>Operating Systems Used: 
										<?php 
								
													if(!empty($_POST['compOS'])){     	//Checks whether the compOS array is empty.  
															foreach($_POST['compOS'] as $selected){  // Goes through the array and retrieves the elements of the array and places them as a variable 'selected'
																echo $selected."</br>";  	//Writes each selected checkbox value to the page
															}
													} 
										?>
								</p>
								
								<p>Area of Study: <?php echo $inAreaStudy; ?></p>
								
						</div>
						
			</div>

		</body>
		
</html>
