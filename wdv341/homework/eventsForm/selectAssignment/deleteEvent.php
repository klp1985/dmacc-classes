<?php
	session_start();
	
	$message = "";
	
	if ($_SESSION['validUser'] == "yes") {
	
		$deleteRecId = $_GET['event_id'];	//Pull the presenter_id from the GET parameter
		
		include '../connectPDO.php';		//connects to the database
		
		try {
				$sql = "DELETE FROM wdv341_event WHERE event_id = $deleteRecId";
				//echo "<p>The SQL Command: $sql </p>";     //testing
				
				$conn->exec($sql);
				
				$message =  "<p>The record was successfully deleted.</p>";
		}
		
		catch (PDOException $e) {
				$message = "<p>There was a problem deleting the record.  Please try again" . $e->getMessage() . "</p>";
		}
		
		$conn = null;	//close the database connection

	}
	else {
		
			header('Location: ../login.php');
		
	}
?>


<!DOCTYPE html>
		<html>
				<head>
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						<title>WDV341 Intro PHP  - Presenters Admin Example</title>
				</head>

				<body>

						<h1>WDV341 Intro PHP </h1>
						
						<h2>Presenters Admin System Example</h2>
						
						<h3>DELETE Record Page</h3>
						
						<p>This page is called from the viewPresenters.php page when the user/customer clicks on the Delete link. This page will use the presenter_id that has been passed as a GET parameter on the URL to this page. </p>
						
						<p>The SQL DELETE query will be created. Once the query is processed this page will confirm that it processed correctly. It will display a confirmation to the user/customer if it worked correctly or it will display an error message if there were problems.</p>
						
						<p>Note: In a production environment this error message should be user/customer friendly. Additional information should be sent to the developer so that they can see 
						what happened when they attempt to fix it. </p>

						<h2>
								<?php echo $message; ?>
						</h2>
						
						<p>
								<a href = "selectEvents.php">Go Back to Table</a>
						</p>
						
						<p>
								<a href='../logout.php'>Logout of Events Admin System</a>
						</p>

				</body>
		</html>
