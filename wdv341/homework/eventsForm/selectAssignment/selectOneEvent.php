<?php

$displayMsg = "";

include '../connectPDO.php';

$sql = "SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = 2";

try {

		$stmt = $conn->prepare($sql);
		$stmt->execute();
		
		
		
		
		if ($stmt->execute()) {
		
				
				while($row = $stmt->fetch()) {
					
						$displayMsg .= "<tr>";
						$displayMsg .= "<td>" . $row['event_name'] . "</td>";
						$displayMsg .= "<td>" . $row['event_description'] . "</td>";
						$displayMsg .= "<td>" . $row['event_presenter'] . "</td>";
						$displayMsg .= "<td>" . $row['event_date'] . "</td>";
						$displayMsg .= "<td>" . $row['event_time'] . "</td>";
						$displayMsg .= "</tr>\n";
				
				}
				
				$conn = null;
				
		}
		
		else {
		
				$displayMsg = "There was an error processing your request.";
		
		}
	
}

catch (PDOException $e) {

		echo "there was an error with your request" . $e->getMessage();

}

?>
		<html>
		
				<head>
				
					<title>WDV341 SELECT Example</title>

				</head>
				
				<body>

							<?php 
									
									if ($displayMsg != "") {
							?>
							
							<h1>We found the following information.</h1>
					
							<div id="content">
							
									<table border="1">
										<tr>
											<th>Event Name</th>
											<th>Description</th>
											<th>Presenter</th>
											<th>Date</th>
											<th>Time</th>
											<th>Delete</th>
										</tr>  
										
							<?php
										echo $displayMsg; 
									}
									
									else {
										
										echo "<h2>There are no events to show.</h2><p>Add one by clicking the 'Enter a New Record' link below.</p>";
										
									}
									
							?>
							
									</table>
						
							</div>
					
							<p>
									<a href = "../eventsForm.php">Enter a New Record</a>
							</p>

				</body>
				
		</html>