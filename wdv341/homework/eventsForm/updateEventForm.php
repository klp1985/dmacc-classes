<?php

				$validForm = false;
				
				$inEventName = "";
				$inDescription = "";
				$inPresenter = "";
				$inDate = "";
				$inTime = "";
				$dateToDB = "";
				$timeToDB = "";
				
				$updateEventId = "";
				$sqlUpdate = "";
				
				$eventNameErrMsg = "";
				$descriptionErrMsg = "";
				$presenterErrMsg = "";
				$dateErrMsg = "";
				$timeErrMsg = "";
				
				
			
				
				function validateEventName() {
					
						global $inEventName, $validForm, $eventNameErrMsg;
						
						$eventNameErrMsg = "";
						
						if ( !$inEventName == "") {
							
								$inEventName = ltrim($inEventName);
						
						}
						
						else {
								
								$validForm = false;
								
								$eventNameErrMsg = "Event name is required.";
								
						}
				
				}
				
			
				
				function validateDescription() {
				
						global $inDescription, $validForm, $descriptionErrMsg;
						
						$descriptionErrMsg = "";
						
						if ( $inDescription == "") {
								
								$validForm = false;
								
								$descriptionErrMsg = "An event description must be entered.";
						
						}
								
				
				}
				
				
				
				function validatePresenter() {
					
						global $inPresenter, $validForm, $presenterErrMsg;
						
						$presenterErrMsg = "";
						
						if ( $inPresenter == "") {
								
								$validForm = false;
								
								$presenterErrMsg = "Enter the name of a presenter.";
						
						}
				
				}
				
				
				
				function validateDate() {
					
						global $inDate, $validForm, $dateErrMsg, $dateToDB;
					
						$date_regex = '^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$^';
						
						if( !$inDate == "") {
						
								if(!preg_match($date_regex, $inDate)) {
									
										$validForm = false;
							
										$dateErrMsg = "The date format is not incorrect.";
						  
								} else {

										$dateToDB = date('Y-m-d', strtotime($inDate));  //If the date is valid, converts the date to the format YYYY-MM-DD for entry into the database.
								
								}
								
						} else {
								
								$validForm = false;
								
								$dateErrMsg = "An event date is required.";
						
						}
					
				}
				
				
				
				function validateTime() {
					
						global $inTime, $validForm, $timeErrMsg, $timeToDB;
					
						$time_regex = '/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i';
						
						if( !$inTime == "") {
						
								if(!preg_match($time_regex, $inTime)) {
									
										$validForm = false;
							
										$timeErrMsg = "The time format is incorrect.";
								} else {
										
										$timeToDB = date("H:i", strtotime($inTime));  //If the time is valid, converts time to the 24 hour format for entry into the database.
										
								}
								
						} else {
								
								$validForm = false;
								
								$timeErrMsg = "The time of the event is required.";
						
						}
					
				}
		/*		
				
		include "connectPDO.php";  //Connects to the database and inserts the data	

		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
				
				$inEventName = $_POST['event_name'];
				$inDescription = $_POST['event_description'];
				$inPresenter = $_POST['event_presenter'];
				$inDate = $_POST['event_date'];
				$inTime = $_POST['event_time'];
				
				$validForm = true;
				
				
				
				
				validateEventName();
				validateDescription();
				validatePresenter();
				validateDate();
				validateTime();
		}

?>




<!DOCTYPE html>

		<html >

				<head>

				
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>WDV341 Intro PHP - Form Validation Example</title>

						
						<style>

								#orderArea	{
									width:600px;
									background-color:#CF9;
								}

								.error	{
									color:red;
									font-style:italic;	
								}

						</style>

						
				</head>



				<body>

						<h1>WDV341 Intro PHP</h1>

						<h2>Form Validation Assignment</h2>
						
						
<?php

					if ($validForm) {			//If the form has been entered and validated a confirmation page is displayed in the VIEW area.
						
						
						
						try {
							
								$sql = "UPDATE wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
								VALUES (:event_name, :event_description, :event_presenter, :event_date, :event_time)"
							
								$sqlPrepare = $conn->prepare($sql);
								
								$sqlPrepare->bindParam(':event_name', $inEventName);
								$sqlPrepare->bindParam(':event_description', $inDescription);
								$sqlPrepare->bindParam(':event_presenter', $inPresenter);
								$sqlPrepare->bindParam(':event_date', $dateToDB);
								$sqlPrepare->bindParam(':event_time', $timeToDB);
						}
						
						catch (PDOException $e) {
								echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
						}
						
						$conn = null;
						
						
						if ($sqlPrepare->execute()){ 
?>
							<h3>Thank You!</h3>
						
							<p>Your event information has been processed.</p>
						
							<p>&nbsp</p>
						
							<p><a href = "eventsForm.php">Enter Another Event</a></p>
							
							<p><a href = "selectAssignment/selectEvents.php">View Events</a></p>

<?php
						}
						
						else {
?>							
								<h3>There Was a Problem</h3>
								
								<p>An error occurred while processing your data.</p>
								
								<p>Please try again</p>
								
								<p><a href = "eventsForm.php">Back to Event Form</a></p>
<?php							
						}
						
					}
						
					else {

?>						

						<div id="orderArea">

						  <form id="form1" name="form1" method="post" action="updateEventForm.php">
						  
								<p>Use this form to update information.  Place new information in the appropriate field(s)</p>
								
								<p>Event Name: <span class = "error"><?php echo "$eventNameErrMsg"; ?></span><br>
										<input type="text" name="event_name" id="eventName" value="<?php echo $inEventName;?>"/>
								</p>

								<p>Event Description:  <span class = "error"><?php echo "$descriptionErrMsg"; ?></span><br>
										<textarea name="event_description" id = "eventDescription" rows="5" cols="40"><?php echo $inDescription;?></textarea>
								</p>
								  
								<p>Presenter:  <span class = "error"><?php echo "$presenterErrMsg"; ?></span><br>
										<input type="text" name="event_presenter" id="eventPresenter" value="<?php echo $inPresenter;?>"/>
								</p>
								
								<p>Event Date:  <span class = "error"><?php echo "$dateErrMsg"; ?></span><br>
										(valid format:  mm/dd/yyyy)<br>
										<input type = "date" name = "event_date" id = "eventDate" value="<?php echo $inDate;?>"/>
								</p>
								
								<p>Event Time: <span class = "error"><?php echo "$timeErrMsg"; ?></span><br>
										(valid format:  HH:MM AM/PM)<br>
										<input type = "text" name = "event_time" id = "eventTime" value="<?php echo $inTime;?>"/>
								</p>
						  
						  
							    <p>
										<input type="submit" name="submitBtn" id="submitBtn" value="Submit" />
										<input type="reset" name="resetBtn" id="resetBtn" value="Clear Form" />
							    </p>
						  
						</form>

						</div>
						
<?php

					} // End else loop
		
?>

				</body>

		</html>
		
*/?>		
		
		
		
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
<?php

		include "connectPDO.php";  //Connects to the database and inserts the data	

		if ( isset($_POST['submitBtn']) ) {	 /*If the form has been posted or submitted*/

				//Then process the form data from the server using $_POST or $_GET
				
				$inEventName = $_POST['event_name'];
				$inDescription = $_POST['event_description'];
				$inPresenter = $_POST['event_presenter'];
				$inDate = $_POST['event_date'];
				$inTime = $_POST['event_time'];
				$updateEventId = $_GET['event_id'];
				
								
				//Validate the form data or at least sanitize the data
				
				$validForm = true;
				
				validateEventName();
				validateDescription();
				validatePresenter();
				validateDate();
				validateTime();
		}
?>

								<!DOCTYPE html>

										<html >

												<head>

												
														<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

														<title>WDV341 Intro PHP - Form Validation Example</title>

														
														<style>

																#orderArea	{
																	width:600px;
																	background-color:#CF9;
																}

																.error	{
																	color:red;
																	font-style:italic;	
																}

														</style>

														
												</head>



												<body>

														<h1>WDV341 Intro PHP</h1>
														
														

														<h2>Form Validation Assignment</h2>

<?php				
				if ($validForm) {  /*If valid data*/
				
						//Then create the update query
						
						echo "<p>Valid Data</p>";
						
						$sqlUpdate = "UPDATE wdv341_event SET ";
						$sqlUpdate .= "event_name = '$inEventName', ";
						$sqlUpdate .= "event_description = '$inDescription', ";
						$sqlUpdate .= "event_presenter = '$inPresenter', ";
						$sqlUpdate .= "event_date = '$dateToDB', ";
						$sqlUpdate .= "event_time = '$timeToDB' ";
						$sqlUpdate .= "WHERE (event_id = '$updateEventId')";
						
						$updateStmt = $conn->prepare($sqlUpdate);
						
						//Run the update query
						
						$updateStmt->execute();
						
						if ($updateStmt->execute()){  /*If update query was successful*/
							
								//Confirm to the user that their data has been successfully processed
								//Provide a link to the home page and the select page
								echo "<h1>Success</h1>";
								
								echo "<a href = 'selectAssignment/selectEvents.php'>Go back to table</a>";
							
						}
						
						else { /*If there was an error with the query and did not execute*/
								echo "<h1>Update error</h1>";
								//Display message “Oops Please try again”
								//Display a link to the home page and select page
						
						}
				
				}
				
				else { /*If the data did not pass validation*/
					
						//Create error messages for the fields in error
						//Display form to user
					
				
		

		  /*If the form has not been seen by the user*/

				//Retrieve the record id from the $_GET name value pair
				
				$updateEventId = $_GET['event_id'];
				
				echo "<h1>Updating Event:  $updateEventId</h1>";
				
				//Create a SELECT query to retrieve the requested record from the database table
				
				$sqlSelect = "SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = $updateEventId";
				
				//Run SELECT query
				
				try {
						$stmt = $conn->prepare($sqlSelect);
						$stmt->execute();
				}
				
				catch (PDOException $e) {

						echo "there was an error with your request" . $e->getMessage();

				}
				
				if ($stmt->execute()){  /*If select query was successful and there is content*/
				//Place content from database into form fields
				//Display the form to the user with content in the fields
				
						while($row = $stmt->fetch()) {
								
							
								
						
?>				
						
								
														<div id="orderArea">

														  <form id="form1" name="form1" method="post" action="updateEventForm.php?event_id=<?php echo $updateEventId?>">
																
																<p>Event Name: <span class = "error"><?php echo "$eventNameErrMsg"; ?></span><br>
																		<input type="text" name="event_name" id="eventName" value="<?php echo $row['event_name'];?>"/>
																</p>

																<p>Event Description:  <span class = "error"><?php echo "$descriptionErrMsg"; ?></span><br>
																		<textarea name="event_description" id = "eventDescription" rows="5" cols="40"><?php echo $row['event_description'];?></textarea>
																</p>
																  
																<p>Presenter:  <span class = "error"><?php echo "$presenterErrMsg"; ?></span><br>
																		<input type="text" name="event_presenter" id="eventPresenter" value="<?php echo $row['event_presenter'];?>"/>
																</p>
																
																<p>Event Date:  <span class = "error"><?php echo "$dateErrMsg"; ?></span><br>
																		(valid format:  mm/dd/yyyy)<br>
																		<input type = "text" name = "event_date" id = "eventDate" value="<?php echo $row['event_date'];?>"/>
																</p>
																
																<p>Event Time: <span class = "error"><?php echo "$timeErrMsg"; ?></span><br>
																		(valid format:  HH:MM AM/PM)<br>
																		<input type = "text" name = "event_time" id = "eventTime" value="<?php echo $row['event_time'];?>"/>
																</p>
														  
														  
																<p>
																		<input type="submit" name="submitBtn" id="submitBtn" value="Submit" />
																		<input type="reset" name="resetBtn" id="resetBtn" value="Clear Form" />
																</p>
														  
														</form>

														</div>
														
												</body>
												
										</html>
						
						
<?php				
						}
				
				
				}	

				}

		$conn = null;
?>
