<?php
		session_start();
		
		$validForm = false;
				
		$inEventName = "";
		$inDescription = "";
		$inPresenter = "";
		$inDate = "";
		$inTime = "";
		$dateToDB = "";
		$timeToDB = "";
		$dateDBtoForm = "";
		$timeDBtoForm = "";
				
		$updateEventId = "";
		$sqlUpdate = "";
		
		$updateHeading = "";
		$resultMsg = "";
		$eventNameErrMsg = "";
		$descriptionErrMsg = "";
		$presenterErrMsg = "";
		$dateErrMsg = "";
		$timeErrMsg = "";
		
		
		/* Form field validation functions*/
		
		function validateEventName() {
					
				global $inEventName, $validForm, $eventNameErrMsg;	
				
				$eventNameErrMsg = "";
				
				if ( !$inEventName == "") {	/*If there is something entered into the field...*/
				
						$inEventName = ltrim($inEventName);			
						
				} else {	 /*If the field was left blank...*/		
				
						$validForm = false;			
						
						$eventNameErrMsg = "Event name is required.";								
				}				
		}
				
							
		function validateDescription() {
				
				global $inDescription, $validForm, $descriptionErrMsg;
						
				$descriptionErrMsg = "";
						
				if ( $inDescription == "") {
								
						$validForm = false;
								
						$descriptionErrMsg = "An event description must be entered.";
						
				}
									
		}
				
				
				
		function validatePresenter() {
					
				global $inPresenter, $validForm, $presenterErrMsg;
						
				$presenterErrMsg = "";
						
				if ( $inPresenter == "") {
								
						$validForm = false;
								
						$presenterErrMsg = "Enter the name of a presenter.";
						
				}
				
		}
				
				
				
		function validateDate() {
					
				global $inDate, $validForm, $dateErrMsg, $dateToDB;
					
				$date_regex = '^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$^';
						
				if( !$inDate == "") {
						
						if(!preg_match($date_regex, $inDate)) {
									
								$validForm = false;
							
								$dateErrMsg = "The date format is not incorrect.";
						  
						} else {

								$dateToDB = date('Y-m-d', strtotime($inDate));  //If the date is valid, converts the date to the format YYYY-MM-DD for entry into the database.
								
						}
								
				} else {
								
						$validForm = false;
								
						$dateErrMsg = "An event date is required.";
						
				}
					
		}
				
				
				
		function validateTime() {
					
				global $inTime, $validForm, $timeErrMsg, $timeToDB;
					
				$time_regex = '/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i';
						
				if( !$inTime == "") {
						
						if(!preg_match($time_regex, $inTime)) {
									
								$validForm = false;
							
								$timeErrMsg = "The time format is incorrect.";
						} else {
										
								$timeToDB = date("H:i", strtotime($inTime));  //If the time is valid, converts time to the 24 hour format for entry into the database.
										
						}
								
				} else {
								
						$validForm = false;
								
						$timeErrMsg = "The time of the event is required.";
						
				}
					
		}
		
		/* End validation functions */
		
		function dateFormatConvert() {
			
				global $inDate, $dateDBtoForm;
				
				$dateDBtoForm = date("m/d/Y", strtotime($inDate));
				
				$inDate = $dateDBtoForm;
			
		}
		
		
		function timeFormatConvert() {
			
				global $inTime, $timeDBtoForm;
				
				$timeDBtoForm = date("h:i a", strtotime($inTime));
				
				$inTime = $timeDBtoForm;
			
		}
		
	if ($_SESSION['validUser'] == "yes") {
		
		
		include "connectPDO.php";  //Connects to the database and inserts the data	
		
		if ( isset($_POST['submitBtn']) ) {	 /*If the form has been posted or submitted*/
		
			/*Get the form data from the $_POST variable and place in PHP variables*/
				$inEventName = $_POST['event_name'];
				$inDescription = $_POST['event_description'];
				$inPresenter = $_POST['event_presenter'];
				$inDate = $_POST['event_date'];
				$inTime = $_POST['event_time'];
				
			/*Get the event ID from the $_GET variable that was passed from the action attribute in the form*/	
				$updateEventId = $_GET['event_id'];
				
			/*Validate the form data or at least sanitize the data*/				
				$validForm = true;
				
				validateEventName();
				validateDescription();
				validatePresenter();
				validateDate();
				validateTime();
		
		} //End if form submitted
		
		else {
		
				/*Run the SELECT query to get data to put in form fields for the specified event_id*/
				
				/*Get the event ID from the $_GET variable that was passed from the 'Update' link on the selectEvents.php page*/
				$updateEventId = $_GET['event_id'];
				
				$updateHeading = "<h1>Updating Event:  $updateEventId</h1>";
				
				/*Create a SELECT query to retrieve the requested record from the database table*/
				
				$sqlSelect = "SELECT event_name, event_description, event_presenter, event_date, event_time FROM wdv341_event WHERE event_id = $updateEventId";
				
				//Run SELECT query
				
				try {
						$stmt = $conn->prepare($sqlSelect);
						$stmt->execute();
				}
				
				catch (PDOException $e) {

						$resultMsg = "<p>There was an error with your request: " . $e->getMessage() . "</p>";

				}
				
				if ($stmt->execute()){  /*If select query was successful and there is content*/
				//Place content from database into form fields
				//Display the form to the user with content in the fields
				
						while($row = $stmt->fetch()) {
							
								$inEventName = $row['event_name'];
								$inDescription = $row['event_description'];
								$inPresenter = $row['event_presenter'];
								$inDate = $row['event_date'];
								$inTime = $row['event_time'];
								
								dateFormatConvert();
								timeFormatConvert();
						}
				}
		
		}
		
		if ($validForm) {  /*If data validation passes...*/
		
				$sqlUpdate = "UPDATE wdv341_event SET ";
				$sqlUpdate .= "event_name = '$inEventName', ";
				$sqlUpdate .= "event_description = '$inDescription', ";
				$sqlUpdate .= "event_presenter = '$inPresenter', ";
				$sqlUpdate .= "event_date = '$dateToDB', ";
				$sqlUpdate .= "event_time = '$timeToDB' ";
				$sqlUpdate .= "WHERE (event_id = '$updateEventId')";
			
			
				$updateStmt = $conn->prepare($sqlUpdate);
						
				//Run the update query
						
				$updateStmt->execute();
						
				if ($updateStmt->execute()){  /*If update query was successful*/
							
						//Confirm to the user that their data has been successfully processed
						//Provide a link to the home page and the select page
						$resultMsg =  "<h1>Success</h1>";								
						$resultMsg .= "<a href = 'selectAssignment/selectEvents.php'>Go back to table</a>";
							
				}
					
				else { /*If there was an error with the query and did not execute*/
						$resultMsg = "<h1>Update error: " . $e->getMessage() . "</h1>";
						//Display message “Oops Please try again”
						//Display a link to the home page and select page
					
				}
				
		} 
			
	}

	else {
			
			header('Location: login.php');
			
	}


?>

<!DOCTYPE html>
		<html>
				<head>
				
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						
						<title>Update Event Page</title>
						
						<style>

								#orderArea	{
										width:600px;
										background-color:#CF9;
								}

								.error	{
										color:red;
										font-style:italic;	
								}

						</style>
				</head>

				<body>
				
						<h1>WDV341 Intro PHP</h1>
						
						<h1>Presenters Admin System Example</h1>
						
						<h3>UPDATE Form for Changing information on a Presenter</h3>
						
						<p>This page is called from the presentersSelectView.php page when you click on the Update link of a presenter. That page attaches the presenter_id to the URL of this page making it a GET parameter.</p>
						
						<p>This page uses that information to SELECT the requested record from the database. Then PHP is used to pull the various column values for the record and place them in the form fields as their default values. </p>
						
						<p>The user/customer can make changes as needed or leave the information as is. When the form is submitted and validated it will update the record in the database.</p>
						
						<p>Notice that this form uses a hidden field. The value of this hidden field contains the presenter_id. It is passed as one of the form name-value pairs. The submitted page will use that value to determine which record to update on the database.</p>

<?php
		//If the user submitted the form the changes have been made
		if ( isset($_POST['submitBtn']) ) {
				echo $resultMsg;	//contains a Success or Failure output content
		}//end if submitted

		else
		{	//The page needs to display the form and associated data to the user for changes
				echo $updateHeading;
?>

						<div id="orderArea">

								 <form id="form1" name="form1" method="post" action="updateForm.php?event_id=<?php echo $updateEventId?>">
																
										<p>Event Name: <span class = "error"><?php echo "$eventNameErrMsg"; ?></span><br>
												<input type="text" name="event_name" id="eventName" value="<?php echo $inEventName;?>"/>
										</p>

										<p>Event Description:  <span class = "error"><?php echo "$descriptionErrMsg"; ?></span><br>
												<textarea name="event_description" id = "eventDescription" rows="5" cols="40"><?php echo $inDescription;?></textarea>
										</p>
																  
										<p>Presenter:  <span class = "error"><?php echo "$presenterErrMsg"; ?></span><br>
												<input type="text" name="event_presenter" id="eventPresenter" value="<?php echo $inPresenter;?>"/>
										</p>
																
										<p>Event Date:  <span class = "error"><?php echo "$dateErrMsg"; ?></span><br>
												(valid format:  mm/dd/yyyy)<br>
												<input type = "text" name = "event_date" id = "eventDate" value="<?php echo $inDate;?>"/>
										</p>
																
										<p>Event Time: <span class = "error"><?php echo "$timeErrMsg"; ?></span><br>
												(valid format:  HH:MM AM/PM)<br>
												<input type = "text" name = "event_time" id = "eventTime" value="<?php echo $inTime;?>"/>
										</p>
														  					  
										<p>
												<input type="submit" name="submitBtn" id="submitBtn" value="Submit" />
												<input type="reset" name="resetBtn" id="resetBtn" value="Clear Form" />
										</p>
														  
								</form>
								
								<p>
										<a href = "selectAssignment/selectEvents.php">Go Back to Table</a>
								</p>
								
								<p>
										<a href='logout.php'>Logout of Events Admin System</a>
								</p>

						</div>

<?php
		}//end else submitted

		$conn = null;

?>						
				</body>
				
		</html>