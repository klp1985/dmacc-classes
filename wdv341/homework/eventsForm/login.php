<?php 
	session_start();
	
	$msg = "";
		
	if (isset($_SESSION['validUser'])) {
		if ($_SESSION['validUser'] == "yes") {	//If user is logged in already
			
				$msg = "Welcome Back, " . $_SESSION['username'];
			
		}	//End validUser is true
		
		else {
				if (isset($_POST['submitLogin'])) {	//If submit button has been pushed
				
						$inUsername = $_POST['usernameLogin'];
						$inPassword = $_POST['passwordLogin'];
						
						include 'connectPDO.php';
						
						$sql = "SELECT event_user_name, event_user_password FROM event_user WHERE event_user_name = :username AND event_user_password = :password;";
						

						
							$stmt = $conn->prepare($sql);
							
							$stmt->bindParam(':username', $inUsername);
							$stmt->bindParam(':password', $inPassword);
							
							$stmt->execute();
						
					
						
						if ($stmt->execute()){  /*If select query was successful and there is content*/
					//Place content from database into form fields
					//Display the form to the user with content in the fields
					
							while($row = $stmt->fetch()) {
								
									$username = $row['event_user_name'];
									$password = $row['event_user_password'];		
						
							}
						}
						
						if ($stmt->rowCount() == 1) {
							
								$_SESSION['validUser'] = "yes";								//this is a valid user so set your SESSION variable
								$_SESSION['username'] = $username;
								$msg = "Welcome Back! $username";
							
						}
						
						else {
							
								$_SESSION['validUser'] = "no";					
								$msg = "Sorry, there was a problem with your username or password. Please try again.";
							
						}
						
						$conn = null;
				
				}  //End if submitted
				
				else {
					
				}
		}
	}	
		
	else{
		$_SESSION['validUser'] = "";
	}
?>

<!DOCTYPE html>
		<html>
				<head>
				
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
						
						<title>WDV341 Intro PHP - Login and Control Page</title>

				</head>

				<body>

						<h1>WDV341 Intro PHP</h1>

						<h2>Presenters Admin System Example</h2>

						<h2><?php echo $msg?></h2>

<?php
	if ($_SESSION['validUser'] == "yes")	{	//This is a valid user.  Show them the Administrator Page		
//turn off PHP and turn on HTML
?>
						<h3>Presenters Administrator Options</h3>
						<p><a href="eventsForm.php">Input New Event</a></p>
						<p><a href="selectAssignment/selectEvents.php">List of Events</a></p>
						<p><a href="logout.php">Logout of Events Admin System</a></p>	
        					
<?php
	}
	else {								//The user needs to log in.  Display the Login Form	
?>
						<h2>Please login to the Administrator System</h2>
							<form method="post" name="loginForm" action="login.php" >
							  <p>Username: <input name="usernameLogin" type="text" /></p>
							  <p>Password: <input name="passwordLogin" type="password" /></p>
							  <p><input name="submitLogin" value="Login" type="submit" /> <input name="" type="reset" />&nbsp;</p>
							</form>
                
<?php //turn off HTML and turn on PHP
		
	}//end of checking for a valid user
		
//turn off PHP and begin HTML			
?>
				</body>
		</html>