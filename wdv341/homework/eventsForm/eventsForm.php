<?php
	session_start();

				$validForm = false;
				
				$inEventName = "";
				$inDescription = "";
				$inPresenter = "";
				$inDate = "";
				$inTime = "";
				$dateToDB = "";
				$timeToDB = "";
				
				$resultMsg = "";
				
				$eventNameErrMsg = "";
				$descriptionErrMsg = "";
				$presenterErrMsg = "";
				$dateErrMsg = "";
				$timeErrMsg = "";
				
				
				
				
				function validateEventName() {
					
						global $inEventName, $validForm, $eventNameErrMsg;
						
						$eventNameErrMsg = "";
						
						if ( !$inEventName == "") {
							
								$inEventName = ltrim($inEventName);
						
						}
						
						else {
								
								$validForm = false;
								
								$eventNameErrMsg = "Event name is required.";
								
						}
				
				}
				
			
				
				function validateDescription() {
				
						global $inDescription, $validForm, $descriptionErrMsg;
						
						$descriptionErrMsg = "";
						
						if ( $inDescription == "") {
								
								$validForm = false;
								
								$descriptionErrMsg = "An event description must be entered.";
						
						}
								
				
				}
				
				
				
				function validatePresenter() {
					
						global $inPresenter, $validForm, $presenterErrMsg;
						
						$presenterErrMsg = "";
						
						if ( $inPresenter == "") {
								
								$validForm = false;
								
								$presenterErrMsg = "Enter the name of a presenter.";
						
						}
				
				}
				
				
				
				function validateDate() {
					
						global $inDate, $validForm, $dateErrMsg, $dateToDB;
					
						$date_regex = '^((0?[13578]|10|12)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[01]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1}))|(0?[2469]|11)(-|\/)(([1-9])|(0[1-9])|([12])([0-9]?)|(3[0]?))(-|\/)((19)([2-9])(\d{1})|(20)([01])(\d{1})|([8901])(\d{1})))$^';
						
						if( !$inDate == "") {
						
								if(!preg_match($date_regex, $inDate)) {
									
										$validForm = false;
							
										$dateErrMsg = "The date format is not incorrect.";
						  
								} else {

										$dateToDB = date('Y-m-d', strtotime($inDate));  //If the date is valid, converts the date to the format YYYY-MM-DD for entry into the database.
								
								}
								
						} else {
								
								$validForm = false;
								
								$dateErrMsg = "An event date is required.";
						
						}
					
				}
				
				
				
				function validateTime() {
					
						global $inTime, $validForm, $timeErrMsg, $timeToDB;
					
						$time_regex = '/^(1[0-2]|0?[1-9]):[0-5][0-9] (AM|PM)$/i';
						
						if( !$inTime == "") {
						
								if(!preg_match($time_regex, $inTime)) {
									
										$validForm = false;
							
										$timeErrMsg = "The time format is incorrect.";
								} else {
										
										$timeToDB = date("H:i", strtotime($inTime));  //If the time is valid, converts time to the 24 hour format for entry into the database.
										
								}
								
						} else {
								
								$validForm = false;
								
								$timeErrMsg = "The time of the event is required.";
						
						}
					
				}
			
	if ($_SESSION['validUser'] == "yes") {
		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
				
				$inEventName = $_POST['event_name'];
				$inDescription = $_POST['event_description'];
				$inPresenter = $_POST['event_presenter'];
				$inDate = $_POST['event_date'];
				$inTime = $_POST['event_time'];
				
				$validForm = true;
				
				validateEventName();
				validateDescription();
				validatePresenter();
				validateDate();
				validateTime();
		}
		
		if ($validForm) {			//If the form has been entered and validated a confirmation page is displayed in the VIEW area.
						
						include "connectPDO.php";  //Connects to the database and inserts the data
						
						try {
								$sqlPrepare = $conn->prepare("INSERT INTO wdv341_event (event_name, event_description, event_presenter, event_date, event_time)
								VALUES (:event_name, :event_description, :event_presenter, :event_date, :event_time)");
								
								$sqlPrepare->bindParam(':event_name', $inEventName);
								$sqlPrepare->bindParam(':event_description', $inDescription);
								$sqlPrepare->bindParam(':event_presenter', $inPresenter);
								$sqlPrepare->bindParam(':event_date', $dateToDB);
								$sqlPrepare->bindParam(':event_time', $timeToDB);
						}
						
						catch (PDOException $e) {
								echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
						}
						
						$conn = null;
						
						if ($sqlPrepare->execute()){
							
							$resultMsg = "<h3>Thank You!</h3>";
						
							$resultMsg .= "<p>Your event information has been processed.</p>";
						
							$resultMsg .= "<p>&nbsp</p>";
						
							$resultMsg .= "<p><a href = 'eventsForm.php'>Enter Another Event</a></p>";
							
							$resultMsg .= "<p><a href = 'selectAssignment/selectEvents.php'>View Events</a></p>";
							
							$resultMsg .= "<p><a href='logout.php'>Logout of Events Admin System</a></p>";
							
						}
						
						else {
							
							$resultMsg = "<h3>There Was a Problem</h3>";
								
							$resultMsg .= "<p>An error occurred while processing your data.</p>";
								
							$resultMsg .= "<p>Please try again</p>";
								
							$resultMsg .= "<p><a href = 'eventsForm.php'>Back to Event Form</a></p>";
							
							$resultMsg .= "<p><a href='logout.php'>Logout of Events Admin System</a></p>";
							
						}
		}
	}

	else {
			
			header('Location: login.php');
			
	}
?>
<!DOCTYPE html>
		<html >
				<head>			
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>WDV341 Intro PHP - Add Event</title>
					
						<style>
								#orderArea	{
									width:600px;
									background-color:#CF9;
								}

								.error	{
									color:red;
									font-style:italic;	
								}

						</style>				
				</head>

				<body>
						<h1>WDV341 Intro PHP</h1>
						<h2>Add Event Form</h2>			
<?php								
		if ( isset($_POST['submitBtn']) ) {
				echo $resultMsg;	//contains a Success or Failure output content
		}//end if submitted
		
		if (!$validForm) {
			//The page needs to display the form and associated data to the user for changes
				
?>						

						<div id="orderArea">

						  <form id="form1" name="form1" method="post" action="eventsForm.php">
								
								<p>Event Name: <span class = "error"><?php echo "$eventNameErrMsg"; ?></span><br>
										<input type="text" name="event_name" id="eventName" value="<?php echo $inEventName;?>"/>
								</p>

								<p>Event Description:  <span class = "error"><?php echo "$descriptionErrMsg"; ?></span><br>
										<textarea name="event_description" id = "eventDescription" rows="5" cols="40"><?php echo $inDescription;?></textarea>
								</p>
								  
								<p>Presenter:  <span class = "error"><?php echo "$presenterErrMsg"; ?></span><br>
										<input type="text" name="event_presenter" id="eventPresenter" value="<?php echo $inPresenter;?>"/>
								</p>
								
								<p>Event Date:  <span class = "error"><?php echo "$dateErrMsg"; ?></span><br>
										(valid format:  mm/dd/yyyy)<br>
										<input type = "text" name = "event_date" id = "eventDate" value="<?php echo $inDate;?>"/>
								</p>
								
								<p>Event Time: <span class = "error"><?php echo "$timeErrMsg"; ?></span><br>
										(valid format:  HH:MM AM/PM)<br>
										<input type = "text" name = "event_time" id = "eventTime" value="<?php echo $inTime;?>"/>
								</p>
						  
						  
							    <p>
										<input type="submit" name="submitBtn" id="submitBtn" value="Submit" />
										<input type="reset" name="resetBtn" id="resetBtn" value="Clear Form" />
							    </p>
						  
						</form>
						
						<p><a href="selectAssignment/selectEvents.php">View Events</a></p>	
						
						<p><a href="logout.php">Logout of Events Admin System</a></p>	

						</div>
						
<?php
		}
?>
				</body>
		</html>