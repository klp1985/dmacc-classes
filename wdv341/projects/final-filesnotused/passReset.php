<?php
	session_start();

	if (!isset($_SESSION['validUser'])) {
			$_SESSION['validUser'] = "";
	}

	echo $_SESSION['user_id'];

	echo $_SESSION['username'];

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
	<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

  <style>

	.containField {
		display: none;
	}

    /* Remove the navbar's default rounded borders and increase the bottom margin */
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }

    /* Remove the jumbotron's default bottom margin */
     .jumbotron {
      margin-bottom: 0;
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

<div class="jumbotron">
  <div class="container text-center">
    <h1>Online Store</h1>
    <p>Mission, Vission & Values</p>
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="storeHome.php">Home</a></li>
        <li><a href="storeProducts.php">Products</a></li>
        <li><a href="storeContact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="login.php"><span class="glyphicon glyphicon-user"></span> <?php if ($_SESSION['validUser'] == "yes") {echo $_SESSION['username'];} else{echo "Your Account";}?></a></li>
        <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container" id = "passResetForm">

							<div class="omb_login">
								<h3 class="omb_authTitle">Reset Your Password</h3>
								<p>Please enter your email address to continue:</p>




								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-6">
										<form class="omb_loginForm" method="post" name="passResetForm" action="login.php" autocomplete="off" >
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="email" placeholder="Email Address">
											</div>
											<span class="help-block"></span>

											<p class = "containField">
													<label>Phone:  <br>
													  <input type="text" name="phone" id="phone">
													</label>
												  </p>

											<button class="btn btn-lg btn-primary btn-block" name = "submitBtn" type="submit">Submit</button>
										</form>
									</div>
								</div>
								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-3">

									</div>
									<div class="col-xs-12 col-sm-3">
										<p class="omb_forgotPwd">
											<a href="#">Forgot password?</a>
										</p>
									</div>
								</div>
							</div>

						</div>

			</body>
		</html>
