<?php
		session_start();

		if (!isset($_SESSION['validUser'])) {
				$_SESSION['validUser'] = "";
		}

		$validForm = false;

				$inName = "";
				$inEmail = "";
				$contactDate = "";
				$nameErrMsg = "";
				$emailErrMsg = "";

				$emailMsg = "";
				$msg = "";

				function sendEmail() {

						global $inEmail, $emailMsg;

						$subject = "E-Comm Store:  Reset Your Password";

						$fromEmail = "web@kraigpopelka.info";

						$infoText = "Form Data\n\n ";
								foreach($_POST as $key => $value)
								{
									$infoText.= $key."=".$value."\n";
								}

						$msgText = "Dear " . $_SESSION['username'] . "," . "\r\n";

						$msgText .= "\r\n";

						$msgText .= "Please find the link below to reset your password.";

						$msgText .= "localhost/wdv341/projects/final/passReset.php?ecomm_user_id=" . $_SESSION['user_id'];

						$msgText .= "\r\n";

						$msgText .= "Regards," . "\r\n";

						$msgText .= "\r\n";

						$msgText .= "E-Comm Support Team" . "\r\n";

						$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";  //Allows the email to be created with HTML

						$headers .= "From: $fromEmail" . "\r\n";				//Creates the From header with the appropriate address

						$headers .= 'Bcc: kpopelka@gmail.com' . "\r\n";


						if (mail($inEmail,$subject,$msgText,$headers)) {	//Sends a confirmation email to the person that is initiating the contact.  A blind copy is sent to the address that will respond to the email.

								$emailMsg = "<h5 style = 'color: #42f442; text-align: center; margin: 15px;'>Confirmation: Your password reset request was successful!</h5><p style = 'text-align: center; margin: 15px;'>You should receive an email shortly at $inEmail containing a link to change your password.</p><hr/><p><a href = 'storeHome.php'>Back to home page</a></p>";
						}

						else {
								$emailMsg = "<p style = 'color: red; text-align: center; margin 15px;'>ERROR: There was a problem sending your message.  Please try again.</p>";
						}

				}


				function validateName() {

						global $inName, $validForm, $nameErrMsg;

						$nameErrMsg = "";

						if ( !$inName == "") {

								$inName = ltrim($inName);

								$inName = filter_var($inName, FILTER_SANITIZE_STRING);


						}

						else {

								$validForm = false;

								$nameErrMsg = "Username is required.";

						}

				}

				function validateEmail() {

						global $inEmail, $validForm, $emailErrMsg;

						$emailErrMsg = "";

						if ( !$inEmail == "") {

								$inEmail = filter_var($inEmail, FILTER_SANITIZE_EMAIL);


								if (filter_var($inEmail, FILTER_VALIDATE_EMAIL) === false) {

										$validForm = false;

										$emailErrMsg = "Email address is not formatted correctly.  Use the format jdoe@example.com";

								}

						}

						else {

								$validForm = false;

								$emailErrMsg = "Email address is required.";

						}

				}

				function validatePhauxn() {

						global $inPhone, $validForm;

						if (!$inPhone == "") {

								$validForm = false;

						}

				}


		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.



				$inEmail = $_POST['email'];
				$inName = $_POST['username'];
				$inPhone = $_POST['phone'];

				$validForm = true;

				validateName();
				validateEmail();
				validatePhauxn();

				if ($validForm) {

						include 'connectPDO.php';

						$sql = "SELECT ecomm_user_id, ecomm_user_email, ecomm_username FROM ecomm_user WHERE ecomm_user_email = :user_email AND ecomm_username = :username";

						$stmt = $conn->prepare($sql);

						$stmt->bindParam(':user_email', $inEmail);
						$stmt->bindParam(':username', $inName);

						$stmt->execute();



						if ($stmt->execute()){  /*If select query was successful and there is content*/
					//Place content from database into form fields
					//Display the form to the user with content in the fields

							while($row = $stmt->fetch()) {

									$userId = $row['ecomm_user_id'];
									$username = $row['ecomm_username'];
									$email = $row['ecomm_user_email'];

							}
						}

						if ($stmt->rowCount() == 1) {

								$_SESSION['username'] = $inName;
								$_SESSION['user_id'] = $userId;

								sendEmail();


						}

						else {


								$msg = "<h3>Sorry, That email address was not found. Please try again.</h3>";

						}

						$conn = null;

				}
		}

?>



<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
	<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>
  <style>

	.containField {
		display: none;
	}

	.error	{
									color:red;
									font-style:italic;
								}

    /* Remove the navbar's default rounded borders and increase the bottom margin */
    .navbar {
      margin-bottom: 50px;
      border-radius: 0;
    }

    /* Remove the jumbotron's default bottom margin */
     .jumbotron {
      margin-bottom: 0;
    }

    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
  </style>
</head>
<body>

<div class="jumbotron">
  <div class="container text-center">
    <h1>Online Store</h1>
    <p>Mission, Vission & Values</p>
  </div>
</div>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="storeHome.php">Home</a></li>
        <li><a href="storeProducts.php">Products</a></li>
        <li><a href="storeContact.php">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
				<li>
								 <div class="col-sm-12 col-md-12">
									<form class="navbar-form" role="search">
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search" name="q">
										<div class="input-group-btn">
											<button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
										</div>
									</div>
									</form>
								</div>
								</li>
								<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
				<?php
										if ($_SESSION['validUser'] == "yes") {
											echo $_SESSION['fullname'];
										} else{
											echo "Your Account";
										}

										if ($_SESSION['validUser'] == "yes") {
				?>
												<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
													<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
													<li><a class="dropdown-item" href="logout.php">Logout</a></li>
												</ul>
				<?php
										} else {
				?>
											<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
												<li><a class="dropdown-item"  href="login.php">Login</a></li>
											</ul>
				<?php
										}
				?>
								</a></li>

								<li><a href="login.php">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
								</ul>
							</div>
							</div>
						</nav>

<?php
					if ($validForm) {
						if ($stmt->rowCount() == 1) {
							echo $emailMsg;
						}

						else {

							echo $msg;
?>
								<div class="container" id = "passResetForm">

							<div class="omb_login">
								<h3 class="omb_authTitle">Reset Your Password</h3>
								<p>Please enter your username and email address to continue:</p>




								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-6">
										<form class="omb_loginForm" method="post" name="passResetForm" action="passResetForm.php" autocomplete="off" >

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $inName;?>"><br>

											</div>
											<span class="help-block"><p><span class = "error"><?php echo "$nameErrMsg"; ?></span></p></span>

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="email" placeholder="Email Address" value="<?php echo $inEmail;?>"><br>

											</div>
											<span class="help-block"><p><span class = "error"><?php echo "$emailErrMsg"; ?></span></p></span>

											<p class = "containField">
													<label>Phone:  <br>
													  <input type="text" name="phone" id="phone">
													</label>
												  </p>

											<button class="btn btn-lg btn-primary btn-block" name = "submitBtn" type="submit">Submit</button>
										</form>
									</div>
								</div>
								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-3">

									</div>
								</div>
							</div>

						</div>
<?php
						}

					}

					else {


?>

						<div class="container" id = "passResetForm">

							<div class="omb_login">
								<h3 class="omb_authTitle">Reset Your Password</h3>
								<p>Please enter your username and email address to continue:</p>




								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-6">
										<form class="omb_loginForm" method="post" name="passResetForm" action="passResetForm.php" autocomplete="off" >

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="username" placeholder="Username" value="<?php echo $inName;?>"><br>

											</div>
											<span class="help-block"><p><span class = "error"><?php echo "$nameErrMsg"; ?></span></p></span>

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="email" placeholder="Email Address" value="<?php echo $inEmail;?>"><br>

											</div>
											<span class="help-block"><p><span class = "error"><?php echo "$emailErrMsg"; ?></span></p></span>

											<p class = "containField">
													<label>Phone:  <br>
													  <input type="text" name="phone" id="phone">
													</label>
												  </p>

											<button class="btn btn-lg btn-primary btn-block" name = "submitBtn" type="submit">Submit</button>
										</form>
									</div>
								</div>
								<div class="row omb_row-sm-offset-3">
									<div class="col-xs-12 col-sm-3">

									</div>
								</div>
							</div>

						</div>

<?php

					} // End else loop

?>

			</body>
		</html>
