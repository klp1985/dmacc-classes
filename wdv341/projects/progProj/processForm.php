<?php

		$toEmail = $_POST['email'];		
		
		$inName = $_POST['fullName']; 
		
		$inReason = $_POST['reason'];
		
		$inComments = $_POST['comments'];

		$sendInfo = "kpopelka@gmail.com";
	
		$subject = "Your recent contact to Kraig Popelka - Web Developer";	

		$fromEmail = "web@kraigpopelka.info";		
		
		$infoText = "Form Data\n\n ";			
				foreach($_POST as $key => $value)											
				{
					$infoText.= $key."=".$value."\n";	
				} 

		$msgText = "Dear " . $inName . "," . "\r\n";
								
		$msgText .= "\r\n";
								
		$msgText .= "Thank you very much for your recent contact regarding " . $inReason . ".  ";  
								
		$msgText .= "Your comment:  '" . $inComments . "' has been received and you should receive a prompt response at " . $toEmail . "." . "\r\n";

		$msgText .= "\r\n";
								
		$msgText .= "Sincerely," . "\r\n";
								
		$msgText .= "\r\n";
								
		$msgText .= "Kraig Popelka" . "\r\n";
		
		$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";  //Allows the email to be created with HTML
		
		$headers .= "From: $fromEmail" . "\r\n";				//Creates the From header with the appropriate address
		
		$headers .= 'Bcc: kpopelka@gmail.com' . "\r\n";
		
		mail ($sendInfo, $subject, $infoText, $headers);  //Sends just the inputed data to email address that will respond to the contact message.

		if (mail($toEmail,$subject,$msgText,$headers)) {	//Sends a confirmation email to the person that is initiating the contact.  A blind copy is sent to the address that will respond to the email.
	
				echo("<h2 style = 'color: #42f442; text-align: center; margin: 15px;'>Confirmation: Your information was successfully sent!</h2>");
		} 
		
		else {
				echo("<h2 style = 'color: red; text-align: center; margin 15px;'>ERROR: There was a problem sending your information.  Please try again.</p>");
		}
		
		

?>




<!doctype html>

<html>

		<head>

				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

				<title>Contact - Confirmation</title>
				
				<link href = "../files/port_style_all.css" rel = "stylesheet" type = "text/css" />

				<style type="text/css">
						
						h1 {
								margin: 20px 25px;
						}
						
						#container {
								border-top: 2px solid #e1ddb7;
						}
						
				</style>

		</head>
		
		<body>
		
				
				<section id = "container">
				
				<?php echo "<h1>Thank You, " . $_POST['fullName'] . "!</h1>" ?>
				
				<p>Your message of "<?php echo $_POST['comments'];?>" regarding <?php echo $_POST['reason'];?> has been received.
				
				<p>You should receive an email confirmation shortly at <?php echo $_POST['email'];?> and you can expect a prompt response.</p>

				
				
				<hr />
							
				<p><a href = "../index.html">Back to home page</a></p>
				

				</section>
				
		</body>

</html>