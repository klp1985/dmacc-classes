<?php
		session_start();

		if (!isset($_SESSION['validUser'])) {
				$_SESSION['validUser'] = "";
		}

    $productNumber = $_GET['product_number'];

    include 'connectPDO.php';
		//Get product information from products table based on value of Get variable
    $sql = "SELECT product_title, product_price, product_description, product_image_path, product_type, product_number FROM ecomm_products WHERE product_number = $productNumber";

		try {

				  $stmt = $conn->prepare($sql);
				  $stmt->execute();

				  if ($stmt->execute()) {
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Products - Outdated Phones</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
	<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

  <style>
body {

  overflow-x: hidden; }

img {
  max-width: 100%; }

.card {
  margin-top: 50px;
  background: #eee;
  padding: 3em;
  line-height: 1.5em; }

.add-to-cart {
	background-color: #1cc2ff;
	color: white;
}

.product-title {
	border-bottom: 1px solid #c9c9c9;
}

.price {
	margin-top: 45px;
	border-bottom: 4px solid #727272;
}
  </style>

</head>


<body>

			<div class="jumbotron">
			  <div class="container text-center">
			    <h1>Outdated Phones</h1>
			    <p>We sell everything but smartphones!</p>
			  </div>
			</div>

			<nav class="navbar navbar-inverse">
			  <div class="container-fluid">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
			    </div>
			    <div class="collapse navbar-collapse" id="myNavbar">
			      <ul class="nav navbar-nav">
			        <li><a href="storeHome.php">Home</a></li>
			        <li><a href="storeProducts.php">Products</a></li>
			        <li><a href="storeContact.php">Contact</a></li>
			      </ul>
			      <ul class="nav navbar-nav navbar-right">
									<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

		
						<li>
						<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">
								<!-- Identify your business so that you can collect the payments. -->
								<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

								<!-- Specify a PayPal shopping cart View Cart button. -->
								<input type="hidden" name="cmd" value="_cart">
								<input type="hidden" name="display" value="1">

								<!-- Display the View Cart button. -->
								<input type="image" name="submit" 
									src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
								alt="Add to Cart" style="margin-top:10px;">
								<img alt="" width="1" height="1"
									src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
							</form>
						</li>
						</ul>
					</div>
					</div>
				</nav>

<?php
        						while($row = $stmt->fetch()) {

        								$productTitle = $row['product_title'];
        								$productPrice = $row['product_price'];
        								$productDescription = $row['product_description'];
        								$productType = $row['product_type'];
        								$productNumber = $row['product_number'];
                        $productImagePath = $row['product_image_path'];
        								$productPrice = floatVal($productPrice);
        								$productPrice = number_format($productPrice, 2);

?>
		        <div class = "container">
		            <div class="card">
					         <div class="container-fliud">
						             <div class="wrapper row">
						                   <div class="preview col-md-6">
								                   <div class="preview-pic tab-content">
								                       <div class="tab-pane active" id="pic-1"><img src=<?php echo $row['product_image_path'];?> alt="Image"></div>
								                   </div>
							                 </div>
							                 <div class="details col-md-6">
								                   <h3 class="product-title"><?php echo $productTitle;?></h3>
		      						             <p class="product-description">Product Description:</p>
																	 <p style = "margin-left: 15px;"><?php echo $productDescription;?></p>
		      						             <h4 class="price">Price: <span><?php echo "$".$productPrice;?></span></h4>
		      						            
																	 <!-- Paypal Buy Now button -->
										<?php 
											if ($productTitle == "Wired Phone 5") {
										?>
											<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
											<input type="hidden" name="cmd" value="_s-xclick">
											<input type="hidden" name="hosted_button_id" value="HBFU9JKATZ4GL">
											

										<?php
											} else {
										?>

												 <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">

												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

												<!-- Specify a Buy Now button. -->
												<input type="hidden" name="cmd" value="_xclick">

												<!-- Specify details about the item that buyers will purchase. -->
												<input type="hidden" name="item_name" value="<?php echo $productTitle; ?>">
												<input type="hidden" name="amount" value="<?php echo $productPrice; ?>">
												<input type="hidden" name="currency_code" value="USD">

									<?php
											}
									?>	
														<!-- Display the Paypal Buy Now payment button. -->
												<input type="image" name="submit" border="0"
												src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_buynow_107x26.png"
												alt="Buy Now">
												<img alt="" border="0" width="1" height="1"
												src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
											</form>

										


									<?php
										if ($productTitle == "Flip Phone 2") {
									?>
												<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">

												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">
												
												<input type="hidden" name="item_name" value="<?php echo $productTitle; ?>">
												<input type="hidden" name="currency_code" value="USD">
												<!-- Specify a PayPal Shopping Cart Add to Cart button. -->
												<input type="hidden" name="cmd" value="_cart">
												<input type="hidden" name="add" value="1">		
												<input type="hidden" name="button_subtype" value="products">
												<input type="hidden" name="no_note" value="0">
												<input type="hidden" name="add" value="1">
												<input type="hidden" name="bn" value="PP-ShopCartBF:btn_cart_LG.gif:NonHostedGuest">
												<table>
												<tr><td><input type="hidden" name="on0" value="Colors">Colors</td></tr><tr><td><select name="os0">
													<option value="Red">Red $85.00 USD</option>
													<option value="Green">Green $85.00 USD</option>
													<option value="Blue">Blue $85.00 USD</option>
												</select> </td></tr>
												</table>
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="option_select0" value="Red">
												<input type="hidden" name="option_amount0" value="85.00">
												<input type="hidden" name="option_select1" value="Green">
												<input type="hidden" name="option_amount1" value="85.00">
												<input type="hidden" name="option_select2" value="Blue">
												<input type="hidden" name="option_amount2" value="85.00">
												<input type="hidden" name="option_index" value="0">

										<?php 
											} else if ($productTitle == "Cordless Phone 1") {
										?>
											
												<form target="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="H7MJGC9HGSCEL">
												<table>
												<tr><td><input type="hidden" name="on0" value="Option">Option</td></tr><tr><td><select name="os0">
													<option value="Handset & Base">Handset & Base $34.00 USD</option>
													<option value="Handset Only">Handset Only $14.00 USD</option>
													<option value="Base Only">Base Only $20.00 USD</option>
												</select> </td></tr>
												</table>
												<input type="hidden" name="currency_code" value="USD">

										<?php 
											} else if ($productTitle == "Wired Phone 2") {
										?>
												<form target="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
													<input type="hidden" name="cmd" value="_s-xclick">
													<input type="hidden" name="hosted_button_id" value="ZQ8C39TVPKS8U">
													<table>
													<tr><td><input type="hidden" name="on0" value="Add Custom Engraving">Add Custom Engraving</td></tr><tr><td><input type="text" name="os0" maxlength="200"></td></tr>
													</table>
													
												
										
										<?php 
											} else if ($productTitle == "Wired Phone 5") {
										?>
												<form target="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="4T7BMESAGATRL">
												
												

										<?php
											} else {
										?>
												<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">

												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

												<!-- Specify a PayPal Shopping Cart Add to Cart button. -->
												<input type="hidden" name="cmd" value="_cart">
												<input type="hidden" name="add" value="1">

												<!-- Specify details about the item that buyers will purchase. -->
											  <input type="hidden" name="item_name" value="<?php echo $productTitle; ?>">
												<input type="hidden" name="currency_code" value="USD">
												<input type="hidden" name="amount" value="<?php echo $productPrice; ?>">
										<?php
											}
										?>

												<!-- Display the payment button. -->
												<input type="image" name="submit"
													src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_addtocart_120x26.png"
													alt="Add to Cart">
												<img alt="" width="1" height="1"
													src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
											</form>


		                           </div>
							           </div>
						       </div>
					    </div>
				  </div>
<?php
       						}
       						$conn = null;
       				}
       				else {
       						$displayMsg = "There was an error processing your request.";
       				}
       		}
       		catch (PDOException $e) {
       			  echo "there was an error with your request" . $e->getMessage();
       		}
?>

        <footer class="container-fluid text-center">
          <p>WDV 341: Intro to PHP Final Project</p>

        </footer>

      </body>
      </html>
