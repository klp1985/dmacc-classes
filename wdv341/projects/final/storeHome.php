<?php
		session_start();

		include 'classes/validations.php';

		$validForm = false;

		$dupeEmail = false;

		if (!isset($_SESSION['validUser'])) {
				$_SESSION['validUser'] = "";
		}

		$inEmail = "";
		$emailErrMsg = "";
		$resultMsg = "";

		$resultEmail = "";
		$resultPhone = "";

				if ( isset($_POST['signupSubmit']) ) {
						//Brings in Validation class
						$validations = new Validations();

						$inEmail = $_POST['email'];

						$validForm = true;

						$validations->set_email($inEmail);
						$validations->set_validForm($validForm);

						$resultEmail = $validations->validateEmail();

						$validForm = $validations->get_validForm();
						$inEmail = $validations->get_email();
				}

				if ($validForm) {
						include 'connectPDO.php';

						//Check the newsletter table for a duplicate email address.
						$sqlSelect = "SELECT newsletter_email FROM ecomm_newsletter WHERE newsletter_email = :email_signup;";

						$stmt = $conn->prepare($sqlSelect);

							$stmt->bindParam(':email_signup', $inEmail);

							$stmt->execute();

						if ($stmt->execute()){  /*If select query was successful and there is content*/
					//Place content from database into form fields
					//Display the form to the user with content in the fields

							while($row = $stmt->fetch()) {
									$emailSignup = $row['newsletter_email'];
							}
						}

						if ($stmt->rowCount() == 1) { // If a row is returned that means a duplicate email address was found.

								$dupeEmail = true;

								$resultMsg = "<p class = 'error'>The email you entered is already receiving our newsletter.  Please try again or if you think this is an error please <a href = 'storeContact.php'>contact us</a> and we will help you with this issue.</p>";
						}

						else { // If no duplicate, insert submitted email into newsletter table.
								$sqlInsert = "INSERT INTO ecomm_newsletter (newsletter_email) VALUES (:newsletter_email);";

								try {
										$sqlPrepare = $conn->prepare($sqlInsert);

										$sqlPrepare->bindParam(':newsletter_email', $inEmail);

								}

								catch (PDOException $e) {
										$resultMsg = "There was a problem entering the information.  Please try again: " . $e->getMessage();
								}



								if ($sqlPrepare->execute()){

									$resultMsg = "<h4>Thank You for signing up for our newsletter!</h4>";

								}

								else {
									$resultMsg .= "<p>An error occurred while processing your data.</p>";

									$resultMsg .= "<p>Please try again</p>";
								}
					}
					$conn = null;
				}

				//-------------------------------------------------------------

				include 'connectPDO.php';

				//Get product type and thumbnail image of products for "Sort by Product Type" links.
				$sql = "SELECT product_type, product_thumb_path, product_number FROM ecomm_products GROUP BY product_type";

				try {

						$prodstmt = $conn->prepare($sql);
						$prodstmt->execute();

						if ($prodstmt->execute()) {

?>

<!DOCTYPE html>

	<html lang="en">

		<head>
				  <title>Outdated Phones</title>
				  <meta charset="utf-8">
				  <meta name="viewport" content="width=device-width, initial-scale=1">

				  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
				  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
				  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
					<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
				  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
				  <link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

				  <style>
				  .carousel-inner > .item > img,
				  .carousel-inner > .item > a > img {
					  width: 80%;
					  max-width: 933px;
					  max-height: 700px;
					  margin: 0 auto;
				  }

				</style>
		</head>
		<body>

				<div class="jumbotron">
				  <div class="container text-center">
					<h1>Outdated Phones</h1>
					<p>We Sell Everything But Smartphones</p>
				  </div>
				</div>
		
				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
					  <ul class="nav navbar-nav">
						<li class="active"><a href="storeHome.php">Home</a></li>
						<li><a href="storeProducts.php">Products</a></li>
						<li><a href="storeContact.php">Contact</a></li>
					  </ul>
					  <ul class="nav navbar-nav navbar-right">
							<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

		
							<li>
								<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
										<!-- Identify your business so that you can collect the payments. -->
										<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

										<!-- Specify a PayPal shopping cart View Cart button. -->
										<input type="hidden" name="cmd" value="_cart">
										<input type="hidden" name="display" value="1">

										<!-- Display the View Cart button. -->
										<input type="image" name="submit" 
											src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
										alt="Add to Cart" style="margin-top:10px;">
										<img alt="" width="1" height="1"
											src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
									</form>
							</li>
					  </ul>
					</div>
				  </div>
				</nav>
				
				

				  <div id="myCarousel" class="carousel slide" data-ride="carousel">
					<!-- Indicators -->
					<ol class="carousel-indicators">
					  <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					  <li data-target="#myCarousel" data-slide-to="1"></li>
					  <li data-target="#myCarousel" data-slide-to="2"></li>
					  <li data-target="#myCarousel" data-slide-to="3"></li>
					  <li data-target="#myCarousel" data-slide-to="4"></li>
					</ol>
					
					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
					  <div class="item active">
						<img src="img/site/stock5.jpg" alt="Chania" width="690" height="516">
					  </div>

					 <div class="item">
						<img src="img/site/stock3.jpg" alt="Chania" width="690" height="516">
					  </div>

					  <div class="item">
						<img src="img/site/stock4.jpg" alt="Chania" width="690" height="516">
					  </div>

					  <div class="item">
						<img src="img/site/stock7.jpg" alt="Chania" width="690" height="516">
					  </div>


					  <div class="item">
						<img src="img/site/stock9.jpg" alt="Chania" width="690" height="516">
					  </div>
					</div>

					<!-- Left and right controls -->
					<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
					  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
					  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					  <span class="sr-only">Next</span>
					</a>
				  </div>

				<br>

				<div class="container">
					<hr/>
					<h2>Shop by Phone Type:</h2>

					<div class="container">
							<div class="row">
<?php
											while($row = $prodstmt->fetch()) { //Create a link for each product type and include thumbnail image.

													$productThumbPath = $row['product_thumb_path'];
													$productType = $row['product_type'];

													$prodTypeNoSpace = str_replace(' Phone','',$productType);

?>
															<div class="col-sm-3">
																<a href = "storeProducts.php?product_type=<?php echo $prodTypeNoSpace;?>">
																	<div class="panel panel-primary">
																		<div class="panel-heading"><?php echo $productType;?></div>
																		<div class="panel-body"><img src=<?php echo $row['product_thumb_path'];?> class="img-responsive" style="width:60%; margin: 0 auto;" alt="Image"></div>
																	</div>
																</a>
															</div>
<?php
											}
											$conn = null;
									} else {
											$displayMsg = "There was an error processing your request.";
									}
							} catch (PDOException $e) {
									echo "there was an error with your request" . $e->getMessage();
							}
?>
					<hr/>
				</div><br><br>
				<footer class="container-fluid text-center">
<?php
					if ( isset($_POST['signupSubmit']) ) {
						echo $resultMsg;	//contains a Success or Failure output content
					}//end if submitted

					if (!$validForm || $dupeEmail) { //Show the form until a valid email address is entered and a unique email is entered.
?>
				  <p>Sign Up For Our Newsletter</p>
				  <form class="form-inline" method="post" name="newsletter-signup" action="storeHome.php">
					<input type="text" class="form-control" size="50" placeholder="Email Address" name = "email" value = "<?php echo $inEmail;?>">
					<br><span class = "error"><?php echo "$resultEmail"; ?></span><br>
					<button type="submit" class="btn btn-danger" name = "signupSubmit">Sign Up</button>
				  </form>
				  <p>&nbsp;</p>
				  <p>WDV 341: Intro to PHP Final Project</p>
				</footer>
<?php
					}
?>

		</body>

	</html>
