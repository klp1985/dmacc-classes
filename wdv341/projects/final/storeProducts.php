<?php
		session_start();

		if (!isset($_SESSION['validUser'])) {
				$_SESSION['validUser'] = "";
		}

		include 'connectPDO.php';

		if (isset($_GET['product_type'])) {  //If a product type has been passed
			$productType = $_GET['product_type'];

//Gets the products from the database based on product type
			if ($productType == "Flip") {
				$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_type = 'Flip Phone'";
			}elseif ($productType == "Brick") {
				$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_type = 'Brick Phone'";
			}elseif ($productType == "Cordless") {
				$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_type = 'Cordless Phone'";
			}elseif ($productType == "Slider") {
				$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_type = 'Slider Phone'";
			}elseif ($productType == "Wired") {
				$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products WHERE product_type = 'Wired Phone'";
			}


		}else { // If no product type is found in the Get variable then get all products from the products table
			$sql = "SELECT product_title, product_price, product_description, product_thumb_path, product_type, product_number FROM ecomm_products";
		}

		try {

				$stmt = $conn->prepare($sql);
				$stmt->execute();

				if ($stmt->execute()) {
?>
<!DOCTYPE html>
	<html lang="en">
			<head>
			  <title>Products - Outdated Phones</title>
			  <meta charset="utf-8">
			  <meta name="viewport" content="width=device-width, initial-scale=1">

			  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
			  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />

			  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
				<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

			  <style>
					.panel-heading h4 {
							border-bottom: 3px solid #727272;
					}

					.product-description {
							margin-left: 15px;
					}


			  </style>
			</head>

			<body>

				<div class="jumbotron">
				  <div class="container text-center">
				    <h1>Outdated Phones</h1>
				    <p>We sell everything but smartphones!</p>
				  </div>
				</div>

				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li><a href="storeHome.php">Home</a></li>
				        <li class="active"><a href="storeProducts.php">Products</a></li>
				        <li><a href="storeContact.php">Contact</a></li>
				      </ul>
				      <ul class="nav navbar-nav navbar-right">
						<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						
				<!--		<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>     -->
						<li>
						<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">
							<!-- Identify your business so that you can collect the payments. -->
							<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

							<!-- Specify a PayPal shopping cart View Cart button. -->
							<input type="hidden" name="cmd" value="_cart">
							<input type="hidden" name="display" value="1">

							<!-- Display the View Cart button. -->
							<input type="image" name="submit" 
								src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
							alt="Add to Cart" style="margin-top:10px;">
							<img alt="" width="1" height="1"
								src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
						</form>
						</li>
						</ul>
					</div>
					</div>
				</nav>

			<div class = "container">
				<h3>Sort By Phone Type</h3>
				<div class="btn-group sortBtns" role="group" aria-label="Sort By Phone Type">
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php?product_type=Flip">Flip</a>
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php?product_type=Brick">Brick</a>
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php?product_type=Cordless">Cordless</a>
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php?product_type=Slider">Sliders</a>
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php?product_type=Wired">Wired</a>
						<a class="btn btn-secondary indSortBtn" href="storeProducts.php">View All</a>
				</div>
			</div>
<div class="container">
		<div class="row">
<?php
						while($row = $stmt->fetch()) { //Place product information into

								$productTitle = $row['product_title'];
								$productPrice = $row['product_price'];
								$productDescription = $row['product_description'];
								$productThumbPath = $row['product_thumb_path'];
								$productType = $row['product_type'];
								$productNumber = $row['product_number'];

								$productPrice = floatVal($productPrice);
								$productPrice = number_format($productPrice, 2);
?>
										<div class="col-sm-4">
											<div class="panel panel-primary">
												<div class = "link-wrap">
													<a href = "productView.php?product_number=<?php echo $productNumber;?>">
														<div class="panel-heading"><h4><?php echo $productTitle;?></h4></div>
														<div class="panel-body"><img src=<?php echo $row['product_thumb_path'];?> class="img-responsive" width= "250px" height = "250px" style="margin: 0 auto;" alt="Image"></div>
													</a>
												</div>
												<p class = "product-description">Description:  <?php echo $productDescription;?></p>
												<div class="panel-footer">
													<h4><?php echo "$".$productPrice;?></h4><br><hr />
											

											<!-- Paypal Buy Now button -->
										<?php 
											if ($productTitle == "Wired Phone 5") {
										?>
											<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
											<input type="hidden" name="cmd" value="_s-xclick">
											<input type="hidden" name="hosted_button_id" value="HBFU9JKATZ4GL">
											

										<?php
											} else {
										?>
											<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">

												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

												<!-- Specify a Buy Now button. -->
												<input type="hidden" name="cmd" value="_xclick">

												<!-- Specify details about the item that buyers will purchase. -->
												<input type="hidden" name="item_name" value="<?php echo $productTitle; ?>">
												<input type="hidden" name="amount" value="<?php echo $productPrice; ?>">
												<input type="hidden" name="currency_code" value="USD">
										<?php
											}
										?>	

													<!-- Display the Paypal Buy Now payment button. -->
												<input type="image" name="submit" border="0"
												src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_buynow_107x26.png"
												alt="Buy Now">
												<img alt="" border="0" width="1" height="1"
												src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >
											</form>
										
										<?php 
											if ($productTitle == "Wired Phone 5") {
										?>
												<form target="paypal" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
												<input type="hidden" name="cmd" value="_s-xclick">
												<input type="hidden" name="hosted_button_id" value="4T7BMESAGATRL">
													

										<?php
											} else {
										?>

										<!--Paypal Add to Cart button -->
											<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_self">
												
												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">
												
												<!-- Specify a PayPal Shopping Cart Add to Cart button. -->
												<input type="hidden" name="cmd" value="_cart">
												<input type="hidden" name="add" value="1">
												
												<!-- Specify details about the item that buyers will purchase. -->
												<input type="hidden" name="item_name" value="<?php echo $productTitle; ?>">
												<input type="hidden" name="amount" value="<?php echo $productPrice; ?>">
										
												<input type="hidden" name="currency_code" value="USD">
										<?php
											}
										?>		
												<!-- Display the payment button. -->
												<input type="image" name="submit"
												src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_addtocart_120x26.png"
												alt="Add to Cart">
												<img alt="" width="1" height="1"
												src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
											</form>

<?php
									if ($_SESSION['validUser'] == "yes" && $_SESSION['userRole'] == 1) {
?>
													<p>&nbsp;</p>

													<p><a href = "add-updateProduct.php?prod_no=<?php echo $productNumber;?>">Update Product</a></p>
													<p><a href = "deletePage.php?keyid=<?php echo $productNumber;?>&tname=ecomm_products">Delete Product</a></p>
<?php
									}
?>
											</div>
										</div>
									</div>
<?php
						}
						$conn = null;
				} else {
						$displayMsg = "There was an error processing your request.";
				}
		} catch (PDOException $e) {
				echo "there was an error with your request" . $e->getMessage();
		}
?>
				</div>
			</div>

			<footer class="container-fluid text-center">
					<p>WDV 341: Intro to PHP Final Project</p>
			</footer>

	</body>
</html>
