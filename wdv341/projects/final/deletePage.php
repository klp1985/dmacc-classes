<?php
    session_start();
// Admin has access to delete any content.  Customer has access to delete their own account.
if ($_SESSION['validUser'] == "yes" && ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 3)) {
  // Bring in the Delete class
    include 'classes/sqlDelete.php';

    //Gets the primary key and table name
    $primaryKey = $_GET['keyid'];
    $tableName = $_GET['tname'];

    //Create delete object
    $delete = new sqlDelete();

    $delete->set_rowId($primaryKey);
    $delete->set_tableName($tableName);

    $resultMsg = $delete->deleteRowFromDB();

    if ($_SESSION['userRole'] == 3) {  //If customer deletes account immediately log them out and send them to login page.
       session_unset();	//remove all session variables related to current session
  	   session_destroy();	//remove current session

  	   header('Location: login.php');
     }

} else {  // Editors do not have access to delete anything.  Send them and non-valid users back to the login page.
    header('Location: login.php');
}
?>

<!DOCTYPE html>

	<html lang="en">

		<head>
				  <title>Delete Success - Outdated Phones</title>
				  <meta charset="utf-8">
				  <meta name="viewport" content="width=device-width, initial-scale=1">

				  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
				  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
				  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
          <link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
				  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
				  <link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

				  <style>

				</style>
		</head>
		<body>

				<div class="jumbotron">
				  <div class="container text-center">
					<h1>Outdated Phones</h1>
					<p>We Sell Everything But Smartphones</p>
				  </div>
				</div>

				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
					<div class="navbar-header">
					  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					  </button>
					  <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
					</div>
					<div class="collapse navbar-collapse" id="myNavbar">
					  <ul class="nav navbar-nav">
						<li class="active"><a href="storeHome.php">Home</a></li>
						<li><a href="storeProducts.php">Products</a></li>
						<li><a href="storeContact.php">Contact</a></li>
					  </ul>
					  <ul class="nav navbar-nav navbar-right">

						       <li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
						<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<!-- Identify your business so that you can collect the payments. -->
								<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

								<!-- Specify a PayPal shopping cart View Cart button. -->
								<input type="hidden" name="cmd" value="_cart">
								<input type="hidden" name="display" value="1">

								<!-- Display the View Cart button. -->
								<input type="image" name="submit" 
									src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
								alt="Add to Cart" style="margin-top:10px;">
								<img alt="" width="1" height="1"
									src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
							</form>				
					</li>
					  </ul>
					</div>
				  </div>
				</nav>

        <div class = "conainer">
            <?php echo $resultMsg;?>
            <p style = "text-align: center;"><a href = "login.php">Back to Admin Panel</a></p>
        </div>

        <footer class="container-fluid text-center">
          <p>Footer Text</p>
        </footer>

    </body>
</html>
