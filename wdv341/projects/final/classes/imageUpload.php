<?php
    class ImageUpload {

      //Properties
          public $filename;
          public $filesize;
          public $filetmp;
          public $filetype;
          public $thumbname;
          public $thumbsize;
          public $thumbtmp;
          public $thumbtype;
          public $productType;
          public $fullImageFilePath;
          public $thumbImageFilePath;

      //Setters

              function set_filename($inFilename) {
                  $this -> filename = $inFilename;  //Put input value into property
              }

              function set_filesize($inFilesize) {
                  $this -> filesize = $inFilesize;  //Put input value into property
              }

              function set_filetmp($inFiletmp) {
                  $this -> filetmp = $inFiletmp;  //Put input value into property
              }

              function set_filetype($inFiletype) {
                  $this -> filetype = $inFiletype;  //Put input value into property
              }

              function set_thumbname($inThumbname) {
                  $this -> thumbname = $inThumbname;  //Put input value into property
              }

              function set_thumbsize($inThumbsize) {
                  $this -> thumbsize = $inThumbsize;  //Put input value into property
              }

              function set_thumbtmp($inThumbtmp) {
                  $this -> thumbtmp = $inThumbtmp;  //Put input value into property
              }

              function set_thumbtype($inThumbtype) {
                  $this -> thumbtype = $inThumbtype;  //Put input value into property
              }

              function set_productType($inProductType) {
                  $this -> productType = $inProductType;
              }

      //Getters
      				function get_filename() {
      						return $this -> filename;
      				}

              function get_filesize() {
      						return $this -> filesize;
      				}

              function get_filetmp() {
      						return $this -> filetmp;
      				}

              function get_filetype() {
      						return $this -> filetype;
      				}

              function get_thumbname() {
      						return $this -> thumbname;
      				}

              function get_thumbsize() {
      						return $this -> thumbsize;
      				}

              function get_thumbtmp() {
      						return $this -> thumbtmp;
      				}

              function get_thumbtype() {
      						return $this -> thumbtype;
      				}

              function get_productType() {
                  return $this -> productType;
              }

              function get_fullPath() {
                  return $this -> fullImageFilePath;
              }

              function get_thumbPath() {
                  return $this -> thumbImageFilePath;
              }


          //  $imageFileName = $_POST["imageFileName"];
          function uploadFullImage() {

              $fullResultMsg = "";

              $trimProdType = strtolower(rtrim($this->productType, " Phone"));

              $sizeError = "";
              $typeError = "";

              $explode = explode('.',$this->filename);

              $file_ext=strtolower(end($explode));

              $extensions= array("jpeg","jpg","png");

              if(in_array($file_ext,$extensions)=== false){
                 $typeError="extension not allowed, please choose a JPEG or PNG file.";
              }

              if($this->filesize > 2097152){
                 $sizeError='File size must be less than 2 MB';
              }

              if(empty($errors)==true){
                 $this->fullImageFilePath = "img/products/".$trimProdType."/".$this->filename;
                 //move_uploaded_file($this->filetmp, "F:/xampp/htdocs/wdv341/projects/final/$this->fullImageFilePath");
		move_uploaded_file($this->filetmp, "$this->fullImageFilePath");
              }else{
                 $fullResultMsg = $sizeError." ".$typeError;
                 return $fullResultMsg;
              }

        }


        function uploadThumbImage() {

            $thumbResultMsg = "";

            $trimProdType = strtolower(rtrim($this->productType, " Phone"));

            $sizeError = "";
            $typeError = "";

            $explode = explode('.',$this->thumbname);

            $file_ext=strtolower(end($explode));

            $extensions= array("jpeg","jpg","png");

            if(in_array($file_ext,$extensions)=== false){
               $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }

            if($this->thumbsize > 2097152){
               $errors[]='File size must be less than 2 MB';
            }

            if(empty($errors)==true){
               $this->thumbImageFilePath = "img/products/".$trimProdType."/thumb/".$this->thumbname;
               move_uploaded_file($this->thumbtmp, "$this->thumbImageFilePath");

            }else{
               $thumbResultMsg = $sizeError." ".$typeError;
               return $thumbResultMsg;
            }

        }
    }
?>