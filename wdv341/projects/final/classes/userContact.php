<?php

		class User_Contact {

				//Properties

						public $name;

						public $email;				//email provided by customer

						public $subject;

						public $userMsg;

						public $contactDate;

						public $sentMsgText;

						public $responseBody;


				//Setters

						function set_name($inName) {
							//this -->  refers to the current object
							// -> go out to the current object and get the property.  No $ in front of property.
								$this -> name = $inName;  //Put input value into property
						}


						function set_email($inEmail) {
								$this -> email = $inEmail;  //Put input value into property
						}


						function set_subject($inSubject) {
								$this -> subject = $inSubject;
						}


						function set_userMsg($inUserMsg) {
								$this -> userMsg = $inUserMsg;
						}

						function set_responseMsg($inResponseBody) {
								$this -> responseBody = $inResponseBody;
						}


						function set_contactDate() {
								date_default_timezone_set('America/Chicago');

								$this -> contactDate = date('h:i A \o\n m/d/y');
						}


				//Getters

						function get_name() {
								return $this -> name;
						}


						function get_email() {
								return $this -> email;
						}


						function get_subject() {
								return $this -> subject;
						}


						function get_userMsg() {
								return $this -> userMsg;
						}

						function get_responseBody() {
								return $this -> responseBody;
						}

						function get_contactDate() {
								return $this -> contactDate;
						}



				//Processing Methods

						function formatHTMLMessage() {

								$msg = "<!DOCTYPE><html><head></head><body>";

								$msg .= "<h2>Your message was received at $this->contactDate.</h2>";

								$msg .= "<p>Thank you very much for your recent contact regarding $this->subject.  Your message:  '<em>$this->userMsg</em>' has been received and you should receive a prompt response at $this->email.</p>";

								$msg .= "<p>Sincerely,</p>";

								$msg .= "<p>The Outdated Phones Online Team</p></body></html>";



								return $msg;

						}


						function formatHTMLResponseMessage() {

								$msg = "<h4 style= 'text-align: center;'>$this->name received the response at $this->email!</h4>";

								return $msg;

						}


						function formatEmailMessage() {

							  $msgText = "Dear $this->name," . "\r\n";

								$msgText .= "\r\n";

								$msgText .= "Thank you very much for your recent contact regarding $this->subject.  ";

								$msgText .= "Your comment:  '$this->userMsg' has been received and you should receive a prompt response." . "\r\n";

								$msgText .= "\r\n";

								$msgText .= "Sincerely," . "\r\n";

								$msgText .= "\r\n";

								$msgText .= "The Outdated Phones Online Team" . "\r\n";

								return $msgText;

						}


						function formatResponseEmailMessage() {

							  $msgText = "Dear $this->name," . "\r\n";

								$msgText .= "\r\n";

								$msgText .= "$this->responseBody";

								$msgText .= "\r\n";

								$msgText .= "Sincerely," . "\r\n";

								$msgText .= "\r\n";

								$msgText .= "The Outdated Phones Online Team" . "\r\n";

								return $msgText;

						}


						function sendEmail() {

								$toEmail = $this->email;

								$subject = "Outdated Phones Support: $this->subject Ticket Created";

								$fromEmail = "web@kraigpopelka.info";

								$msg = $this->formatEmailMessage();

								$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";

								$headers .= "From: $fromEmail" . "\r\n";



								if (mail($toEmail,$subject,$msg,$headers)) 	//puts pieces together and sends the email to your hosting account's smtp (email) server
								{
									echo("<h2 style = 'color: #42f442; text-align: center;'>Confirmation: Your information was successfully sent!</h2>");
								}
								else
								{
									echo("<h2 style = 'color: red; text-align: center;'>ERROR: There was a problem sending your information.  Please try again.</p>");
								}

						}


						function sendResponseEmail() {

								$toEmail = $this->email;

								$subject = "Outdated Phones Support response for subject: $this->subject";

								$fromEmail = "web@kraigpopelka.info";

								$msg = $this->formatResponseEmailMessage();

								$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";

								$headers .= "From: $fromEmail" . "\r\n";



								if (mail($toEmail,$subject,$msg,$headers)) 	//puts pieces together and sends the email to your hosting account's smtp (email) server
								{
									echo("<h2 style = 'color: #42f442; text-align: center;'>Confirmation: Message successfully sent!</h2>");
								}
								else
								{
									echo("<h2 style = 'color: red; text-align: center;'>ERROR: There was a problem sending the message.  Please try again.</p>");
								}

						}


		} //End Class


?>
