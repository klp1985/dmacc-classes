<?php
		session_start();

		$validForm = false;

    $inFullName = "";
    $inUserEmail = "";
    $inUsername = "";
    $inPassword = "";
    $inUserRole = "";
    $inUserId = "";

		$updateHeading = "";
		$resultMsg = "";
		$userFullNameErrMsg = "";
		$userEmailErrMsg = "";
		$usernameErrMsg = "";
		$passwordErrMsg = "";
		$userRoleErrMsg = "";

		//Bring in Validation class
    include 'classes/validations.php';


	if ($_SESSION['validUser'] == "yes") {
    if ($_SESSION['userRole'] == 1 || $_SESSION['userRole'] == 2 || $_SESSION['userRole'] == 3) {

      include "connectPDO.php";  //Connects to the database and inserts the data

      if ( isset($_POST['submitBtn']) ) {	 /*If the form has been posted or submitted*/

          $validations = new Validations();

        /*Get the form data from the $_POST variable and place in PHP variables*/

          $inFullName = $_POST['user_full_name'];
          $inUserEmail = $_POST['ecomm_user_email'];
          $inUsername = $_POST['ecomm_username'];
          $inPassword = $_POST['ecomm_password'];
          $inUserRole = $_POST['user_role'];

          if (isset($_GET['user_id'])) {
        /*Get the event ID from the $_GET variable that was passed from the action attribute in the form*/
              $inUserId = $_GET['user_id'];
              $updateHeading = "<h1>Updating User:  $inUserId</h1>";
          } else {
              $updateHeading = "<h1>Add User:</h1>";
          }
        /*Validate the form data or at least sanitize the data*/
          $validForm = true;

          $validations->set_name($inFullName);
  				$validations->set_email($inUserEmail);
  				$validations->set_username($inUsername);
  				$validations->set_password($inPassword);
          $validations->set_userRole($inUserRole);
  				$validations->set_validForm($validForm);

  				$userFullNameErrMsg = $validations->validateName();
  				$userEmailErrMsg = $validations->validateEmail();
  				$usernameErrMsg = $validations->validateUsername();
  				$passwordErrMsg = $validations->validatePassword();
          $userRoleErrMsg = $validations->validateUserRole();

  				$validForm = $validations->get_validForm();
  				$inFullName = $validations->get_name();
  				$inEmail = $validations->get_email();
  				$inUsername = $validations->get_username();
  				$inPassword = $validations->get_password();
          $inUserRole = $validations->get_userRole();

      } //End if form submitted

      elseif (isset($_GET['user_id'])) {
          //If there is a product number value in the Get variable
          //Do update

          $inUserId = $_GET['user_id'];

          $updateHeading = "<h1>Updating User:  $inUserId</h1>";

          /*Create a SELECT query to retrieve the requested record from the database table*/

          $sqlSelect = "SELECT user_full_name, ecomm_user_email, ecomm_username, ecomm_password,user_role, ecomm_user_id FROM ecomm_user WHERE ecomm_user_id = $inUserId";

          //Run SELECT query

          try {
              $stmt = $conn->prepare($sqlSelect);

              $stmt->execute();
          } catch (PDOException $e) {

              $resultMsg = "<p>There was an error with your request: " . $e->getMessage() . "</p>";
          }

          if ($stmt->execute()){  /*If select query was successful and there is content*/
          //Place content from database into form fields
          //Display the form to the user with content in the fields
              while($row = $stmt->fetch()) {

                  $inFullName = $row['user_full_name'];
                  $inUserEmail = $row['ecomm_user_email'];
                  $inUsername = $row['ecomm_username'];
                  $inPassword = $row['ecomm_password'];
                  $inUserRole = $row['user_role'];
              }
          }
      } else {
          $updateHeading = "<h1>Add User:</h1>";
      }

    	if ($validForm) {  /*If data validation passes...*/
        if (isset($_GET['user_id'])) {  // if product number is present in Get variable then update the info in database with new values
    				$sqlUpdate = "UPDATE ecomm_user SET ";
    				$sqlUpdate .= "user_full_name = '$inFullName', ";
    				$sqlUpdate .= "ecomm_user_email = '$inUserEmail', ";
    				$sqlUpdate .= "ecomm_username = '$inUsername', ";
    				$sqlUpdate .= "ecomm_password = '$inPassword', ";
    				$sqlUpdate .= "user_role = '$inUserRole' ";
    				$sqlUpdate .= "WHERE (ecomm_user_id = '$inUserId')";

    				$updateStmt = $conn->prepare($sqlUpdate);

    				//Run the update query

    				$updateStmt->execute();

    				if ($updateStmt->execute()){  /*If update query was successful*/

    						//Confirm to the user that their data has been successfully processed
    						//Provide a link to the home page and the select page
    						$resultMsg =  "<h1>Success! The user has been updated.</h1>";
                if ($_SESSION['userRole'] == 1) {
                  $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateUser.php'>Add A User</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageUsers.php'>Back to Users List</a>";
                }
                $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
    				}

    				else { /*If there was an error with the query and did not execute*/
    						$resultMsg = "<h1>Update error: " . $e->getMessage() . "</h1>";
    						//Display message “Oops Please try again”
    						//Display a link to the home page and select page

    				}
          } elseif (!isset($_GET['user_id'])) { // add product to database
            try {

              $sqlAdd = "INSERT INTO ecomm_user (user_full_name, ecomm_user_email, ecomm_username, ecomm_password, user_role) VALUES (:user_full_name, :ecomm_user_email, :ecomm_username, :ecomm_password, :user_role)";

              $sqlPrepareAdd = $conn->prepare($sqlAdd);

              $sqlPrepareAdd->bindParam(':user_full_name', $inFullName);
              $sqlPrepareAdd->bindParam(':ecomm_user_email', $inUserEmail);
              $sqlPrepareAdd->bindParam(':ecomm_username', $inUsername);
              $sqlPrepareAdd->bindParam(':ecomm_password', $inPassword);
              $sqlPrepareAdd->bindParam(':user_role', $inUserRole);

            } catch (PDOException $e) {
                echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
            }

            if ($sqlPrepareAdd->execute()){
              if ($_SESSION['userRole'] == 1) {
                  $resultMsg = "<h4>The new user has been added.</h4>";
                  $resultMsg .= "<p><a class='btn btn-info btn-lg' href = 'add-updateUser.php'>Add Another User</a>";
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'manageUsers.php'>Back to Users List</a>";
              }
                  $resultMsg .= "<a class='btn btn-info btn-lg' style='margin-left:15px;' href = 'logout.php'>Logout</a></p>";
            }
        }
    	}

    } else {

  			header('Location: login.php');

  	}
	} else {

			header('Location: login.php');

	}


?>
<!DOCTYPE html>
	<html lang="en">
		<head>
		  <title>Products - Outdated Phones</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">

		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
			<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
		  <link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
			<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

		  <style>

		  </style>
		</head>


		<body>

				<div class="jumbotron">
				  <div class="container text-center">
				    <h1>Outdated Phones</h1>
				    <p>We sell everything but smartphones!</p>
				  </div>
				</div>

				<nav class="navbar navbar-inverse">
				  <div class="container-fluid">
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
				    </div>
				    <div class="collapse navbar-collapse" id="myNavbar">
				      <ul class="nav navbar-nav">
				        <li><a href="storeHome.php">Home</a></li>
				        <li><a href="storeProducts.php">Products</a></li>
				        <li><a href="storeContact.php">Contact</a></li>
				      </ul>
				      <ul class="nav navbar-nav navbar-right">

										<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
						<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<!-- Identify your business so that you can collect the payments. -->
								<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

								<!-- Specify a PayPal shopping cart View Cart button. -->
								<input type="hidden" name="cmd" value="_cart">
								<input type="hidden" name="display" value="1">

								<!-- Display the View Cart button. -->
								<input type="image" name="submit" 
									src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
								alt="Add to Cart" style="margin-top:10px;">
								<img alt="" width="1" height="1"
									src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
							</form>				
					</li>
						</ul>
					</div>
					</div>
				</nav>

        <div class = "container">
<?php
		//If the user submitted the form the changes have been made
		//if ( isset($_POST['submitBtn']) ) {
      if ($validForm) {
				echo $resultMsg;	//contains a Success or Failure output content
      }
		//}//end if submitted

		else
		{	//The page needs to display the form and associated data to the user for changes
				echo $updateHeading;
?>
								 <form id="form1" name="Add-Update_User" method="post" action="add-updateUser.php<?php if (isset($_GET['user_id'])){echo '?user_id='.$inUserId;}?>">

                   <div class = "row">
                     <div class = "col-sm-12">

										<p>Users Full Name: <span class = "error"><?php echo "$userFullNameErrMsg"; ?></span><br>
												<input type="text" name="user_full_name" id="userFullName" value="<?php echo $inFullName;?>"/>
										</p>

                    <p>User Email:  <span class = "error"><?php echo "$userEmailErrMsg"; ?></span><br>
												<input type="text" name="ecomm_user_email" id="userEmail" value="<?php echo $inUserEmail;?>"/>
										</p>

										<p>Username:  <span class = "error"><?php echo "$usernameErrMsg"; ?></span><br>
												<input type="text" name="ecomm_username" id="username" value="<?php echo $inUsername;?>"/>
										</p>

										<p>Password:  <span class = "error"><?php echo "$passwordErrMsg"; ?></span><br>
												<input type = "text" name = "ecomm_password" id = "password" value="<?php echo $inPassword;?>"/>
										</p>
                  <?php if ($_SESSION['userRole'] == 1) {?>

                    <p>User Role:    <span class = "error"><?php echo "$userRoleErrMsg"; ?></span><br>
												<label>
													  <select name="user_role" id="userRole">
    														<option value="1" <?php if($inUserRole == "1"){echo "selected = 'selected'";}?>>Administrator</option>
    														<option value="2" <?php if($inUserRole == "2"){echo "selected = 'selected'";}?>>Editor</option>
    														<option value="3" <?php if($inUserRole == "3"){echo "selected = 'selected'";}?>>Customer</option>
													  </select>
												</label>
											</p>

                  <?php } ?>
										<p>
												<input type="submit" class="btn btn-info btn-lg" name="submitBtn" id="submitBtn" value="Submit" />
												<input type="reset" class="btn btn-info btn-lg" name="resetBtn" id="resetBtn" value="Clear Form" />
                      <?php if ($_SESSION['userRole'] == 3) {?>
                        <a style = "background-color: red; margin-left:50px" class="btn btn-info btn-lg" href = "deletePage.php?keyid=<?php echo $_SESSION['userIdLogin'];?>&tname=ecomm_user">Delete Account</a>This can not be undone!
                      <?php } ?>
										</p>
                  </div>


                  </div>
								</form>
                <hr />
								<p>
                  <?php if ($_SESSION['userRole'] == 1) {?>
										<a class="btn btn-info btn-lg" href = "manageUsers.php">Go Back to Table</a>
                  <?php } ?>
										<a class="btn btn-info btn-lg" style = "margin-left: 15px;" href='logout.php'>Logout</a>
								</p>

						</div>

<?php
		}//end else submitted

		$conn = null;

?>
				</body>

		</html>
