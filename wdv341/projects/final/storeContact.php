<?php
		session_start();

		//Bring in Email and Validation classes
		include 'classes/userContact.php';
		include 'classes/validations.php';

		$inName = "";
		$inEmail = "";
		$inSubject = "";
		$inUserMsg = "";
		$inPhone = "";

		$validForm = false;

		$resultName = "";
		$resultEmail = "";
		$resultSubject = "";
		$resultUserMsg = "";
		$resultPhone = "";

		$nameErrMsg = "";
		$emailErrMsg = "";
		$subjectErrMsg = "";
		$userMsgErrMsg = "";

		$dbResult = "";

		if ( isset($_POST['submitBtn']) ) { //If submit button was pressed

				//Create email and validatrion objects
				$emailUser = new User_Contact();
				$validations = new Validations();

				$inName = $_POST['name'];
				$inEmail = $_POST['email'];
				$inSubject = $_POST['subject'];
				$inUserMsg = $_POST['message'];

				$validForm = true;

				$validations->set_name($inName);
				$validations->set_email($inEmail);
				$validations->set_subject($inSubject);
				$validations->set_userMsg($inUserMsg);
				$validations->set_phone($inPhone);
				$validations->set_validForm($validForm);

				$resultName = $validations->validateName();
				$resultEmail = $validations->validateEmail();
				$resultSubject =$validations->validateSubject();
				$resultUserMsg = $validations->validateUserMsg();
				$resultPhone = $validations->validatePhauxn();

				$validForm = $validations->get_validForm();
				$inName = $validations->get_name();
		}


		if ($validForm) { //If form passes validation then place in messages table
			include "connectPDO.php";
			try {

				$sql = "INSERT INTO ecomm_contact_messages (message_sender_name, message_sender_email, message_subject, message_body) VALUES (:message_sender_name, :message_sender_email, :message_subject, :message_body)";

				$sqlPrepare = $conn->prepare($sql);

				$sqlPrepare->bindParam(':message_sender_name', $inName);
				$sqlPrepare->bindParam(':message_sender_email', $inEmail);
				$sqlPrepare->bindParam(':message_subject', $inSubject);
				$sqlPrepare->bindParam(':message_body', $inUserMsg);

			} catch (PDOException $e) {
					echo "There was a problem entering the information.  Please try again: " . $e->getMessage();
			}

				$conn = null;

				if (!$sqlPrepare->execute()){

						$dbResult = "<p>An error occurred recording your message.</p>";

				}

		}
?>

<!DOCTYPE html>

		<html lang="en">

				<head>
						<title>Contact - Outdated Phones</title>

						<meta charset="utf-8">
						<meta name="viewport" content="width=device-width, initial-scale=1">

						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
						<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
						<link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
						<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

						<style>


								.h1 small {
									font-size: 24px;
								}

								/* Remove the navbar's default margin-bottom and rounded borders */
								.navbar {
								  margin-bottom: 0;
								  border-radius: 0;
								}

								/* Set height of the grid so .sidenav can be 100% (adjust as needed) */
								.row.content {height: 450px}

								/* Set gray background color and 100% height */
								.sidenav {
								  padding-top: 20px;
								  background-color: #f1f1f1;
								  height: 100%;
								}

								/* Remove the jumbotron's default bottom margin */
								 .jumbotron {
								  margin-bottom: 0;
								}

								/* Set black background color, white text and some padding */
								footer {

								}

								/* On small screens, set height to 'auto' for sidenav and grid */
								@media screen and (max-width: 767px) {
								  .sidenav {
									height: auto;
									padding: 15px;
								  }
								  .row.content {height:auto;}
								}

						</style>
				</head>


				<body>

						<div class="jumbotron">
						  <div class="container text-center">
							<h1>Outdated Phones</h1>
							<p>We sell everything but smartphones!</p>
						  </div>
						</div>

						<nav class="navbar navbar-inverse">
							<div class="container-fluid">
								<div class="navbar-header">

									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>

									<a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
								</div>

								<div class="collapse navbar-collapse" id="myNavbar">

									<ul class="nav navbar-nav">
										<li><a href="storeHome.php">Home</a></li>
										<li><a href="storeProducts.php">Products</a></li>
										<li class="active"><a href="storeContact.php">Contact</a></li>
									</ul>

									<ul class="nav navbar-nav navbar-right">
										<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
												if ($_SESSION['validUser'] == "yes") {
													echo $_SESSION['fullname'];
												} else{
													echo "Your Account";
												}

												if ($_SESSION['validUser'] == "yes") {
?>
														<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
															<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
															<li><a class="dropdown-item" href="logout.php">Logout</a></li>
														</ul>
<?php
												} else {
?>
													<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
														<li><a class="dropdown-item"  href="login.php">Login</a></li>
													</ul>
<?php
												}
?>
										</a></li>

										<li>
											<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
												<!-- Identify your business so that you can collect the payments. -->
												<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

												<!-- Specify a PayPal shopping cart View Cart button. -->
												<input type="hidden" name="cmd" value="_cart">
												<input type="hidden" name="display" value="1">

												<!-- Display the View Cart button. -->
												<input type="image" name="submit" 
													src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
												alt="Add to Cart" style="margin-top:10px;">
												<img alt="" width="1" height="1"
													src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
											</form>									
										</li>
									  </ul>
									</div>
								  </div>
								</nav>

<?php

			if ($validForm) { // If form passes validation then send message information to email object

				$emailUser->set_name($inName);
				$emailUser->set_email($inEmail);
				$emailUser->set_subject($inSubject);
				$emailUser->set_userMsg($inUserMsg);
				$emailUser->set_contactDate();
?>

						<div class="jumbotron jumbotron-sm">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 col-lg-12">
										<h1 class="h1">
											Thank You, <?php echo $inName;?>!</h1>
									</div>
								</div>
							</div>
						</div>

						<div class="container">
							<div class="row">
								<div class="col-md-8">
									<div class="well well-sm">
<?php
										$emailUser->sendEmail();  // Send email

										echo $emailUser->formatHTMLMessage();  //Get result message from email object and display to user.
?>
									</div>
								</div>

								<div class="col-md-4">
									<form>
										<legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>

										<address>
											<strong>Outdated Phones, Inc.</strong><br>
											1234 Street Ave<br>
											Ames, Iowa 50010<br>
											<abbr title="Phone">
												P:</abbr>
											(123) 456-7890
										</address>

									</form>
								</div>
							</div>
						</div>

<?php

			} else {  // If forma validation fails

?>
						<div class="jumbotron jumbotron-sm">
							<div class="container">
								<div class="row">
									<div class="col-sm-12 col-lg-12">
										<h1 class="h1">Contact us</h1>
									</div>
								</div>
							</div>
						</div>


						<div class="container">
							<div class="row">
								<div class="col-md-8">
									<div class="well well-sm">

						<!-- - - - - - - - - - Form - - - - - - - - - - - - - - - - - - - - - - - - - - -->
										<form name="contactForm" method="post" action="storeContact.php">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group">
														<label for="name">
															Name</label>
															<span class = "error"><?php echo $resultName; ?></span><br>
														<input type="text" class="form-control" id="name" name = "name" placeholder="Enter name" value = "<?php echo $inName;?>" />
													</div>

													<div class="form-group">
														<label for="email">
															Email Address</label>
															<span class = "error"><?php echo $resultEmail; ?></span><br>
														<div class="input-group">
															<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
															</span>
															<input type="text" class="form-control" id="email" name = "email" placeholder="Enter email" value = "<?php echo $inEmail;?>"/>
														</div>
													</div>

													<div class="form-group">
														<label for="subject">
															Subject</label>
															<span class = "error"><?php echo $resultSubject; ?></span><br>
														<select id="subject" name="subject" class="form-control">
															<option value="default" <?php if($inSubject == "default"){echo "selected = 'selected'";}?>>Choose One:</option>
															<option value="service" <?php if($inSubject == "service"){echo "selected = 'selected'";}?>>General Customer Service</option>
															<option value="account" <?php if($inSubject == "product"){echo "selected = 'selected'";}?>>Account Support</option>
															<option value="suggestions" <?php if($inSubject == "suggestions"){echo "selected = 'selected'";}?>>Suggestions</option>
															<option value="product" <?php if($inSubject == "product"){echo "selected = 'selected'";}?>>Product Support</option>
															<option value="other" <?php if($inSubject == "other"){echo "selected = 'selected'";}?>>Other</option>
														</select>
													</div>

												</div>

												<div class="col-md-6">
													<div class="form-group">
														<label for="name">
															Message</label>
															<span class = "error"><?php echo $resultUserMsg; ?></span><br>
														<textarea name="message" id="message" class="form-control" rows="9" cols="25"
															placeholder="Message"><?php echo $inUserMsg;?></textarea>
													</div>
												</div>

												<div class="col-md-12">
													<button type="submit" class="btn btn-primary pull-right" id="btnContactUs" name = "submitBtn">
														Send Message</button>
												</div>

											</div>
										</form>

									</div>
								</div>

								<div class="col-md-4">
									<form>
										<legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>

										<address>
											<strong>Outdated Phones, Inc.</strong><br>
											1234 Street Ave<br>
											Ames, Iowa 50010<br>
											<abbr title="Phone">
												P:</abbr>
											(123) 456-7890
										</address>

									</form>
								</div>
							</div>
						</div>

<?php
			}
?>

						<footer class="container-fluid text-center">
							<p>WDV 341: Intro to PHP Final Project</p>
						</footer>

				</body>
		</html>
