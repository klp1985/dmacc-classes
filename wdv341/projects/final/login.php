<?php
	session_start();

	$_SESSION['message_id'] = "";
	$displayMessages = "";
	$loginErrMsg = "";

	if (isset($_SESSION['validUser'])) {
		if ($_SESSION['validUser'] == "yes") {	//If user is logged in already

			include 'connectPDO.php';

			//Get message information from messages table for a user that is already logged in
			$sqlMessages = "SELECT message_sender_name, message_sender_email, message_subject, message_body, message_id FROM ecomm_contact_messages";

			try {
						$stmtMsg = $conn->prepare($sqlMessages);
						$stmtMsg->execute();

						if ($stmtMsg->execute()) {
							//Format messages table
								$displayMessages .= "<div class = 'container'>";
								$displayMessages .= "<h3>Messages Received</h3>";
								$displayMessages .= "<div class = 'row'>";
								$displayMessages .= "<div class = 'col-sm-12'>";
								$displayMessages .= "<table class='tg'>";
								$displayMessages .= "<tr>";
								$displayMessages .= "<th class='tg-aq88'>Sender Name</th>";
								$displayMessages .= "<th class='tg-aq88'>Sender Email</th>";
								$displayMessages .= "<th class='tg-aq88'>Subject</th>";
								$displayMessages .= "<th class='tg-aq88'></th>";
								if ($_SESSION['userRole'] == 1) {
									$displayMessages .= "<th class='tg-wr1b'></th>";
								}
								$displayMessages .= "</tr>";

								while($row = $stmtMsg->fetch()) {
									// Place each message into table
										$displayMessages .= "<tr>";
										$displayMessages .= "<td class='tg-yzt1'>" . $row['message_sender_name'] . "</td>";
										$displayMessages .= "<td class='tg-yzt1'>" . $row['message_sender_email'] . "</td>";
										$displayMessages .= "<td class='tg-yzt1'>" . $row['message_subject'] . "</td>";
										$displayMessages .= "<td class='tg-yzt1'><a href='messageView.php?message_id=" . $row['message_id'] . "'>View</a></td>";
										if ($_SESSION['userRole'] == 1) {
											$displayMessages .= "<td class='tg-yzt1'><a href='deletePage.php?keyid=" . $row['message_id'] . "&tname=ecomm_contact_messages'>Delete</a></td>";
										}
										$displayMessages .= "</tr>\n";

								}

								$displayMessages .= "</table>";
								$displayMessages .= "</div>";
								$displayMessages .= "</div>";
								$displayMessages .= "</div>";

								$_SESSION['messages'] = $displayMessages;

								$conn = null;
						} else {
								$displayMessages = "There was an error collecting the messages.";
						}
				}
				catch (PDOException $e) {
						echo "There was an error collecting the messages." . $e->getMessage();
				}

			if ($_SESSION['userRole'] == 1) {	//This is an admin.  Show the admin options.
				$msg = "<div class = 'container'><h1 style = 'text-align: center;'>Administrator Panel</h1><h3>Hello, " . $_SESSION['fullname'] . "</h3><h4>You are making changes as: " . $_SESSION['username'] . "</h4>$displayMessages</div>";


			}

			elseif ($_SESSION['userRole'] == 2) {
				$msg = "<div class = 'container'><h1>Editor Panel</h1><h3>Hello, " . $_SESSION['fullname'] . "</h3><h4>You are making changes as: " . $_SESSION['username'] . "</h4>$displayMessages</div>";
			}

			elseif ($_SESSION['userRole'] == 3){
				$msg = "<div class = 'container'><h1>Hello, " . $_SESSION['fullname'] . "!</h3></div>";
			}

		}	//End validUser is true

		else {
				if (isset($_POST['submitLogin'])) {	//If submit button has been pushed

						$inUsername = $_POST['username'];
						$inPassword = $_POST['password'];

						include 'connectPDO.php';

						//Get user information from users table based on username and password
						$sql = "SELECT user_full_name, ecomm_username, ecomm_password, user_role, ecomm_user_id FROM ecomm_user WHERE ecomm_username = :username AND ecomm_password = :password;";



							$stmt = $conn->prepare($sql);

							$stmt->bindParam(':username', $inUsername);
							$stmt->bindParam(':password', $inPassword);

							$stmt->execute();



						if ($stmt->execute()){  /*If select query was successful and there is content*/
					//Place content from database into form fields
					//Display the form to the user with content in the fields

							while($row = $stmt->fetch()) {

									$fullName = $row['user_full_name'];
									$username = $row['ecomm_username'];
									$password = $row['ecomm_password'];
									$userRole = $row['user_role'];
									$userId = $row['ecomm_user_id'];

							}
						}

						if ($stmt->rowCount() == 1) {  // If exactly one row is returned then the user was found in the users table and is a valid user.

								$_SESSION['validUser'] = "yes";								//this is a valid user so set SESSION variable
								$_SESSION['username'] = $username;
								$_SESSION['fullname'] = $fullName;
								$_SESSION['userRole'] = $userRole;
								$_SESSION['userIdLogin'] = $userId;

								include 'connectPDO.php';

								//Get message information from messages table for a user that has just logged in.
								$sqlMessages = "SELECT message_sender_name, message_sender_email, message_subject, message_body, message_id FROM ecomm_contact_messages";

								try {
											$stmtMsg = $conn->prepare($sqlMessages);
											$stmtMsg->execute();

											if ($stmtMsg->execute()) {
												//Format message
													$displayMessages .= "<div class = 'container'>";
													$displayMessages .= "<h3>Messages Received</h3>";
													$displayMessages .= "<div class = 'row'>";
													$displayMessages .= "<div class = 'col-sm-12'>";
													$displayMessages .= "<table class='tg'>";
													$displayMessages .= "<tr>";
													$displayMessages .= "<th class='tg-aq88'>Sender Name</th>";
													$displayMessages .= "<th class='tg-aq88'>Sender Email</th>";
													$displayMessages .= "<th class='tg-aq88'>Subject</th>";
													$displayMessages .= "<th class='tg-aq88'></th>";
													if ($_SESSION['userRole'] == 1) {
														$displayMessages .= "<th class='tg-wr1b'></th>";
													}
													$displayMessages .= "</tr>";

													while($row = $stmtMsg->fetch()) {
														//Place each message into table
															$displayMessages .= "<tr>";
															$displayMessages .= "<td class='tg-yzt1'>" . $row['message_sender_name'] . "</td>";
															$displayMessages .= "<td class='tg-yzt1'>" . $row['message_sender_email'] . "</td>";
															$displayMessages .= "<td class='tg-yzt1'>" . $row['message_subject'] . "</td>";
															$displayMessages .= "<td class='tg-yzt1'><a href='messageView.php?message_id=" . $row['message_id'] . "'>View</a></td>";
															if ($_SESSION['userRole'] == 1) {
																$displayMessages .= "<td class='tg-yzt1'><a href='deletePage.php?keyid=" . $row['message_id'] . "&tname=ecomm_contact_messages'>Delete</a></td>";
															}
															$displayMessages .= "</tr>\n";

													}

													$displayMessages .= "</table>";
													$displayMessages .= "</div>";
													$displayMessages .= "</div>";
													$displayMessages .= "</div>";

													$_SESSION['messages'] = $displayMessages;

													$conn = null;
											} else {
													$displayMessages = "There was an error collecting the messages.";
											}
									}
									catch (PDOException $e) {
											echo "There was an error collecting the messages." . $e->getMessage();
									}

							//User roles -  there is a user role column in the users table that determines what access is allowed to each user.  Administrator = 1, Editor = 2, Customer = 3.
								if ($_SESSION['userRole'] == 1) {	//This is an admin.  Show the admin options.
									$msg = "<div class = 'container'><h1 style = 'text-align: center;'>Administrator Panel</h1><h3>Hello, " . $_SESSION['fullname'] . "</h3><h4>You are making changes as: " . $_SESSION['username'] . "</h4>$displayMessages</div>";
									}

								elseif ($_SESSION['userRole'] == 2) { //This is an editor.  Show the editor options.
									$msg = "<div class = 'container'><h1>Editor Panel</h1><h3>Hello, " . $_SESSION['fullname'] . "</h3><h4>You are making changes as: " . $_SESSION['username'] . "</h4>$displayMessages</div>";
								}

								elseif ($_SESSION['userRole'] == 3){ //This is a customer.  Show the customer options.
									$msg = "<h2>Hello, " . $_SESSION['fullname'] . "!</h2>";
								}

						}

						else {

								$_SESSION['validUser'] = "no";
								$loginErrMsg = "Sorry, there was a problem with your username or password. Please try again.";
						}
						$conn = null;
				}  //End if submitted
		}
	}

	else{ //Set variables for user first seeing page.
		$_SESSION['validUser'] = "no";
		$_SESSION['userRole'] = 0;
		$msg = "";
	}
?>

<!DOCTYPE html>
		<html>
				<head>

						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>Login - Outdated Phones</title>

						<link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
						<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
						<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
						<link href = "style/ecommStyles.css" rel = "stylesheet" type = "text/css" />
						<link rel = "icon" type = "image/png" href = "img/site/icon.png"/>
						<link rel = "shortcut icon" type = "image/png" href = "img/site/icon.png"/>

						<style>

								.equal , .equal > div[class*='col-'] {
								    display: -webkit-box;
								    display: -moz-box;
								    display: -ms-flexbox;
								    display: -webkit-flex;
								    display: flex;
								    flex:1 1 auto;
								}

						</style>

				</head>

				<body>

						<div class="jumbotron">
						  <div class="container text-center">
							<h1>Outdated Phones</h1>
							<p>We sell everything but smartphones!</p>
						  </div>
						</div>

						<nav class="navbar navbar-inverse">
						  <div class="container-fluid">
							<div class="navbar-header">
							  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							  </button>
							  <a class="navbar-brand" href="storeHome.php"><img src = "img/site/icon.png" height = '100%'/></a>
							</div>
							<div class="collapse navbar-collapse" id="myNavbar">
							  <ul class="nav navbar-nav">
								<li class="active"><a href="storeHome.php">Home</a></li>
								<li><a href="storeProducts.php">Products</a></li>
								<li><a href="storeContact.php">Contact</a></li>
							  </ul>
							  <ul class="nav navbar-nav navbar-right">

									<li class="dropdown"><a class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href = "#"><span class="glyphicon glyphicon-user"></span>
<?php
								if ($_SESSION['validUser'] == "yes") {
									echo $_SESSION['fullname'];
								} else{
									echo "Your Account";
								}

								if ($_SESSION['validUser'] == "yes") {
?>
										<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
											<li><a class="dropdown-item"  href="login.php">Account Panel</a></li>
											<li><a class="dropdown-item" href="logout.php">Logout</a></li>
										</ul>
<?php
								} else {
?>
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
										<li><a class="dropdown-item"  href="login.php">Login</a></li>
									</ul>
<?php
								}
?>
						</a></li>

						<li>
							<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
								<!-- Identify your business so that you can collect the payments. -->
								<input type="hidden" name="business" value="kpopelka-seller@gmail.com">

								<!-- Specify a PayPal shopping cart View Cart button. -->
								<input type="hidden" name="cmd" value="_cart">
								<input type="hidden" name="display" value="1">

								<!-- Display the View Cart button. -->
								<input type="image" name="submit" 
									src="https://www.paypalobjects.com/webstatic/en_US/i/btn/png/btn_viewcart_113x26.png"
								alt="Add to Cart" style="margin-top:10px;">
								<img alt="" width="1" height="1"
									src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif">
							</form>							
						</li>
						</ul>
					</div>
					</div>
				</nav>



						<?php //echo $msg?>

<?php
	if ($_SESSION['validUser'] == "yes")	{	//This is a valid user.

					echo $msg;

			if ($_SESSION['userRole'] == 1) {	//This is an admin.  Show the admin options.


	//turn off PHP and turn on HTML
?>
				<p>&nbsp;</p>
				<hr />
				<p>&nbsp;</p>
				<div class = "container">
					<h3>Administrator Options</h3>
					<div class = "row equal">
						<div class="col-sm-4">
							<div class="panel panel-primary">
								<div class="panel-heading"></div>
								<div class="panel-body"><h4 style = "text-align: center;">Products</h4><p>View a list of all products with options to add, edit or delete a product.</p></div>
								<div class="panel-footer" style = "text-align: center;">
									<a href="manageProducts.php" class="btn btn-info btn-lg">View Products</a>
								</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="panel panel-primary">
								<div class="panel-heading"></div>
								<div class="panel-body"><h4 style = "text-align: center;">Users</h4><p>View a list of all users with options to add, edit or delete a user.</p></div>
								<div class="panel-footer"  style = "text-align: center;">
									<a href="manageUsers.php" class="btn btn-info btn-lg">View Users</a>
								</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="panel panel-primary">
								<div class="panel-heading"></div>
								<div class="panel-body"><h4 style = "text-align: center;">Newsletter Email List</h4><p>View a list of all email addresses with options to add, edit or delete an email address.</p></div>
								<div class="panel-footer" style = "text-align: center;">
									<a href="manageNewsletter.php" class="btn btn-info btn-lg">View Email List</a>
								</div>
							</div>
						</div>

					</div>
					<hr />
					<a href="logout.php" class="btn btn-info btn-lg">Logout</a>
				</div>

<?php
			} //End user_role: admin

			elseif ($_SESSION['userRole'] == 2) {  //This is a user with limited admin options.  They can add/update/delete products.


?>
						<p>&nbsp;</p>
						<hr />
						<p>&nbsp;</p>
						<div class = "container">
							<h3>Editor Options</h3>
							<div class = "row  equal">
								<div class="col-sm-4">
									<div class="panel panel-primary">
										<div class="panel-heading"></div>
										<div class="panel-body"><h4 style = "text-align: center;">Products</h4><p>View a list of all products with options to add or edit a product.</p></div>
										<div class="panel-footer" style = "text-align: center;">
											<a href="manageProducts.php" class="btn btn-info btn-lg">View Products</a>
										</div>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="panel panel-primary">
										<div class="panel-heading"></div>
										<div class="panel-body"><h4 style = "text-align: center;">Newsletter Email List</h4><p>View a list of all email addresses that are signed up to receive the newsletter.</p></div>
										<div class="panel-footer" style = "text-align: center;">
											<a href="manageNewsletter.php" class="btn btn-info btn-lg">View Email List</a>
										</div>
									</div>
								</div>

								<div class="col-sm-4">
									<div class="panel panel-primary">
										<div class="panel-heading"></div>
										<div class="panel-body"><h4 style = "text-align: center;">Update Account</h4><p>Edit your account information.</p></div>
										<div class="panel-footer" style = "text-align: center;">
											<a href="add-updateUser.php?user_id=<?php echo $_SESSION['userIdLogin'];?>" class="btn btn-info btn-lg">Update</a>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<a href="logout.php" class="btn btn-info btn-lg">Logout</a>
						</div>
<?php
			}
			elseif ($_SESSION['userRole'] == 3) {	//This is a registered user to the site.  They can view their account and orders.
?>

						<p>&nbsp;</p>
						<hr />
						<p>&nbsp;</p>
						<div class = "container">
							<div class = "row  equal">

								<div class="col-sm-6">
									<div class="panel panel-primary">
										<div class="panel-heading"></div>
										<div class="panel-body"><h4 style = "text-align: center;">Update Account</h4><p>Edit your account information or delete your account.</p></div>
										<div class="panel-footer" style = "text-align: center;">
											<a href="add-updateUser.php?user_id=<?php echo $_SESSION['userIdLogin'];?>" class="btn btn-info btn-lg">Update</a>
										</div>
									</div>
								</div>
							</div>
							<hr />
							<a href="logout.php" class="btn btn-info btn-lg">Logout</a>
						</div>


<?php
			}
	} //End validUser
	else {								//The user needs to log in.  Display the Login Form
?>
						<div class="container">
								<h3>Login or <a href="storeRegistration.php">Sign up</a></h3>
								<h5 class = "error"><?php echo $loginErrMsg;?></h5>

									<div class="col-xs-12 col-sm-6">
										<form class="omb_loginForm" method="post" name="loginForm" action="login.php" autocomplete="off" >
											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-user"></i></span>
												<input type="text" class="form-control" name="username" placeholder="Username or Email Address">
											</div>
											<span class="help-block"></span>

											<div class="input-group">
												<span class="input-group-addon"><i class="fa fa-lock"></i></span>
												<input  type="password" class="form-control" name="password" placeholder="Password">
											</div>
											<span class="help-block"></span>

											<button class="btn btn-lg btn-primary btn-block" name = "submitLogin" type="submit">Login</button>
										</form>
									</div>
						</div>

<?php

	}//end of checking for a valid user

?>
						<footer style = "margin-top: 75px;" class="container-fluid text-center">
							<p>WDV 341: Intro to PHP Final Project</p>
						</footer>
				</body>
		</html>
