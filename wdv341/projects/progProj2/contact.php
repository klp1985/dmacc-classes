<?php

				$validForm = false;
				
				$inName = "";
				$inEmail = "";
				$inReason = "";
				$inComments = "";
				
				$contactDate = "";
				
				
				$nameErrMsg = "";
				$emailErrMsg = "";
				$reasonErrMsg = "";
				$commentsErrMsg = "";
				
				
				function sendEmail() {
				
						global $inName, $inEmail, $inReason, $inComments, $contactDate;
						
						getDateTime();
		
						$sendInfo = "kpopelka@gmail.com";
					
						$subject = "Your recent contact to Kraig Popelka - Web Developer";	

						$fromEmail = "web@kraigpopelka.info";		
						
						$infoText = "Form Data\n\n ";			
								foreach($_POST as $key => $value)											
								{
									$infoText.= $key."=".$value."\n";	
								} 

						$msgText = "Dear " . $inName . "," . "\r\n"; 
												
						$msgText .= "\r\n";
												
						$msgText .= "Thank you very much for your recent contact regarding " . $inReason . ".  ";  
												
						$msgText .= "  Your comment:  '" . $inComments . "' was received at " . $contactDate . ". ";
						
						$msgText .= "  Expect a prompt response at your email address: " . $inEmail . "." . "\r\n";

						$msgText .= "\r\n";
												
						$msgText .= "Sincerely," . "\r\n";
												
						$msgText .= "\r\n";
												
						$msgText .= "Kraig Popelka" . "\r\n";
						
						$headers = "Content-Type: text/plain; charset=\"utf-8\"\r\n";  //Allows the email to be created with HTML
						
						$headers .= "From: $fromEmail" . "\r\n";				//Creates the From header with the appropriate address
						
						$headers .= 'Bcc: kpopelka@gmail.com' . "\r\n";
						
						mail ($sendInfo, $subject, $infoText, $headers);  //Sends just the inputed data to email address that will respond to the contact message.
						
						

						if (mail($inEmail,$subject,$msgText,$headers)) {	//Sends a confirmation email to the person that is initiating the contact.  A blind copy is sent to the address that will respond to the email.
					
								echo("<h2 style = 'color: #42f442; text-align: center; margin: 15px;'>Confirmation: Your message was successfully sent!</h2>");
						} 
						
						else {
								echo("<h2 style = 'color: red; text-align: center; margin 15px;'>ERROR: There was a problem sending your message.  Please try again.</p>");
						}
				
				}
				
				
				function getDateTime() {
						
						global $contactDate;
				
						date_default_timezone_set('America/Chicago');
							
						$contactDate = date('h:i A \o\n m/d/y');
				
				}
				
				
				function validateName() {
					
						global $inName, $validForm, $nameErrMsg;
						
						$nameErrMsg = "";
						
						if ( !$inName == "") {
							
								$inName = ltrim($inName);
								
								$inName = filter_var($inName, FILTER_SANITIZE_STRING);
								
						
						}
						
						else {
								
								$validForm = false;
								
								$nameErrMsg = "Your name is required.";
								
						}
				
				}
				
			
				
				function validateEmail() {
				
						global $inEmail, $validForm, $emailErrMsg;
						
						$emailErrMsg = "";
						
						if ( !$inEmail == "") {
								
								$inEmail = filter_var($inEmail, FILTER_SANITIZE_EMAIL);
								
								
								if (filter_var($inEmail, FILTER_VALIDATE_EMAIL) === false) {
										
										$validForm = false;
								
										$emailErrMsg = "Email address is not formatted correctly.  Use the format jdoe@example.com";
										
								}
						
						}
						
						else {
								
								$validForm = false;
								
								$emailErrMsg = "Email address is required.";
								
						}
				
				}
				
				
				
				function validateReason() {
					
						global $inReason, $inComments, $validForm, $reasonErrMsg, $commentsErrMsg;
						
						$reasonErrMsg = "";
						
						$commentsErrMsg = "";
						
						if ($inReason == "default") {
						
								$validForm = false;
								
								$reasonErrMsg = "Please select a reason for contact.";
						
						}

				}
				
				
				
				function validateComments() {
					
						global $inComments, $inReason, $validForm, $commentsErrMsg;
						
						$commentsErrMsg = "";
						
						
						
						if ($inComments == "") {
							
								if ($inReason == "other") {
								
										$validForm = false;
										
										$commentsErrMsg = "A comment is required for the reason selected.";
								
								}
							
						}
					
						
					
				}
				
				
				function validatePhauxn() {
				
						global $inPhone, $validForm;
						
						if (!$inPhone == "") {
							
								$validForm = false;
						
						}
				
				}
			

		if ( isset($_POST['submitBtn']) ) {				//Checks if the form has been submitted.  If it has it will validate the form.
				

				$inName = $_POST['fullName']; 
				
				$inEmail = $_POST['email'];		
						
				$inReason = $_POST['reason'];
						
				$inComments = $_POST['comments'];
				
				$inPhone = $_POST['phone'];
				
				$validForm = true;
				
				
				
				
				validateName();
				validateEmail();
				validateReason();
				validateComments();
				validatePhauxn();
				
		}

?>




<!DOCTYPE html>

		<html >

				<head>

				
						<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

						<title>Contact</title>
				
						<link href = "../files/port_style_all.css" rel = "stylesheet" type = "text/css" />

						
						<style>

								#submitBtn, #resetBtn {
									margin-right: 5px;
									background-color: #4d4d4d;
									color: white;
									padding: .4em;
									border: 1px solid #e1ddb7;
								}
								
								#submitBtn:hover, #resetBtn:hover {
										background-color: #3d3d5c;
										color: white;
										cursor: pointer;
										padding: .4em;
										border: 1px solid white;
								}
								
								form {
										border: 1px solid #e1ddb7;
								}

								.error	{
									color:red;
									font-style:italic;	
								}
								
								h1 {
								margin: 20px 25px;
								}
						
								#container {
										border-top: 2px solid #e1ddb7;
								}
								
								.containField {
										display: none;
								}

						</style>

						
				</head>



				<body>


<?php

					if ($validForm) {			//If the form has been entered and validated a confirmation page is displayed in the VIEW area.
					
						sendEmail();
					
?>

						<section id = "container">
						
								<nav>
									<ul class = "menu">
											<li><a href = "../index.html">Home</a></li>
											<li><a href = "../about/about.html">About</a></li>
											<li><a href = "../work/work.html">Work</a></li>
											<li class = "dropdown">
												<a href = "#" class = "dropmenu">DMACC Coursework</a>
													<div class = "courses">
														<a href="../courses/art186/art186.html">Art 186: Principles of Digital Photography</a>
														<a href="http://kraigpopelka.info/mkt121">Mkt 121: Digital Marketing</a>
														<a href="../courses/wdv101/webdev101.html">Web Dev 101: Intro to HTML & CSS</a>
														<a href="../courses/wdv131/wdv131.html">Web Dev 131: Intro to Photoshop & Fireworks</a>
														<a href="../courses/wdv205/wdv205.html">Web Dev 205: Advanced CSS</a>
														<a href="../courses/wdv221/wdv221homework.html">Web Dev 221: Intro to Javascript</a>
														<a href="http://kraigpopelka.info/courses/wdv240/final">Web Dev 240: Intro to WordPress</a>
													</div> <!-- End div courses -->
											</li>
											<li><a class = "current" href = "contact.html">Contact</a></li>
									</ul>
								</nav> <!--End nav-->
								
								

								<?php echo "<h1>Thank You, " . $inName . "!</h1>" ?>
						
								<p>Your message of "<?php echo $inComments;?>" regarding <?php echo $inReason;?> was received at <?php echo $contactDate; ?>.
						
								<p>You should receive an email confirmation shortly at <?php echo $inEmail;?> and you can expect a prompt response.</p>
								
								<hr />
									
								<p><a href = "../index.html">Back to home page</a></p>
						
						</section>

<?php

					}
						
					else {

?>						

						<header>
	
								<h1 class = "header-content">Kraig Popelka</h1>
			
						</header>
		
						<section id = "container">
						
								<nav>
									<ul class = "menu">
											<li><a href = "../index.html">Home</a></li>
											<li><a href = "../about/about.html">About</a></li>
											<li><a href = "../work/work.html">Work</a></li>
											<li class = "dropdown">
												<a href = "#" class = "dropmenu">DMACC Coursework</a>
													<div class = "courses">
														<a href="../courses/art186/art186.html">Art 186: Principles of Digital Photography</a>
														<a href="http://kraigpopelka.info/mkt121">Mkt 121: Digital Marketing</a>
														<a href="../courses/wdv101/webdev101.html">Web Dev 101: Intro to HTML & CSS</a>
														<a href="../courses/wdv131/wdv131.html">Web Dev 131: Intro to Photoshop & Fireworks</a>
														<a href="../courses/wdv205/wdv205.html">Web Dev 205: Advanced CSS</a>
														<a href="../courses/wdv221/wdv221homework.html">Web Dev 221: Intro to Javascript</a>
														<a href="http://kraigpopelka.info/courses/wdv240/final">Web Dev 240: Intro to WordPress</a>
													</div> <!-- End div courses -->
											</li>
											<li><a class = "current" href = "contact.html">Contact</a></li>
									</ul>
								</nav> <!--End nav-->
								
								
								
								<h1>Contact Me</h1>
								
								<p>Please fill out the form below to send me a message.</p> 

									<form name="form1" method="post" action="contact.php">
								
										  <p>&nbsp;</p>
										  
										  <p>
											<label>Your Name:  <span class = "error"><?php echo "$nameErrMsg"; ?></span><br>
											  <input type="text" name="fullName" id="fullName" value="<?php echo $inName;?>" >
											</label>
										  </p>
										  
										  <p>Your Email:   <span class = "error"><?php echo "$emailErrMsg"; ?></span><br>
											<input type="text" name="email" id="email" value="<?php echo $inEmail;?>" >
										  </p>
										  
										  <p>Reason for contact:    <span class = "error"><?php echo "$reasonErrMsg"; ?></span><br>
											<label>
											  <select name="reason" id="reason">
												<option value="default" <?php if($inReason == "default"){echo "selected = 'selected'";}?>>Please Select a Reason</option>
												<option value="employment" <?php if($inReason == "employment"){echo "selected = 'selected'";}?>>Inquire About Employment</option>
												<option value="website technical issues" <?php if($inReason == "website technical issues"){echo "selected = 'selected'";}?>>Report a Website Problem</option>
												<option value="website critique" <?php if($inReason == "website critique"){echo "selected = 'selected'";}?>>Website Critique</option>
												<option value="other" <?php if($inReason == "other"){echo "selected = 'selected'";}?>>Other</option>
											  </select>
											</label>
										  </p>
										  
										  <p>
											<label>Comments: <br>   <span class = "error"><?php echo "$commentsErrMsg"; ?></span><br>
											  <textarea name="comments" id="comments" cols="45" rows="5" ><?php echo $inComments;?></textarea>
											</label>
										  </p>
										  
										  
										  <p class = "containField">
											<label>Phone:  <br>
											  <input type="text" name="phone" id="phone">
											</label>
										  </p>
										  
										  
										  <p>
											<input type="submit" name="submitBtn" id="submitBtn" value="Send">
											<input type="reset" name="resetBtn" id="resetBtn" value="Reset">
										  </p>
										  
										  
										  
										  
								</form>

								<p><a href = "../index.html">Back to home page</a></p>
						
						</section> <!-- End section container -->
							
<?php

					} // End else loop
		
?>

				</body>

		</html>