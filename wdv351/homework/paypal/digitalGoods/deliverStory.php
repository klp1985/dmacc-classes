<?php
    $wordDoc = "files/greatStory/great-story.docx";
    $pdfDoc = "files/greatStory/great-story.pdf";

    $html = "<h3>Your story is available as a PDF or Word document</h3><p><a href='". $wordDoc . "' download>Download as Word Document</a></p><p><a href='". $pdfDoc . "' download>Download as PDF Document</a></p>";

    echo $html;
?>