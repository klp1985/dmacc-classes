function shopifyProducts(productID, elementID) {
    var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
    if (window.ShopifyBuy) {
      if (window.ShopifyBuy.UI) {
        ShopifyBuyInit();
      } else {
        loadScript();
      }
    } else {
      loadScript();
    }
  
    function loadScript() {
      var script = document.createElement('script');
      script.async = true;
      script.src = scriptURL;
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
      script.onload = ShopifyBuyInit;
    }
  
    function ShopifyBuyInit() {
      var client = ShopifyBuy.buildClient({
        domain: 'the-uniquely-named-store.myshopify.com',
        apiKey: 'b4a09fb0ad4df734537c3d6d0e0ad9e5',
        appId: '6',
      });
  
      ShopifyBuy.UI.onReady(client).then(function (ui) {
        ui.createComponent('product', {
          id: [productID],
          node: document.getElementById(elementID),
          moneyFormat: '%24%7B%7Bamount%7D%7D',
          options: {
    "product": {
      "variantId": "all",
      "width": "240px",
      "contents": {
        "img": false,
        "imgWithCarousel": false,
        "title": false,
        "variantTitle": false,
        "price": false,
        "description": false,
        "buttonWithQuantity": false,
        "quantity": false
      },
      "styles": {
        "product": {
          "@media (min-width: 601px)": {
            "max-width": "calc(25% - 20px)",
            "margin-left": "20px",
            "margin-bottom": "50px"
          }
        },
        "button": {
          "background-color": "#5779b6",
          "font-family": "Roboto, sans-serif",
          "padding-left": "50px",
          "padding-right": "50px",
          ":hover": {
            "background-color": "#4e6da4"
          },
          ":focus": {
            "background-color": "#4e6da4"
          },
          "font-weight": "bold"
        }
      },
      "googleFonts": [
        "Roboto"
      ]
    },
    "cart": {
      "contents": {
        "button": true
      },
      "styles": {
        "cart": {
          "background-color": "#f0f0f0"
        },
        "button": {
          "background-color": "#5779b6",
          "font-family": "Roboto, sans-serif",
          ":hover": {
            "background-color": "#4e6da4"
          },
          ":focus": {
            "background-color": "#4e6da4"
          },
          "font-weight": "bold"
        },
        "title": {
          "color": "#545c86"
        },
        "footer": {
          "background-color": "#f0f0f0"
        },
        "header": {
          "color": "#545c86"
        },
        "lineItems": {
          "color": "#545c86"
        },
        "subtotalText": {
          "color": "#545c86"
        },
        "subtotal": {
          "color": "#545c86"
        },
        "notice": {
          "color": "#545c86"
        },
        "currency": {
          "color": "#545c86"
        },
        "close": {
          ":hover": {
            "color": "#545c86"
          },
          "color": "#545c86"
        },
        "emptyCart": {
          "color": "#545c86"
        }
      },
      "googleFonts": [
        "Roboto"
      ]
    },
    "modalProduct": {
      "contents": {
        "img": false,
        "imgWithCarousel": true,
        "variantTitle": false,
        "buttonWithQuantity": true,
        "button": false,
        "quantity": false
      },
      "styles": {
        "product": {
          "@media (min-width: 601px)": {
            "max-width": "100%",
            "margin-left": "0px",
            "margin-bottom": "0px"
          }
        },
        "button": {
          "background-color": "#5779b6",
          "font-family": "Roboto, sans-serif",
          "padding-left": "50px",
          "padding-right": "50px",
          ":hover": {
            "background-color": "#4e6da4"
          },
          ":focus": {
            "background-color": "#4e6da4"
          },
          "font-weight": "bold"
        }
      },
      "googleFonts": [
        "Roboto"
      ]
    },
    "toggle": {
      "styles": {
        "toggle": {
          "font-family": "Roboto, sans-serif",
          "background-color": "#5779b6",
          ":hover": {
            "background-color": "#4e6da4"
          },
          ":focus": {
            "background-color": "#4e6da4"
          },
          "font-weight": "bold"
        }
      },
      "googleFonts": [
        "Roboto"
      ]
    },
    "productSet": {
      "styles": {
        "products": {
          "@media (min-width: 601px)": {
            "margin-left": "-20px"
          }
        }
      }
    },
    "lineItem": {
      "styles": {
        "variantTitle": {
          "color": "#545c86"
        },
        "title": {
          "color": "#545c86"
        },
        "price": {
          "color": "#545c86"
        },
        "quantity": {
          "color": "#545c86"
        },
        "quantityIncrement": {
          "color": "#545c86",
          "border-color": "#545c86"
        },
        "quantityDecrement": {
          "color": "#545c86",
          "border-color": "#545c86"
        },
        "quantityInput": {
          "color": "#545c86",
          "border-color": "#545c86"
        }
      }
    }
  }
        });
      });
    }
  }
