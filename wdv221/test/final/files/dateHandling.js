
		function formatUSDate(inDate) {
			
					//alert("formatUSDate function works!");
					
					var getDay= inDate.getDate();
					
					var getMonth = inDate.getMonth() + 1;
					
					var getYear = inDate.getFullYear();
					
					var formattedDate = getMonth + "/" + getDay + "/" + getYear;
					
					return formattedDate;
			
		}
		
		
		
		function formatFullDate(inDate) {
			
						var dayList = new Array();
								dayList[0] = "Sunday";
								dayList[1] = "Monday";
								dayList[2] = "Tuesday";
								dayList[3] = "Wednesday";
								dayList[4] = "Thursday";
								dayList[5] = "Friday";
								dayList[6] = "Saturday";
								
						var monthList = new Array();
								monthList[0] = "January";
								monthList[1] = "February";
								monthList[2] = "March";
								monthList[3] = "April";
								monthList[4] = "May";
								monthList[5] = "June";
								monthList[6] = "July";
								monthList[7] = "August";
								monthList[8] = "September";
								monthList[9] = "October";
								monthList[10] = "November";
								monthList[11] = "December";
								
								
								
								var getDay= inDate.getDate();
					
								var getMonth = monthList[inDate.getMonth() ];
					
								var getYear = inDate.getFullYear();
								
								var nameDay = dayList[inDate.getDay() ];
								
								
					
								var formattedDate = nameDay + " " + getMonth + " " + getDay + ", " + getYear;
					
								return formattedDate;
			
		}