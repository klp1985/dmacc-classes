	function compareNames() { // 1.  Creates the function "compareNames()" which will determine whether two entered String values are identical or not
			
		// Declares the variables inName1 and inName2 and gets the name values entered by the user and stores them in the variables.
			var inName1 = document.getElementById("value1").value;
				
			var inName2 = document.getElementById("value2").value;
				
				
				
			if (inName1.toLowerCase() === inName2.toLowerCase()) { 
		// 2.  The if statement condition defines the comparison of the inName1 and inName2 variables to determine if they are identical.
			
		// 3.  The toLowerCase() method is used to change any potential uppercase user input to lower case.
			

		// 4 & 5.  When the condition is true, 	the string "The names are the same" is written within the span tags in the html.
				document.getElementById("displayResult").innerHTML = "The names are the same.";
			}
		
		// 4 & 5.  When the condition is false, the string "The names are different" is written within the span tags in the html.
			else {
				document.getElementById("displayResult").innerHTML = "The names are different.";
			}
			
	}
		
		
		
		
		
		// 6.  This function clears the text that is written out by the compareNames() function.  It is called by the onclick attribute tied to the button with the name attribute "reset".
	function clearText() {
				document.getElementById("displayResult").innerHTML = "";
				document.getElementById("value1").value = "";	
				document.getElementById("value2").value = "";
			}