<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Message Confirmation - Downtown Thai</title>
<style type="text/css">

	
	body {
			background-color: #1b1f26;
			color: #e0d0b5;
	}
	
	a {
			text-decoration: none;
	}
	
	a:link {
			color: #32846d
	}
	
	a:visited {
			color: #53dbb4;
	}
}
</style>
</head>

<body>
<h1>Thank You For Your Message </h1>


<hr />
<h4>Your message has been recieved.  A response will be sent promptly.
</h4>

<p>
<a href = "../adTiming.html">Return to Downtown Thai Home</a>
</p>
<?php


//It will create a table and display one set of name value pairs per row
	echo "<table border='1'>";
	echo "<tr><th>Field Name</th><th>Value of field</th></tr>";
	foreach($_POST as $key => $value)
	{
		echo '<tr>';
		echo '<td>',$key,'</td>';
		echo '<td>',$value,'</td>';
		echo "</tr>";
	} 
	echo "</table>";
	
	
	
	echo "<p>&nbsp;</p>";

//This code pulls the field name and value attributes from the Post file
//The Post file was created by the form page when it gathered all the name value pairs from the form.
//It is building a string of data that will become the body of the email

	$emailBody = "Form Data\n\n ";			//stores the content of the email
	foreach($_POST as $key => $value)		//Reads through all the name-value pairs. 	$key: field name 
											//											$value: value from the form
	{
		$emailBody.= $key."=".$value."\n";	//Adds the name value pairs to the body of the email
	} 
	
	$to = "web@kraigpopelka.info";				//change this to the address you wish to send the form information
 	$subject = "Downtown Thai Contact";			//change this for Subject line
 	if (mail($to, $subject, $emailBody)) 	//puts pieces together and emails
	{
   		echo("<p>Message successfully sent!</p>");
  	} 
	else 
	{
   		echo("<p>Message delivery failed...</p>");
  	}

?>

</body>
</html>
