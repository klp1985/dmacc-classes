		
		
		
		
		
		function mealCount() {
			
				var deliveryFee = 0;
				
				
				curryQuantity = document.getElementById("numberCurry").value;
				larbQuantity = document.getElementById("numberLarb").value;
				padThaiQuantity = document.getElementById("numberPadThai").value;
			
				var mealCounter = parseFloat(curryQuantity) + parseFloat(larbQuantity) + parseFloat(padThaiQuantity);
		
				if(mealCounter >= 3) {
						
						var divide = Math.floor(mealCounter / 3);
						var remainder = mealCounter % 3;
						
						
						if (remainder != 0) {
								
								deliveryFee =  Math.floor(mealCounter/3) * 3.5;
							
						}
						
						
						
						else  {
							
								deliveryFee = (mealCounter / 3) * 3.5;
							
						}
							
				}
			
				return deliveryFee;
		}
		
	
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */	
	
		
		function selectCurry() {
			
				var redCurryPrice = 0;
				
				var redCurry = document.getElementById("checkRedCurry");
				
				var curryQuantity  = parseInt(document.getElementById("numberCurry").value);

				
				
				if (redCurry.checked == true) {
					
						redCurryPrice = parseFloat(redCurry.value);
						
						
						redCurryPrice = redCurryPrice * curryQuantity;
						
						document.getElementById("displayOrder").style.display = "inherit";
						
						document.getElementById("itemizedList").innerHTML = "<p>Red Curry Chicken   X   " + curryQuantity + ".........." + dollarFormat(redCurryPrice + "") + "</p>";
						
						document.getElementById("itemizedList").style.display = "inherit";
					
				}
				
				else {
					
						redCurryPrice = 0;
						
						document.getElementById("numberCurry").value = 0;
						
						document.getElementById("itemizedList").style.display = "none";
				}
				
				return redCurryPrice;
				
		}		
		
		
		function selectLarb() {
		
				var larbPrice = 0;
				
				var larb = document.getElementById("checkLarb");
				
				var larbQuantity  = parseInt(document.getElementById("numberLarb").value);
				
				
				
				if (larb.checked == true) {
					
						larbPrice = parseFloat(larb.value);				
						
						larbPrice = larbPrice * larbQuantity;
						
						document.getElementById("displayOrder").style.display = "inherit";
						
						document.getElementById("itemizedList2").innerHTML = "<p>Beef Larb   X   " + larbQuantity + "........................" + dollarFormat(larbPrice + "") + "</p>";
						
						document.getElementById("itemizedList2").style.display = "inherit";
					
				}
				
				else {
						
						larbPrice = 0;
						
						document.getElementById("numberLarb").value = 0;
						
						
						document.getElementById("itemizedList2").style.display = "none";
					
				}
				
				return larbPrice;
			
		}
		
		
		function selectPadThai() {
			
				var padThaiPrice = 0;
				
				var padThai = document.getElementById("checkPadThai");
				
				var padThaiQuantity  = parseInt(document.getElementById("numberPadThai").value);
				
				
				
				if (padThai.checked == true) {
					
						padThaiPrice = parseFloat(padThai.value);
											
						padThaiPrice = padThaiPrice * padThaiQuantity;
						
						document.getElementById("displayOrder").style.display = "inherit";
						
						document.getElementById("itemizedList3").innerHTML = "<p>Veg Pad Thai   X   " + padThaiQuantity + "..................." + dollarFormat(padThaiPrice + "") + "</p>";
						
						document.getElementById("itemizedList3").style.display = "inherit";
					
				}
				
				else {
					
						padThaiPrice = 0;
						
						document.getElementById("numberPadThai").value = 0;
						
						
						document.getElementById("itemizedList3").style.display = "none";
				}
				
				
				
				return padThaiPrice;
				
				
				
		}
	

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */		

	
		
		function displayTotal() {
			
					var fee = mealCount();
					
					var grandTotal = selectCurry() + selectLarb() + selectPadThai() + mealCount();	
					
					document.getElementById("displayFee").innerHTML = dollarFormat(fee + "");
					
					document.getElementById("displayTotal").innerHTML = dollarFormat(grandTotal + "");
		}
		
		
		
		
		function displayDeliverName() {
			
					var inName = document.getElementById("delName").value;
					
					document.getElementById("placeDelName").innerHTML = "<p>Name:   " + inName + "</p>";
			
		}
		
		function displayDeliverEmail() {
			
					var inEmail = document.getElementById("delEmail").value;
					
					document.getElementById("placeDelEmail").innerHTML = "<p>Email:   " + inEmail + "</p>";
			
		}
		
		function displayDeliverPhone() {
			
					var inPhone = document.getElementById("delPhone").value;
					
					document.getElementById("placeDelPhone").innerHTML = "<p>Phone:   " + inPhone + "</p>";
			
		}
		
		function displayDeliverAddress() {
			
					var inAddress = document.getElementById("delAddress").value;
					
					document.getElementById("placeDelAddress").innerHTML = "<p>Address:   " + inAddress + "</p>";
			
		}
		
		function displayDeliverApt() {
			
					var inApt = document.getElementById("delApt").value;
					
					document.getElementById("placeDelApt").innerHTML = "<p>Apt:   " + inApt + "</p>";
			
		}
		
		function displayDeliverCity() {
			
					var inCity = document.getElementById("delCity").value;
					
					document.getElementById("placeDelCity").innerHTML = "<p>City:   " + inCity + "</p>";
			
		}
		
		function displayDeliverZip() {
			
					var inZip = document.getElementById("delZip").value;
					
					document.getElementById("placeDelZip").innerHTML = "<p>Zip:   " + inZip + "</p>";
			
		}
		
		function displayDeliverInstruct() {
			
					var inInstruct = document.getElementById("delInstruct").value;
					
					document.getElementById("placeDelInstruct").innerHTML = "<p>Special Instructions:   " + inInstruct + "</p>";
			
		}
		

/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */			
		
		function resetOrderForm() {
			
					document.getElementById("validateAlert").innerHTML = "";
					document.getElementById("validateAlert2").innerHTML = "";
					document.getElementById("validateAlert3").innerHTML = "";
					document.getElementById("validateAlert4").innerHTML = "";
					document.getElementById("validateAlert5").innerHTML = "";
					document.getElementById("validateAlert6").innerHTML = "";
					
					document.getElementById("displayFee").innerHTML = "";
					document.getElementById("displayTotal").innerHTML = "";
					
					document.getElementById("placeDelName").innerHTML = "";
					document.getElementById("placeDelEmail").innerHTML = "";
					document.getElementById("placeDelPhone").innerHTML = "";
					document.getElementById("placeDelAddress").innerHTML = "";
					document.getElementById("placeDelApt").innerHTML = "";
					document.getElementById("placeDelCity").innerHTML = "";
					document.getElementById("placeDelZip").innerHTML = "";
					document.getElementById("placeDelInstruct").innerHTML = "";
					
					document.getElementById("displayOrder").style.display = "none";
			
		}
		
		function resetContactForm() {
			
					document.getElementById("validateAlert7").innerHTML = "";
					document.getElementById("validateAlert8").innerHTML = "";
					document.getElementById("validateAlert9").innerHTML = "";
					document.getElementById("validateAlert10").innerHTML = "";

		}
		
/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */	