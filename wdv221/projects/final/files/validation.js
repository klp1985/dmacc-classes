		function orderFormValidate() {
		
					var inName = document.forms["quickMealOrder"]["delName"].value;
					
					if (inName == "") {
							document.getElementById("validateAlert").innerHTML = "<p style = 'color: #ff2828;'>Name is required!</p>";
							
					}
					
					var inEmail = document.forms["quickMealOrder"]["delEmail"].value;
					
					if (inEmail == "") {
							document.getElementById("validateAlert2").innerHTML = "<p style = 'color: #ff2828;'>Email address is required!</p>";
							
					}
					
					var inPhone = document.forms["quickMealOrder"]["delPhone"].value;
					
					if (inPhone == "") {
							document.getElementById("validateAlert3").innerHTML = "<p style = 'color: #ff2828;'>Phone number is required!</p>";
							
					}
					
					var inAddress = document.forms["quickMealOrder"]["delAddress"].value;
					
					if (inAddress == "") {
							document.getElementById("validateAlert4").innerHTML = "<p style = 'color: #ff2828;'>Delivery address is required!</p>";
							
					}
					
					var inCity = document.forms["quickMealOrder"]["delCity"].value;
					
					if (inCity == "") {
							document.getElementById("validateAlert5").innerHTML = "<p style = 'color: #ff2828;'>City is required!</p>";
							
					}
					
					var inZip = document.forms["quickMealOrder"]["delZip"].value;
					
					if (inZip == "") {
							document.getElementById("validateAlert6").innerHTML = "<p style = 'color: #ff2828;'>Zipcode is required!</p>";
							return false;
					}
		
		}
		
		
		function contactFormValidate() {
			
					var inName = document.forms["feedback"]["contactName"].value;
					
					if (inName == "") {
							document.getElementById("validateAlert7").innerHTML = "<p style = 'color: #ff2828;'>Name is required!</p>";
							
					}
					
					var inEmail = document.forms["feedback"]["contactEmail"].value;
					
					if (inEmail == "") {
							document.getElementById("validateAlert8").innerHTML = "<p style = 'color: #ff2828;'>Email address is required!</p>";
							
					}
					
					var inPhone = document.forms["feedback"]["contactPhone"].value;
					
					if (inPhone == "") {
							document.getElementById("validateAlert9").innerHTML = "<p style = 'color: #ff2828;'>Phone number is required!</p>";
							
					}
					
					var inMessage = document.forms["feedback"]["contactMessage"].value;
					
					if (inMessage == "") {
							document.getElementById("validateAlert10").innerHTML = "<p style = 'color: #ff2828;'>Message is required!</p>";
							return false;
					}
					
					
			
		}