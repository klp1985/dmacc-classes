/*  Daily specials function that will display different specials on different days and different times of day */

		function todaySpecials() {		//Displays the days current special below the heading.
													
															//alert("Works!");
															
															var date = new Date();
															
															
						/*			// Date Testing -----------------------------------------------------------------------------------------------------------
															var daytoset = 6;  // Set the date from 0 - 6; 0 being Sunday and 6 being Saturday.
															
															var currentDay = date.getDay();
															var distance = (daytoset + 7 - currentDay) % 7;
															date.setDate(date.getDate() + distance);
						
						
															date.setHours(18);  // Enter the hour of day using the 24 hour clock.
															
															date.setMinutes(0);  // Enter the minute of the hour.
							*/		//-------------------------------------------------------------------------------------------------------------------------------	
							
							
															var dayOfWeek = date.getDay();
															
															var hours = date.getHours();
															
															var min = date.getMinutes();
															
															//alert(hours);
															
																	//For Monday
															
																	if (dayOfWeek == 1) {
																	
																			if (hours * 60 + min >= 450 && hours < 12) { // 7:30AM - 11:59AM
																			
																						//alert("monday special is live");
																						
																						document.getElementById("monday").style.display = "inherit";
																			
																			} else {  // 12:00AM - 7:29AM and 12:00PM - 11:59PM
																			
																						//alert("monday special not live");
																			
																						document.getElementById("noSpecial").style.display = "inherit";
																			
																			}
																	
																	}
																	
																	
																	
																	//For Tuesday
															
																	if (dayOfWeek == 2) {
																	
																			if (hours * 60 + min >= 450 && hours < 12) { // 7:30AM - 11:59AM
																			
																						//alert("Tuesday lunch special is live");
																						
																						document.getElementById("tuesdayLunch").style.display = "inherit";
																			
																			} 
																			
																			else if (hours >= 13 && hours < 20) {  // 1:00PM - 7:59PM
																			
																						//alert("Tuesday dinner special is live");
																						
																						document.getElementById("tuesdayDinner").style.display = "inherit";
																			
																			} 
																			
																			else { // 12:00AM - 7:29AM & 12:00PM - 12:59PM & 8:00PM - 11:59PM
																			
																						//alert("Tuesday dinner special not live");
																						
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																			}
																	
																	}
																	
																	
																	
																	//For Wednesday
															
																	if (dayOfWeek == 3) {
																	
																			if (hours * 60 + min >= 450 && hours < 12) { // 7:30AM - 11:59AM
																			
																						//alert("Wednesday lunch special is live");
																						
																						document.getElementById("wednesdayLunch").style.display = "inherit";
																			
																			} 
																			
																			
																			
																			else if (hours >= 13 && hours < 20) {  // 1:00PM - 7:59PM
																			
																						//alert("Wednesday dinner special is live");
																						
																						document.getElementById("wednesdayDinner").style.display = "inherit";
																			
																			} 
																			
																			else { // // 12:00AM - 7:29AM & 12:00PM - 12:59PM & 8:00PM - 11:59PM
																			
																						//alert("Wednesday dinner special not live");
																						
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																			}
																	
																	}
																	
																	
																	
																	//For Thursday
															
																	if (dayOfWeek == 4) {
																	
																			if (hours * 60 + min >= 450 && hours < 12) {  // 7:30AM - 11:59AM
																			
																						//alert("Thursday lunch special is live");
																						
																						document.getElementById("thursdayLunch").style.display = "inherit";
																			
																			} 
																			
																			else if (hours >= 13 && hours < 20) {  // 1:00PM - 7:59PM
																			
																						//alert("Thursday dinner special is live");
																						
																						document.getElementById("thursdayDinner").style.display = "inherit";
																			
																			} 
																			
																			else { // 12:00AM - 7:29AM & 12:00PM - 12:59PM & 8:00PM - 11:59PM
																			
																						//alert("Thursday dinner special not live");
																						
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																			}
																	
																	}
																	
																	
																	
																	//For Friday
															
																	if (dayOfWeek == 5) {
																	
																			if (hours * 60 + min >= 450 && hours < 12) {  // 7:30AM - 11:59AM
																			
																						//alert("Friday lunch special is live");
																						
																						document.getElementById("fridayLunch").style.display = "inherit";
																			
																			} 
																			
																			 else if ((hours >= 13 && hours <14)||(hours >= 17 && hours < 20)) {  // 1:00 PM - 1:59 PM or 5:00PM - 7:59PM
																			
																						//alert("Friday dinner special is live");
																						
																						document.getElementById("fridayDinner").style.display = "inherit";
																			
																			}
																			
																			else if (hours >= 14 && hours < 17) { // 2:00PM - 4:59PM
																			
																						//alert("Friday happy hour special is live");
																						
																						document.getElementById("fridayHappyHour").style.display = "inherit";
																						
																						document.getElementById("fridayDinner").style.display = "inherit";
																			
																			} 
																			
																			 else  { // 12:00AM - 7:29AM & 12:00PM - 12:59PM & 8:00PM - 11:59PM
																			
																						//alert("Friday dinner special not live");
																						
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																			}
																	
																	}
																	
																	
																	
																	//For Saturday
															
																	if (dayOfWeek == 6) {
																	
																			if (hours >= 14 && hours < 17) { // 2:00PM - 4:59PM
																			
																						//alert("Saturday happy hour special is live");
																						
																						document.getElementById("saturdayHappyHour").style.display = "inherit";
																			
																			} 
																			
																			 if (hours >= 13 && hours < 20) {  // 1:00PM - 7:59PM
																			
																						//alert("Saturday dinner special is live");
																						
																						document.getElementById("saturdayDinner").style.display = "inherit";
																			
																			} else { //12:00AM - 1:59PM & 8:00PM - 11:59PM
																			
																						//alert("Saturday dinner special not live");
																						
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																			}
																	
																	}
																	
																	
																	//For Sunday
															
																	if (dayOfWeek == 0) {
																	
																						document.getElementById("noSpecial").style.display = "inherit";
																						
																	}
															
													}