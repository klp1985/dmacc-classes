var recipes= [
{
    "recipeName":"Chorizo Sweet Potato Skillet",
    "recipeImage":"images/chorizosweetpotatoskillet.jpg",
    "serves":6,
    "prepTime":"55 Minutes",
    "difficulty":"Easy",
    
    "ingredients": [
        {
            "name":"Olive Oil",
            "quantity":1,
            "quantityUnit":"tbsp."
        },
        {
            "name":"Sweet Potato",
            "quantity":1,
            "quantityUnit":"lb"
        },
        {
            "name":"Black Beans",
            "quantity":1,
            "quantityUnit":"can"
        },
        {
            "name":"Chorizo",
            "quantity":0.5,
            "quantityUnit":"lb"
        },
        {
            "name":"Uncooked Long Grain White Rice",
            "quantity":1,
            "quantityUnit":"cup"
        },
        {
            "name":"Salsa(Red or Green)",
            "quantity":1,
            "quantityUnit":"cup"
        },
        {
            "name":"Chicken Broth",
            "quantity":1.75,
            "quantityUnit":"cups"
        },
        {
            "name":"Shredded Cheese",
            "quantity":1,
            "quantityUnit":"cup"
        },
        {
            "name":"Green Onions",
            "quantity":2,
            "quantityUnit":""
        }
],
    "instructions": 
    {
        "step1":"Peel and dice the sweet potato into 1/2 to 3/4 inch cubes (size matters, make them small). Sauté the sweet potato cubes in a large skillet with olive oil over medium heat for about 5 minutes, or until the sweet potatoes have softened about half way through (they'll cook more later).", 
        "step2":"Squeeze the chorizo out of its casing into the skillet with the sweet potatoes. Sauté the chorizo and sweet potatoes together, breaking the chorizo up into small pieces as it browns.",
        "step3":"Once the chorizo is fully browned, pour off any excess grease if needed. Rinse and drain the black beans. Add the beans, salsa, and uncooked rice to the skillet. Stir them into the sweet potatoes and chorizo until everything is well combined.",
        "step4":"Add the chicken broth, stir briefly, then place a lid on the skillet. Allow the contents of the skillet to come up to a boil, then turn the heat down to low. Let the skillet simmer on low for 30 minutes. Make sure it is simmering the whole time (you should be able to hear it quietly simmer away). If it is not, turn the heat up slightly.",
        "step5":"After 30 minutes the rice should be tender and have absorbed all of the liquid. Turn off the heat, fluff the mixture, sprinkle the cheese on top, then return the lid to trap the residual heat and help the cheese melt. Slice the green onions while the cheese is melting, then sprinkle them on top and serve."
    }
},
{
    "recipeName":"Dragon Noodles",
    "recipeImage":"images/dragonnoodles.jpg",
    "serves":2,
    "prepTime":"15 minutes",
    "difficulty":"Medium",
    
    "ingredients": [
        {
            "name":"Lo Mein Noodles",
            "quantity":4,
            "quantityUnit":"oz."
        },
        {
            "name":"butter",
            "quantity":2,
            "quantityUnit":"tbsp"
        },
        {
            "name":"Crushed Red Pepper",
            "quantity":0.25,
            "quantityUnit":"tsp"
        },
        {
            "name":"Egg",
            "quantity":1,
            "quantityUnit":""
        },
        {
            "name":"Brown Sugar",
            "quantity":1,
            "quantityUnit":"tbsp"
        },
        {
            "name":"Soy Sauce",
            "quantity":1,
            "quantityUnit":"tbsp"
        },
        {
            "name":"Sriracha",
            "quantity":1,
            "quantityUnit":"tbsp"
        },
        {
            "name":"Cilantro",
            "quantity":1,
            "quantityUnit":"handful"
        },
        {
            "name":"sliced Green Onion",
            "quantity":1,
            "quantityUnit":""
        }
],
    "instructions": 
    {
        "step1":"Begin to boil water for the noodles. Once the water reaches a full boil, add the noodles and cook according to the package directions (boil for 5-7 minutes).", 
        "step2":"While waiting for the water to boil, prepare the sauce. In a small bowl stir together the brown sugar, soy sauce, and sriracha.",
        "step3":"In a large skillet melt butter over medium-low heat. Add the red pepper to the butter as it melts. Whisk an egg in a bowl and then add to the melted butter. Stir gently and cook through. Once the egg is done cooking, turn off the heat.",
        "step4":"When the noodles are tender, drain the water and then add them to the skillet with the cooked egg. Also add the prepared sauce. Turn the heat on to low to evaporate excess moisture, and stir until everything is coated well with the sauce. Sprinkle the sliced green onions and cilantro leaves (whole) on top and serve!"
    }
},
{
    "recipeName":"Spinach Lasagna Roll Ups",
    "recipeImage":"images/spinachlasagnarollups.jpg",
    "serves":7,
    "prepTime":"1 hour",
    "difficulty":"Easy",
    
    "ingredients": [
        {
            "name":"Lasagna Noodles",
            "quantity":1,
            "quantityUnit":"lb."
        },
        {
            "name":"Ricotta",
            "quantity":15,
            "quantityUnit":"oz"
        },
        {
            "name":"Shredded Mozzarella",
            "quantity":1,
            "quantityUnit":"cup"
        },
        {
            "name":"Grated Parmesan",
            "quantity":0.25,
            "quantityUnit":"cup"
        },
        {
            "name":"Egg",
            "quantity":1,
            "quantityUnit":""
        },
        {
            "name":"Spinach",
            "quantity":10,
            "quantityUnit":"oz"
        },
        {
            "name":"Marinara Sauce",
            "quantity":2.5,
            "quantityUnit":"cups"
        },
        {
            "name":"Salt and Pepper",
            "quantity":null,
            "quantityUnit":"to taste"
        },
        {
            "name":"Non-stick Spray",
            "quantity":null,
            "quantityUnit":"as needed"
        }
],
    "instructions": 
    {
        "step1":"Get a large pot of water boiling with a dash of salt. When it comes to a full boil, add the lasagna noodles and cook until al dente (soft but not soggy… about 12-15 minutes). When they are finished cooking, drain in a colander.", 
        "step2":"While the noodles are boiling, prepare the filling. Thaw the package of frozen spinach in the microwave and then squeeze out as much excess liquid as possible. Combine the spinach in a bowl with the ricotta, mozzarella, parmesan, egg, freshly grated pepper and about 1/4 tsp of salt. Mix until well combined.",
        "step3":"When the noodles and filling are ready to go, preheat the oven to 400 degrees. Prepare a glass casserole dish by spraying with non-stick spray.",
        "step4":"On a clean surface, lay out a few noodles at a time. Place a few tablespoons of filling on each noodle and spread to cover from edge to edge. The filling does not need to be thick because once the noodle is rolled up, it will be compounded. Make sure to spread it all the way to the edges of the noodles.",
        "step5":"Roll the noodles up and place in the prepared casserole dish. Repeat until all of your filling is gone (there may be some noodles left over, these are “back ups” in case any of the others rip during assembly). Pour the marinara sauce over the rolled noodles making sure to cover all surfaces. The sauce will keep the noodles hydrated and soft while baking.",
        "step6":"Cover the dish in foil and bake for 30 minutes. Serve hot or divide into individual portions and refrigerate."
    }
}

];