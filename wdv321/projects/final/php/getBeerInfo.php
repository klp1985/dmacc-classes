<?php
    session_start();
    $exportArray = [];
    $beerID = $_POST['beerID'];
    $beerName = '';
    $breweryName = '';
    $beerRating = '';
    $beerNotes = '';
    $imagePath = '';

    include 'connectPDO.php';

    $sql = "SELECT beer_name, beer_brewery, beer_rating, beer_notes, image_path FROM beer WHERE beer_id = :beer_id";
    try {
        
            $stmt = $conn->prepare($sql);
            $stmt->bindParam(':beer_id', $beerID);
            $stmt->execute();
        
            if ($stmt->execute()) {
                while($row = $stmt->fetch()) {
                    $beerName = $row['beer_name'];
                    $breweryName = $row['beer_brewery'];
                    $beerRating = $row['beer_rating'];
                    $beerNotes = $row['beer_notes'];
                    $imagePath = $row['image_path'];
                }
                $conn = null;
            } else {
                $displayMsg = "There was an error processing your request.";
            }
    } catch (PDOException $e) {
              echo "there was an error with your request" . $e->getMessage();
    }

    array_push($exportArray, $beerID, $beerName, $breweryName, $beerRating, $beerNotes, $imagePath);

    echo json_encode($exportArray);

?>