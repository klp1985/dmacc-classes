<?php
    session_start();
    $importArray = [];
    $formResultMsg = '';
    $beerNameMsg = '';
    $breweryNameMsg = '';
    $notesMsg = '';
    $beerID = '';
    $formSet = false;

    if (!isset($_SESSION['validUser']) || $_SESSION['validUser'] != "yes") {
        header('Location: ../index.html');
    } else {
        
        if (isset($_SESSION['export_array'])) {
            $formSet = true;
            $importArray = $_SESSION['export_array'];
            if ($importArray[0]=="valid_success") {
                $formResultMsg = $importArray[1];
                
            } else if ($importArray[0]=="valid_fail") {
                $beerNameMsg = $importArray[1];
                $breweryNameMsg = $importArray[2];
                $notesMsg = $importArray[3];
            }
            unset($_SESSION['export_array']);
        } 

        $userEmail = $_SESSION['userEmail'];
        $getUsername = explode("@", $userEmail);
        $userName = $getUsername[0];
        $userId = $_SESSION['user_id'];
        $displayList = "";
        
        include 'connectPDO.php';

        $sqlList = "SELECT beer_name, beer_brewery, beer_rating, beer_id FROM beer WHERE user_id = :user_id";

        $stmt = $conn->prepare($sqlList);
        $stmt->bindParam(':user_id', $userId);
        $stmt->execute();
        if ($stmt->execute()) {
            
                $displayList .= "<div class = 'container'>";
                $displayList .= "<h3>Your Beers</h3>";
                $displayList .= "<table class='tg'>";
                $displayList .= "<tr>";
                $displayList .= "<th class='tg-aq88'>Beer Name</th>";
                $displayList .= "<th class='tg-aq88'>Brewery</th>";
                $displayList .= "<th class='tg-aq88'></th>";
                $displayList .= "<th class='tg-wr1b'></th>";
                $displayList .= "</tr>";

                while($row = $stmt->fetch()) {
                    // Place each message into table
                        $displayList .= "<tr>";
                        $displayList .= "<td class='tg-yzt1'>" . $row['beer_name'] . "</td>";
                        $displayList .= "<td class='tg-yzt1'>" . $row['beer_brewery'] . "</td>";
                        $displayList .= "<td class='tg-yzt1'><a href='#' class='beerListItem button button-primary' id=beerId-" . $row['beer_id'] . "' onclick='viewBeerInfo(".$row['beer_id'].")'>View</a></td>";
                        $displayList .= "<td class='tg-yzt1'><a href='deletePage.php?keyid=" . $row['beer_id'] . "&tname=beer' class='button button-primary'>Delete</a></td>";
                        $displayList .= "</tr>\n";

                }

                $displayList .= "</table>";
                $displayList .= "</div>";
                
                $conn = null;
        }

?>
<!DOCTYPE html>
<html>
  <head>
      <title>Deja Brew</title>

      <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">  
      <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="../vendor/skeleton/normalize.css">
      <link rel="stylesheet" href="../vendor/skeleton/skeleton.css">
      <link rel="stylesheet" type="text/css" href="../css/style.css">
      <link rel="stylesheet" type="text/css" href="../vendor/jquery-ui.css">
      <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script> 
      <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
       
      <script src="../js/capturePicture.js"></script>
      <script src='../js/viewBeerInfo.js'></script>
      <script src='../js/beerToServe.js'></script>

      <script>
            $(document).ready(function() { 

                beerEntry();

<?php if ($formSet) { ?>
                $( function() {
                    $( "#dialog-message" ).dialog({
                    modal: true,
                    buttons: {
                        Ok: function() {
                        $( this ).dialog( "close" );
                        }
                    }
                    });
                } );
<?php } ?>
                
                $( function() {
                    $( "#insertInfo" ).dialog({
                        width: 700,
                        modal: true,
                        autoOpen: false
                    });
                } );

                $('#addBeerBtn').click(function() {
                    $( "#insertInfo" ).dialog( "open" );
                    return false;
                });

                $( function() {
                    $( "#viewBeerInfo" ).dialog({
                        width: 750,
                        modal: true,
                        autoOpen: false,
                        close: function( event, ui ) {
                        $("#viewBeerRating").html("");
                    }
                    });
                } );

                $('.beerListItem').click(function() {
                    $( "#viewBeerInfo" ).dialog( "open" );
                    return false;
                });

                $('#takeImage').click(function() {
                    $('#player, #capture').css('display', 'inherit');
                    $('#takeImage, #imageUploadContainer,#orText').css('display', 'none');
                    captureImage();
                });  

                $('#takeImage').click(function() {
                    $('#capImageOverlay').css('display','block');
                });

                $('#capture').click(function() {
                    $('#capImageOverlay').css('display','none');
                });

                $('#uploadImage').click(function() {
                    $('#takeImage,#orText').css('display','none');
                });

                $('#closeImageCap').click(function() {
                    player.srcObject.getVideoTracks().forEach(track => track.stop());
                    $('#capImageOverlay').css('display','none');
                    $('#imageUploadContainer, #takeImage').css('display','inherit');
                });

                $('#resetForm').click(function() {
                    $('#takeImage, #imageUploadContainer,#orText').css('display', 'inherit');
                    $('#player, #capture, #canvas').css('display', 'none');
                    $('#beerNameMsg, #breweryNameMsg, #ratingMsg, #beerNotesMsg').html('');
                    $('#beerFormSubmit').html('Submit Info');
                });

            });
      </script>

<body>

<div id="dialog-message" title="Form Submit Result">
    <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
    <span id="formResult"><?php echo $formResultMsg;?></span>
</div>

<div class="row">
    <div id="loginPageText">
        <h1>Deja Brew</h1>
    </div><!-- End loginPageText -->
    <hr>
</div><!-- End row -->

<div class="row">
    <div class="one-fourth column" id="showUsername">
        <h2>Logged in as: <?php echo $userName;?></h2>
    </div><!-- End four column -->
    <hr>
</div><!-- End row -->

<div class="container">
    <div class="row">
        <div class="one-half column">
            <div id="addBeerBtnContainer">
                <button type="button" class="button-primary" id="addBeerBtn">Add Beer!</button>
            </div>
        </div><!-- End one-half column -->
        
        <div class="one-half column" id="logoutBtnContainer">
            <a class="button button-primary" href="logout.php">Logout</a>
        </div><!-- End one-half column -->
    </div><!-- End row -->
</div><!-- End container -->

<div id="beerList">
    <?php echo $displayList;?>
</div>


<div id="insertInfo">
<form action="handleBeerInput.php" method="post" enctype="multipart/form-data" id="beerEntryForm">
  <div id="formText">
      <h3>Add Beer</h3>
    
    <div id="beerNameInput">
        <p>Beer Name:</p><span id="beerNameMsg"></span>
        <input type="text" size="30" name = "beerName" id="beerName">
    </div>
    <div id="breweryNameInput">
        <p>Brewery:</p><span id="breweryNameMsg"></span>
        <input type="text" size="30" name = "breweryName" id="breweryName">
    </div>
    <div id="beerRatingInput" class="beerRating">
        <p>Rating:</p><span id="ratingMsg"></span>
            <input class="rating beerRate-5" id="rating-5" type="radio" name="rating" value="5" />
            <label class="rating beerRate-5" for="rating-5"></label>
            <input class="rating beerRate-4" id="rating-4" type="radio" name="rating"  value="4"/>
            <label class="rating beerRate-4" for="rating-4"></label>
            <input class="rating beerRate-3" id="rating-3" type="radio" name="rating"  value="3"/>
            <label class="rating beerRate-3" for="rating-3"></label>
            <input class="rating beerRate-2" id="rating-2" type="radio" name="rating"  value="2"/>
            <label class="rating beerRate-2" for="rating-2"></label>
            <input class="rating beerRate-1" id="rating-1" type="radio" name="rating"  value="1"/>
            <label class="rating beerRate-1" for="rating-1"></label>
    </div>
    <div id="beerNotesInput">
        <p>Notes:</p><span id="beerNotesMsg"></span>
        <textarea rows="6" cols="30" name = "beerNotes" id="beerNotes"></textarea>
    </div>
      <h5>Image:</h5>
      <p>
          <input type="hidden" name="captureImagePath" id="captureImagePath" >
          <button type="button" id="takeImage">Take Picture</button>          
      </p>
    
      <div id="capImageOverlay">
        <div id="capImageOverlayItems">
        <button type="button" class="button-primary" id="closeImageCap">Cancel</button>
        <video id="player" autoplay></video>
        <button type="button" class="button-primary" id="capture">Capture</button>
        </div>
      </div>
      <canvas id="canvas" width=360 height=480></canvas>
      
      <h4 id="orText">Or</h4>

      <div id="imageUploadContainer">
        Select image to upload:
        <input type="file" name="uploadImage" id="uploadImage">
      </div>  
    
    <button type="button" id="beerFormSubmit" class="button" name="sendForm">Submit Info</button>
    <!--<input type="submit" value="Submit Info" name="submit">-->
    <button type="reset" value="Reset" id="resetForm">Reset</button>

        </div>
    </form>
    </div>

    <div id='viewBeerInfo'>
        <div id='beerInfoText'>
            <h4>Beer: <span id="viewBeerName"></span></h4>
            <h3>Rating: <span id="viewBeerRating"></span></h3>
            <img src='' id="viewBeerImage"/>
            <h4>Brewery: <span id="viewBreweryName"></span></h4>
            <h5>Notes:</h5>
            <p id="viewBeerNotes"></p> 
        </div>
    </div>
    
</body>
</html>
<?php       
    }    //End else
?>