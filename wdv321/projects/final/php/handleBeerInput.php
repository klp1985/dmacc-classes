<?php
    session_start();
    
    $userEmail = $_SESSION['userEmail'];
    $userId = $_SESSION['user_id'];
    $username = $_SESSION['username'];
    $validForm = true;
    $exportArray = [];
    $resultMsg = '';
     
    $inBeerName = $_POST['beerName'];
    $inBreweryName = $_POST['breweryName'];
    $inRating = $_POST['rating'];
    $inBeerNotes = $_POST['beerNotes'];
    $inImagePath = '';

    include 'validations.php';
    include 'imageUpload.php';

    $imageUpload = new ImageUpload();

    //If a file was submitted then get file information.  Then pass information to image upload object and store the result.
    if($_FILES['uploadImage']['error']==0) {
        $file_name = $_FILES['uploadImage']['name'];
        $file_size = $_FILES['uploadImage']['size'];
        $file_tmp = $_FILES['uploadImage']['tmp_name'];
        $file_type= $_FILES['uploadImage']['type'];

        $imageUpload->set_filename($file_name);
        $imageUpload->set_filesize($file_size);
        $imageUpload->set_filetmp($file_tmp);
        $imageUpload->set_filetype($file_type);
        $imageUpload->set_username($username);

        $productFullImageErrMsg = $imageUpload->uploadFullImage();

        $inImagePath = $imageUpload->get_fullPath();
    } else {
        $inImagePath = $_POST['captureImagePath'];
    }

    $validation = new Validations();


    $beerNameResult = '';
    $breweryNameResult = '';
    $beerNotesResult = '';
    
    $validation->set_validForm($validForm);

    $validation->set_name($inBeerName);
    $beerNameResult = $validation->validateName();
    $inBeerName = $validation->get_name();

    $validation->set_name($inBreweryName);
    $breweryNameResult = $validation->validateName();
    $inBreweryName = $validation->get_name();

    $validation->set_userMsg($inBeerNotes);
    $beerNotesResult = $validation->validateUserMsg();
    $inBeerNotes = $validation->get_userMsg();
    
    $validForm = $validation->get_validForm();

    if ($validForm) {

        include "connectPDO.php";

        try {
            $sql = "INSERT INTO beer (beer_name, beer_brewery, beer_rating, beer_notes, user_id, image_path) VALUES (:beer_name, :beer_brewery, :beer_rating, :beer_notes, :user_id, :image_path)";
    
            $sqlPrepare = $conn->prepare($sql);
            $sqlPrepare->bindParam(':beer_name', $inBeerName);
            $sqlPrepare->bindParam(':beer_brewery', $inBreweryName);
            $sqlPrepare->bindParam(':beer_rating', $inRating);
            $sqlPrepare->bindParam(':beer_notes', $inBeerNotes);
            $sqlPrepare->bindParam(':user_id', $userId);
            $sqlPrepare->bindParam(':image_path', $inImagePath);
        } catch (PDOException $e) {
            $resultMsg = "There was a problem entering the information.  Please try again: " . $e->getMessage();
        }
        $conn = null;
    
        if ($sqlPrepare->execute()){
            $resultMsg = "<h2>Your Beer Has Been Added!</h2>";
            $resultMsg .= "<p>Please Drink Responsibly</p>";
        } else {
            $resultMsg = "<h3>A Small Problem Occurred.</h3>";
            $resultMsg .= "<p>There was an error processing your information.</p>";
            $resultMsg .= "<p>Please try again.</p>";
        }

        array_push($exportArray, "valid_success", $resultMsg);
    } else {
        array_push($exportArray, "valid_fail", $beerNameResult, $breweryNameResult, $beerNotesResult);
    }

    $_SESSION['export_array'] = $exportArray;
    header('Location: dashboard.php');
    
?>