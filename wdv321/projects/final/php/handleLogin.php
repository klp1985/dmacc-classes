<?php
    session_start();
    $_SESSION['validUser'] = "no";	
    $exportArray = [];
    $resultMsg = '';
    $pass_hash = '';
    $userId = '';
    $retrievedEmail = '';
    $retrievedUsername = '';

    $inPass = $_POST['passValue'];
    $inEmail = $_POST['emailValue'];

    include 'connectPDO.php';

    $sql = "SELECT user_email, user_pass_hash, user_id, user_username FROM users WHERE user_email = :user_email;";
    
    $stmt = $conn->prepare($sql);
    
    $stmt->bindParam(':user_email', $inEmail);
    $stmt->execute();
    //$result = $stmt->fetch(\PDO::FETCH_ASSOC);
    if ($stmt->execute()){ 
        while($row = $stmt->fetch()) {
            $retrievedEmail = $row['user_email'];
            $pass_hash = $row['user_pass_hash'];
            $userId = $row['user_id'];
            $retrievedUsername = $row['user_username'];
            
        }
    }
    
    if ($stmt->rowCount() == 1) {

        $checkPass = password_verify($inPass, $pass_hash);

        if ($checkPass) {
            $_SESSION['validUser'] = "yes";
            $_SESSION['username'] = $retrievedUsername;
            $_SESSION['userEmail'] = $retrievedEmail;	
            $_SESSION['user_id'] = $userId;
            array_push($exportArray, "pass_success", $retrievedUsername);
        } else {
            array_push($exportArray, "pass_fail", "There was an error with your username or password");
        }

    } else {
        array_push($exportArray, "pass_fail", "There was an error with your username or password");
    }

    echo json_encode($exportArray);
?>