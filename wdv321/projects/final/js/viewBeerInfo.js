var viewBeerInfo = (beerID) => {
    $.ajax({
        type: 'POST',
        url: 'getBeerInfo.php',
        data: {beerID: beerID},
        success: function(data) {
            var responseInfoArray = JSON.parse(data);
            
            var fsbeerName = responseInfoArray[1];
            var fsbreweryName = responseInfoArray[2];
            var fsbeerRating = responseInfoArray[3];
            var fsbeerNotes = responseInfoArray[4];
            var fsimagePath = responseInfoArray[5];

            $('#viewBeerName').html(fsbeerName);
            $('#viewBreweryName').html(fsbreweryName);
            for(i=0;i<fsbeerRating;i++){
                $('#viewBeerRating').append("<i class='fa fa-beer' aria-hidden='true'></i>");
            }
            $('#viewBeerNotes').html(fsbeerNotes);
            $('#viewBeerImage').attr("src",fsimagePath);
            
        },
        error: function() {alert("there was an error")}
    });
}