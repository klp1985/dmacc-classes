var beerEntry = () => {
    var validForm = true;
    var regexName = /^[^-\s][a-zA-Z0-9_\s-]+$/;
    var regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    var regexTextarea = /^[^<>']{0,1000}$/;

    $("input[name='rating']").click(function(){
        $('#ratingMsg').html("");
        validForm=true;
    });

    $("#beerFormSubmit").click(function(){

        if($("#beerName,#breweryName").val() != "") {
            $('#beerNameMsg,#breweryNameMsg').html("");
            $validForm=true;
            if(!$("#beerName,#breweryName").val().match(regexName)) {
                validForm = false;
                $('#beerNameMsg,#breweryNameMsg').html("<p class='error'>Name entered is invalid.</p>");
            } 
        } else {
            validForm = false;
            $('#beerNameMsg,#breweryNameMsg').html("<p class='error'>Name is required.</p>");
        }

        if($("#beerNotes").val() != "") {
            $('#beerNotesMsg').html("");
            $validForm=true;
            if(!$("#beerNotes").val().match(regexTextarea)) {
                validForm = false;
                $('#beerNotesMsg').html("<p class='error'>Message is invalid. Max characters is 1000 and can not contain special characters < > or '.</p>");
            } 
        } else {
            validForm = false;
            $('#beerNotesMsg').html("<p class='error'>Tell your future self how good it is.</p>");
        }

        if($("input[name='rating']:checked").val()==null) {
            $('#ratingMsg').html("<p class='error'>Please rate your beer</p>");
            $validForm = false;
        } else {
            $('#ratingMsg').html("");
            $validForm=true;
        }
        //alert(validForm);
        if(validForm) {
            $('#beerEntryForm').submit();
        }
    });
}