var passEntry = () => {
    //Returning user with a password
        $.ajax({
            type: 'POST',
            url: 'php/handleLogin.php',
            data: {passValue: $('#userPassword').val(), emailValue: $('#userEmail').val()},
            success: function(data)
            {
                //alert(data);
                var responsePassArray = JSON.parse(data);

                if(responsePassArray[0]== "pass_success") {
                    var toCookie = responsePassArray[1];
                    date = new Date();
                    date.setTime(date.getTime()+(7*24*60*60*1000));
                    expires = "; expires="+date.toGMTString();
                    document.cookie = "rememberUser="+toCookie+";"+expires+"; path=/";
                    window.location="php/dashboard.php";
                } else if (responsePassArray[0]== "pass_fail") {
                    $('#loginErrMsg').html(responsePassArray[1]);
                }            
            }
        });
}