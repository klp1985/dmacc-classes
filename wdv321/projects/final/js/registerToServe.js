var registerEntry = () => {
    //New user that creates a password
        $.ajax({
            type: 'POST',
            url: 'php/handleUserRegistration.php',
            data: {passValue: $('#registerPassword').val(), passConfirmValue: $('#registerPasswordConfirm').val(), emailValue: $('#registerEmail').val()},
            success: function(data) {
                //alert(data);
                var responseRegisterArray = JSON.parse(data);
                if(responseRegisterArray[0] == "register_success") {
                    $('#confirmReg').html(responseRegisterArray[5]);
                    $('#confirmReg').css('display','inherit');
                    $('#regForm').css('display','none');
                } else if(responseRegisterArray[0] == "register_fail") {
                    $('#regEmailErrMsg').html(responseRegisterArray[1]);
                    $('#regPasswordErrMsg').html(responseRegisterArray[2]);
                    $('#regPassConfirmErrMsg').html(responseRegisterArray[3]);
                    $('#confirmReg').html(responseRegisterArray[4]);
                }
            }
        });
}