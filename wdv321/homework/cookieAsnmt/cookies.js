//Creates a new cookie.  Tag is the name of the cookie and value is the value of the cookie.
//To set a cookie duration in days add a third arguement.  If no time is specified in the arguement, a session cookie will be created.
function addCookie(tag, value, days) {
    var expireDate = new Date();
    var expireString = "";
    var secureString = "";
    if (tag == "authenticated" && value == "yes") {
        secureString = "secure";
    }

    if (days != "") {
        expireDate.setTime(expireDate.getTime() + (1000 * 60 * 60 * 24 * days) );
        expireString = "expires="+ expireDate.toGMTString();
        document.cookie = tag + "=" + escape(value) + ";" + expireString + ";" + secureString + ";";
    } else {
        document.cookie = tag + "=" + escape(value) + ";" + secureString + ";";
        }
  }

//Retrieve information from the cookie
  function getCookie(tag) {
    var value = null;
    var myCookie = document.cookie + ";";
    var findTag = tag + "=";
    var endPos;
    if (myCookie.length > 0 ) {
      var beginPos = myCookie.indexOf(findTag);
      if (beginPos != -1) {
        beginPos = beginPos + findTag.length;
        endPos = myCookie.indexOf(";", beginPos);
        if (endPos == -1)
          endPos = myCookie.length;
        value = unescape(myCookie.substring(beginPos, endPos));
      }
    } 
  return value;   
  } 


//Clears all cookies
  function deleteCookie() {
        var cookies = document.cookie.split(";");
    
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
        }
  }

