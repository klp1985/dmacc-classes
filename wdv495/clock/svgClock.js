//Tutorial used: http://goinkscape.com/how-to-create-a-working-clock-in-inkscape/  by Aaron Nieze

		window.onload = function () {
				
				var s = Snap("#clockDiv");
				
						Snap.load("clock.svg", function(f) {
				
				
								secHand = f.select("#secHand");
						
								minHand = f.select("#minHand");
						
								hourHand = f.select("#hourHand");
						
				
								animateTime();
				                    
								s.append(f);
						});	
		};
		
		
		
		function animateTime() {
				                
				  var timeNow = new Date();
				  
				  var hours   = timeNow.getHours();
				  
				  var minutes = timeNow.getMinutes();
				  
				  var seconds = timeNow.getSeconds();
				  
				//Move second needle halfway                
				  secHand.transform('r' + (seconds*6-3) + ',83.478043,79.855125');
				
				//Animate the second needle the rest of the way
				  secHand.animate({transform: 'r' + (seconds*6) + ',83.478043,79.855125'}, 500, mina.elastic);
				
				//Move minute needle                
				  minHand.transform('r' + (minutes*6) + ',83.478043,79.855125');
				
				//Only animate the minute needle when the minute changes    
				  if(seconds == 0){minHand.transform('r' + (minutes*6-3) + ',83.478043,79.855125');
				  minHand.animate({transform: 'r' + (minutes*6) + ',83.478043,79.855125'}, 500, mina.elastic);}
				
				//Allow the hour needle to move accordingly when the minutes change             
				  hourHand.transform('r' + ((hours*30)+(minutes/2)) + ',83.478043,79.855125');
				
				//Repeat this entire function every 1 second
				  setTimeout("animateTime()", 1000);
				  }