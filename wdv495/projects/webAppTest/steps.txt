1.  Clone my Git repository from this URL: https://klp1985@bitbucket.org/klp1985/wdv495.git




          You don't need to do anything with the "geckodriver.exe" file.  It is just there to make the test work with Firefox.  The html and php files are for the form and the js file is the testing script.

2.  Download and install the Java dev kit and Selenium server.  Should be able to start the server by double clicking the .jar file after the JDK is installed.

Downloads can be found here:  
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html


http://www.seleniumhq.org/download/

3.  Install NodeJS  
Download link:  https://nodejs.org/en/

4.  In the command prompt,  change directory to the folder where you cloned the repository.


5.  Type "npm init" and press enter.  This will give a few prompts.  You can hit enter to skip some of them but you will need to enter something into the Name, Git Repository URL, and Description fields when they come up.


6.  Type in "npm --save install selenium-webdriver" 

7.  Then to start the test, you type in "node formTest.js".  You will need to make sure you have the latest version of Firefox or it will give an error.  
