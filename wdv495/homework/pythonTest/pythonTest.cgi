#!/usr/local/python-3.5/bin/python3
import cgi
import cgitb
#import smtplib
cgitb.enable()

#from email.mime.multipart import MIMEMultipart
#from email.mime.text import MIMEText

formData = cgi.FieldStorage()

firstname = formData.getvalue('firstname')

lastname = formData.getvalue('lastname')

toEmail = formData.getValue('email')

radioValue = formData.getvalue("radio")

checkValue1 = formData.getvalue("check1")

checkValue2 = formData.getvalue("check2")

dropdown = formData.getvalue("dropdown")

message = formData.getvalue("message")

#print("""Content-type: text/html\n\n""")
print("""Content-type: text/html\n\n""")
print("""<!DOCTYPE html>
 <html lang="en">
   <head>
      <title>Server-side scripting</title>
   </head>
   <body>
     <h1>Python Form Handling</h1>
	  <p>The form was processed using Python</p>
	  <p>The name entered into the form: {0} {1}</p>
	  <p>Radio selected: {2}</p>
	  <p>Checkbox 1 selected:  {3}</p>
	  <p>Checkbox 2 selected:  {4}</p>
	  <p>Dropdown list selection:  {5}</p>
	  <p>Message entered:  {6}</p>
	  <p>Email entered:  {7}</p>
   </body>
</html>
""".format(firstname, lastname, radioValue, checkValue1, checkValue2, dropdown, message, toEmail))

#fromEmail = "web@kraigpopelka.info"

#msg = MIMEMultipart('alternative')
#msg['Subject'] = "Python Form Processing"
#msg['From'] = fromEmail
#msg['To'] = toEmail

#msgText = "The form was processed and emailed using Python.\nThe name entered into the form: {0} {1}\nRadio selected: {2}\nCheckbox 1 selected:  {3}\nCheckbox 2 selected:  {4}\nDropdown list selection:  {5}\nMessage entered:  {6}\nEmail entered:  {7}".format(firstname, lastname, radioValue, checkValue1, checkValue2, dropdown, message, toEmail)

#type = MIMEText(msgText, 'plain')

#msg.attach(type)

#s = smtplib.SMTP('localhost')

#s.sendmail(fromEmail, toEmail, msgText.as_string())
#s.quit()