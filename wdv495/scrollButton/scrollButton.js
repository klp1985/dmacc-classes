			window.onload = function () {
			
				var s = new Snap("#iconDiv");
				
					Snap.load("scroll.svg", function(f) {
				
							orangeRect = f.select("#orangeRect");
							
							activateTop = f.select("#activateTop");
							
							activateBottom = f.select("#activateBottom");
							
							
							
							activateTop.hover(function() {
							
								orangeRect.animate({d: 'M 50.648811,63.410713 H 161.01785 V 244.83929 H 50.648811 Z'}, 200, mina.easein);
								
								orangeRect.animate({transform: 'r45,100,100'}, 200, mina.easein);
								},
								
								
								function() {
								
										orangeRect.animate({d: 'M 50.648811,63.410713 H 161.01785 V 244.83929 H 50.648811 Z'}, 200, mina.easein);
										
										orangeRect.animate({transform: 'r0,100,100'}, 200, mina.easein);
										
										orangeRect.animate({fill: '#fff'}, 50, mina.easein);
								});    

								
								activateBottom.hover(function() {
								
										orangeRect.animate({d: 'M 50.648811,63.410713 H 161.01785 V 244.83929 H 50.648811 Z'}, 200, mina.easein);
										
										orangeRect.animate({transform: 'r-45,100,100'}, 200, mina.easein); 
								},
								
								
								function() {
								
										orangeRect.animate({d: 'M 50.648811,63.410713 H 161.01785 V 244.83929 H 50.648811 Z'}, 200, mina.easein);
										
										orangeRect.animate({transform: 'r0,100,100'}, 200, mina.easein);
										
										orangeRect.animate({fill: '#fff'}, 50, mina.easein);
							});
								
								
								
							activateTop.mousedown(function() { 
									var y = $(window).scrollTop();
									$("html, body").animate({ scrollTop: y - $(window).height() }, 400);
									orangeRect.animate({fill: '#ccc'}, 50, mina.easein);                      
							});
							
							
							activateTop.mouseup(function() {
							orangeRect.animate({fill: '#fff'}, 50, mina.easein);                      
							});
							
							
							activateBottom.mousedown(function() {
							var y = $(window).scrollTop();
							
							
							$("html, body").animate({ scrollTop: y + $(window).height() }, 400);
							orangeRect.animate({fill: '#ccc'}, 50, mina.easein);                 
							});
							
							
							activateBottom.mouseup(function() {
							orangeRect.animate({fill: '#fff'}, 50, mina.easein);                      
							});
							
							s.append(f);
							});	
							
							};	